import {useToasts} from 'react-toast-notifications'
import {ResponeApiCheck} from '../helpers'
import {useIntl} from 'react-intl'

export function useNotification() {
  const {addToast, updateToast} = useToasts()
  const intl = useIntl()
  const showNotification = (props: ResponeApiCheck, notificationContent = null, status = null) => {
    const appearance = status
      ? status
      : props?.result == 'success' || notificationContent
      ? 'success'
      : 'error'
    addToast(
      notificationContent
        ? notificationContent
        : props?.result == 'success'
        ? intl.formatMessage({id: 'operation_accomplished_successfully'})
        : props?.error_description,
      {
        autoDismissTimeout: 5000,
        autoDismiss: notificationContent != null ? false : true,
        appearance,
      }
    )
  }

  return {showNotification}
}
