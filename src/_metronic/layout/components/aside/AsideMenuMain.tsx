/* eslint-disable react/jsx-no-target-blank */
import {useIntl} from 'react-intl'
import {KTIcon} from '../../../helpers'
import {AsideMenuItemWithSub} from './AsideMenuItemWithSub'
import {AsideMenuItem} from './AsideMenuItem'
import {useAuth} from '../../../../app/modules/auth'
import {AdminMenu} from './AdminMenu'
import {UserMenu} from './UserMenu'

export function AsideMenuMain() {
  const intl = useIntl()
  const {currentUser} = useAuth()
  return <>{currentUser.role_id === 1 ? <AdminMenu /> : <UserMenu />}</>
}
