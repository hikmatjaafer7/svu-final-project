/* eslint-disable react/jsx-no-target-blank */
import {useIntl} from 'react-intl'
import {KTIcon} from '../../../helpers'
import {AsideMenuItemWithSub} from './AsideMenuItemWithSub'
import {AsideMenuItem} from './AsideMenuItem'
import {useAuth} from '../../../../app/modules/auth'

export function UserMenu() {
  const intl = useIntl()
  const {currentUser} = useAuth()
  return (
    <>
      <AsideMenuItem
        to='/dashboard'
        icon='element-11'
        title={intl.formatMessage({id: 'dashboard'})}
      />
      <AsideMenuItemWithSub
        // to='/crafted/widgets'
        to=''
        title={intl.formatMessage({id: 'submit_request'})}
        icon='element-plus'
      >
        <AsideMenuItem
          key={'subscription'}
          to='/user/submit-request/subscription'
          title={intl.formatMessage({id: 'subscription'})}
          hasBullet
        />
        <AsideMenuItem
          key={'loan'}
          to='/user/submit-request/loan'
          title={intl.formatMessage({id: 'loan'})}
          hasBullet
        />
      </AsideMenuItemWithSub>
      <AsideMenuItemWithSub
        // to='/crafted/widgets'
        to=''
        title={intl.formatMessage({id: 'tracking_requests'})}
        icon='element-plus'
      >
        <AsideMenuItem
          key={'subscriptions'}
          to='/user/tracking-requests/subscriptions'
          title={intl.formatMessage({id: 'subscriptions'})}
          hasBullet
        />
        <AsideMenuItem
          key={'loans'}
          to='/user/tracking-requests/loans'
          title={intl.formatMessage({id: 'loans'})}
          hasBullet
        />
        {/* <AsideMenuItem
          key={'user-fund/details'}
          to='/user/user-fund/details'
          title={intl.formatMessage({id: 'user-fund/details'})}
          hasBullet
        /> */}
      </AsideMenuItemWithSub>
      <AsideMenuItem
        key={'funds'}
        to='/user/user-funds'
        title={intl.formatMessage({id: 'funds'})}
        icon='abstract-26'
      />
    </>
  )
}
