/* eslint-disable react/jsx-no-target-blank */
import {useIntl} from 'react-intl'
import {KTIcon} from '../../../helpers'
import {AsideMenuItemWithSub} from './AsideMenuItemWithSub'
import {AsideMenuItem} from './AsideMenuItem'
import {useAuth} from '../../../../app/modules/auth'

export function AdminMenu() {
  const intl = useIntl()
  const {currentUser} = useAuth()
  return (
    <>
      {(currentUser.id === 23 ||
        currentUser.permissions.find((p) => p.code === 'view_dashboard') !== undefined) && (
        <AsideMenuItem
          to='/dashboard'
          icon='element-11'
          title={intl.formatMessage({id: 'dashboard'})}
        />
      )}
      {(currentUser.id === 23 ||
        currentUser.permissions.find((p) => p.code === 'manage_users') !== undefined) && (
        <AsideMenuItem
          to='/admin/users-management/users'
          icon='profile-circle'
          title={intl.formatMessage({id: 'user_management'})}
        />
      )}
      {(currentUser.id === 23 ||
        currentUser.permissions.find((p) => p.code === 'manage_permissions') !== undefined) && (
        <AsideMenuItemWithSub
          // to='/crafted/widgets'
          to=''
          title={intl.formatMessage({id: 'permissions_management'})}
          icon='shield-tick'
        >
          <AsideMenuItem
            key={'permissions_management'}
            to='/admin/roles-management/roles'
            title={intl.formatMessage({id: 'roles'})}
            hasBullet
          />
          <AsideMenuItem
            key={'permissions_management'}
            to='/admin/roles-management/permissions'
            title={intl.formatMessage({id: 'permissions'})}
            hasBullet
          />
          <AsideMenuItem
            key={'permissions_groups_management'}
            to='/admin/roles-management/groups'
            title={intl.formatMessage({id: 'permissions_groups'})}
            hasBullet
          />
        </AsideMenuItemWithSub>
      )}
      {(currentUser.id === 23 ||
        currentUser.permissions.find((p) => p.code === 'manage_funds') !== undefined) && (
        <AsideMenuItem
          to='/admin/funds-management/funds'
          icon='abstract-26'
          title={intl.formatMessage({id: 'funds_management'})}
        />
      )}
      {/* {(currentUser.id === 23 ||
        currentUser.permissions.find((p) => p.code === 'manage_subscriptions') !== undefined) && (
        // <AsideMenuItemWithSub
        //   to=''
        //   title={intl.formatMessage({id: 'subscriptions_management'})}
        //   icon='element-plus'
        // >
        <AsideMenuItem
          key={'subscriptions_management'}
          to='/admin/subscriptions-management/subscriptions'
          title={intl.formatMessage({id: 'subscriptions_management'})}
          // hasBullet
          icon='element-plus'
        />
        //   <AsideMenuItem
        //     key={'subscriptions_management'}
        //     to='/admin/subscriptions-management/requests'
        //     title={intl.formatMessage({id: 'subscription_requests'})}
        //     hasBullet
        //   />
        // </AsideMenuItemWithSub>
      )}
      {(currentUser.id === 23 ||
        currentUser.permissions.find((p) => p.code === 'manage_loans') !== undefined) && (
        // <AsideMenuItemWithSub
        //   to=''
        //   title={intl.formatMessage({id: 'subscriptions_management'})}
        //   icon='element-plus'
        // >
        <AsideMenuItem
          key={'loans_management'}
          to='/admin/loans-management/loans'
          title={intl.formatMessage({id: 'loans_management'})}
          // hasBullet
          icon='element-plus'
        />
        //   <AsideMenuItem
        //     key={'loans_management'}
        //     to='/admin/loans-management/requests'
        //     title={intl.formatMessage({id: 'loan_requests'})}
        //     hasBullet
        //   />
        // </AsideMenuItemWithSub>
      )}{' '} */}
    </>
  )
}
