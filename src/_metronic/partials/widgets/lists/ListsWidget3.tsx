/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import {KTIcon} from '../../../helpers'
import {Dropdown1} from '../../content/dropdown/Dropdown1'
import {Link} from 'react-router-dom'

type Props = {
  className: string
  title?: string
  year?: string
  fund_id?: string | number
  withFilterBtn?: boolean
  name?: string
  to?: string
  items?: {color: string; title: string; description: string; total: string | number}[]
}

const ListsWidget3: React.FC<Props> = ({
  className,
  title = 'Todo',
  withFilterBtn = false,
  items = [],
  year,
  name,
  fund_id,
  to = '/admin/funds-management/payments/users',
}) => {
  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className='card-header border-0'>
        <h3 className='card-title fw-bold text-dark'>{title}</h3>
        <div className='card-toolbar'>
          {/* begin::Menu */}
          {withFilterBtn && (
            <>
              {' '}
              <button
                type='button'
                className='btn btn-sm btn-icon btn-color-primary btn-active-light-primary'
                data-kt-menu-trigger='click'
                data-kt-menu-placement='bottom-end'
                data-kt-menu-flip='top-end'
              >
                <KTIcon iconName='category' className='fs-2' />
              </button>
              {/* <Dropdown1 /> */}
            </>
          )}
          {/* end::Menu */}
        </div>
      </div>
      <div className='separator mx-9 mb-3'></div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className='card-body pt-2'>
        {items &&
          items.map((item: any) => {
            return (
              <>
                {/* begin::Item */}
                <div className='d-flex align-items-center mb-4 border border-dashed p-1 rounded'>
                  {/* begin::Bullet */}
                  <span className={`bullet bullet-vertical mx-5 h-40px bg-${item.color}`}></span>
                  {/* end::Bullet */}
                  {/* begin::Checkbox */}
                  {/* <div className='form-check form-check-custom form-check-solid mx-5'>
                    <input className='form-check-input' type='checkbox' value='' />
                  </div> */}
                  {/* end::Checkbox */}
                  {/* begin::Description */}
                  <div className='flex-grow-1'>
                    <Link
                      to={to}
                      state={{id: fund_id, year, month: item.number, name}}
                      className='text-gray-800 text-hover-primary fw-bold fs-4'
                    >
                      {item.title}
                    </Link>
                    <span className='text-muted fw-semibold d-block '>{item.description}</span>
                  </div>
                  {/* end::Description */}
                  <span
                    className={`badge badge-light-${item.color} fs-4 fw-bold min-w-70px text-center justify-content-center`}
                  >
                    {item.total}
                  </span>
                </div>
                {/* end:Item */}
              </>
            )
          })}
        {/* begin::Item */}
        {/* <div className='d-flex align-items-center mb-8'> */}
        {/* begin::Bullet */}
        {/* <span className='bullet bullet-vertical h-40px bg-success'></span> */}
        {/* end::Bullet */}
        {/* begin::Checkbox */}
        {/* <div className='form-check form-check-custom form-check-solid mx-5'>
            <input className='form-check-input' type='checkbox' value='' />
          </div> */}
        {/* end::Checkbox */}
        {/* begin::Description */}
        {/* <div className='flex-grow-1'>
            <a href='#' className='text-gray-800 text-hover-primary fw-bold fs-6'>
              Create FireStone Logo
            </a>
            <span className='text-muted fw-semibold d-block'>Due in 2 Days</span>
          </div> */}
        {/* end::Description */}
        {/* <span className='badge badge-light-success fs-8 fw-bold'>New</span>
        </div> */}
        {/* end:Item */}
      </div>
      {/* end::Body */}
    </div>
  )
}

export {ListsWidget3}
