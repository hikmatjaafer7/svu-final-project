import {AdminRoutes} from './AdminRoutes'
import {UserRoutes} from './UserRoutes'
import {useAuth} from '../modules/auth'

const PrivateRoutes = () => {

  const {currentUser} = useAuth()

  return <>{currentUser.role_id === 1 ? <AdminRoutes /> : <UserRoutes />}</>
}


export {PrivateRoutes}
