import {useMemo} from 'react'
import {subscriptionsColumns} from './columns/_columns'
import Table from '../../../../../components/Table'
import {useGenericQueryRequest} from '../../../../../core/GenericQueryRequestProvider'
import {
  useGenericQueryResponseData,
  useGenericQueryResponseLoading,
  useGenericQueryResponsePagination,
} from '../../../../../core/GenericQueryResonseProvider'
import {KTCardBody} from '../../../../../../_metronic/helpers'

const SubscriptionsTable = () => {
  const subscriptions = useGenericQueryResponseData()
  const isLoading = useGenericQueryResponseLoading()
  const pagination = useGenericQueryResponsePagination()
  const {updateState} = useGenericQueryRequest()
  const data = useMemo(() => subscriptions, [subscriptions])
  const columns = useMemo(() => subscriptionsColumns, [])
  return (
    <>
      <KTCardBody className='py-4'>
        <Table
          withPagination={false}
          columns={columns}
          data={data}
          isLoading={isLoading}
          pagination={pagination}
          updateState={updateState}
        />
      </KTCardBody>
    </>
  )
}

export {SubscriptionsTable}
