import {SubscriptionsTable} from './table'
import {KTCard, QUERIES} from '../../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponseRefetch,
} from '../../../../core/GenericQueryResonseProvider'
import {
  GenericQueryRequestProvider,
  useGenericQueryRequest,
} from '../../../../core/GenericQueryRequestProvider'
import {getSubscriptionsByUserId} from './core/_requests'
import {ListHeader} from '../../../../components/Table/header/ListHeader'
import {useState} from 'react'
import {Loading} from '../../../../components/Loading'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {useNavigate} from 'react-router-dom'
import Select from '../../../../components/Select'
import {useQuery} from 'react-query'
import {useAuth} from '../../../auth'

const SubscriptionsList = () => {
  const {itemIdForUpdate} = useGenericListView()
  const refetch = useGenericQueryResponseRefetch()
  // const {selected} = useGenericListView()
  const [isLoading, setIsLoading] = useState(false)
  const {showNotification} = useNotification()
  const navigate = useNavigate()
  return (
    <>
      <KTCard>
        <ListHeader
          ListFilterChildren={<></>}
          queryString={QUERIES.SUBSCRIPTIONS_BY_FUND_LIST}
          deleteSelected={(selected) => {
            // setIsLoading(true)
            // deleteSelectedSubscriptions(selected)
            // .then((res) => {
            //   setIsLoading(false)
            //   showNotification({result: 'success', data: '', error_description: ''})
            //   refetch()
            // })
            // .catch((err) => {
            //   setIsLoading(false)
            //   showNotification({result: 'error', data: '', error_description: 'error'})
            //   refetch()
            // })
          }}
        />
        <SubscriptionsTable />
        {isLoading && <Loading />}
      </KTCard>
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <SubscriptionsList />
    </GenericListViewProvider>
  )
}

const SubscriptionsListWrapper = () => {
  const data = useGenericQueryResponseData()
  const {currentUser} = useAuth()

  return (
    <>
      <GenericQueryRequestProvider>
        <GenericQueryResponseProvider
          getApi={getSubscriptionsByUserId}
          urlParameter={currentUser.id.toString()}
          queryString={QUERIES.SUBSCRIPTIONS_BY_FUND_LIST}
        >
          <ListView />
        </GenericQueryResponseProvider>
      </GenericQueryRequestProvider>
    </>
  )
}

export {SubscriptionsListWrapper}
