import {ID, Response} from '../../../../../../_metronic/helpers'
import RequestStatus from '../../../../../core/enums/RequestStatus'
export type Subscription = {
  id?: ID
  value: string
  UserId: ID
  FundId: ID
  status?: RequestStatus
  reason?: string
  createdAt?: Date
  updatedAt?: Date | null
}

export type SubscriptionsQueryResponse = Response<Array<Subscription>>

export const initialSubscription: Subscription = {
  value: '',
  status: RequestStatus.Pending,
  UserId: 0,
  FundId: 0,
}
