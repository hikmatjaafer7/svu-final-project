import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../../_metronic/helpers'
import {Subscription, SubscriptionsQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../../services/crud-api'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const SUBSCRIPTION_URL = `${API_URL}/subscriptions`
const GET_SUBSCRIPTIONS_URL = `${API_URL}/subscriptions/query`

const getSubscriptionsByUserId = async (userId: ID, query: string) => {
  const data = await getList(`${SUBSCRIPTION_URL}/user/${userId}?` + query)

  console.log('data :>> ', data)
  return {data: data}
  // .then((d: AxiosResponse<SubscriptionsQueryResponse>) => d)
}

export {getSubscriptionsByUserId}
