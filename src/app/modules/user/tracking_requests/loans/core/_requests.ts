import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../../_metronic/helpers'
import {Loan, LoansQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../../services/crud-api'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const LOAN_URL = `${API_URL}/loans`
const GET_LOANS_URL = `${API_URL}/loans/query`

const getSubscriptionsByUserId = async (userId: ID, query: string) => {
  const data = await getList(`${LOAN_URL}/user/${userId}?` + query)
  return {data: data}
  // .then((d: AxiosResponse<SubscriptionsQueryResponse>) => d)
}

export {getSubscriptionsByUserId}
