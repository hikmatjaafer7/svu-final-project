import {useMemo} from 'react'
import {loansColumns} from './columns/_columns'
import Table from '../../../../../components/Table'
import {useGenericQueryRequest} from '../../../../../core/GenericQueryRequestProvider'
import {
  useGenericQueryResponseData,
  useGenericQueryResponseLoading,
  useGenericQueryResponsePagination,
} from '../../../../../core/GenericQueryResonseProvider'
import {KTCardBody} from '../../../../../../_metronic/helpers'

const LoansTable = () => {
  const loans = useGenericQueryResponseData()
  const isLoading = useGenericQueryResponseLoading()
  const pagination = useGenericQueryResponsePagination()
  const {updateState} = useGenericQueryRequest()
  const data = useMemo(() => loans, [loans])
  const columns = useMemo(() => loansColumns, [])
  return (
    <>
      <KTCardBody className='py-4'>
        <Table
          withPagination={false}
          columns={columns}
          data={data}
          isLoading={isLoading}
          pagination={pagination}
          updateState={updateState}
        />
      </KTCardBody>
    </>
  )
}

export {LoansTable}
