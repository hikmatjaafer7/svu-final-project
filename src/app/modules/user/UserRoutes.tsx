import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import Subscription from './submit_request/subscription'
import {SubscriptionsListWrapper} from './tracking_requests/subscriptions/SubscriptionsListWrapper'
import Loan from './submit_request/loan'
import {LoansListWrapper} from './tracking_requests/loans/LoansListWrapper'
import { UserFundsListWrapper } from '../admin/userFunds/UserFundsListWrapper'
import UserFundDetails from '../admin/userFunds/details'

const UserRoutes = () => {
  const intl = useIntl()
  const subscriptionsBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'dashboard'}),
      path: 'user/dashboard',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/submit-request/subscription'
          element={
            <>
              <PageTitle breadcrumbs={subscriptionsBreadcrumbs}>
                {intl.formatMessage({id: 'submit_subscription_request'})}
              </PageTitle>
              <Subscription />
            </>
          }
        />
        <Route
          path='/tracking-requests/subscriptions'
          element={
            <>
              <PageTitle breadcrumbs={subscriptionsBreadcrumbs}>
                {intl.formatMessage({id: 'subscriptions_list'})}
              </PageTitle>
              <SubscriptionsListWrapper />
            </>
          }
        />

        <Route
          path='/submit-request/loan'
          element={
            <>
              <PageTitle breadcrumbs={subscriptionsBreadcrumbs}>
                {intl.formatMessage({id: 'submit_loan_request'})}
              </PageTitle>
              <Loan />
            </>
          }
        />
        <Route
          path='/tracking-requests/loans'
          element={
            <>
              <PageTitle breadcrumbs={subscriptionsBreadcrumbs}>
                {intl.formatMessage({id: 'loans_list'})}
              </PageTitle>
              <LoansListWrapper />
            </>
          }
        />
        <Route
          path='/user-funds'
          element={
            <>
              <PageTitle breadcrumbs={subscriptionsBreadcrumbs}>
                {intl.formatMessage({id: 'funds_list'})}
              </PageTitle>
              <UserFundsListWrapper />
            </>
          }
        />
        <Route
          path='/user-funds/details'
          element={
            <>
              <PageTitle breadcrumbs={subscriptionsBreadcrumbs}>
                {intl.formatMessage({id: 'fund_details'})}
              </PageTitle>
              <UserFundDetails />
            </>
          }
        />
      </Route>

      {/* <Route index element={<Navigate to='/user/tracking-requests/subscriptions' />} /> */}
    </Routes>
  )
}

export default UserRoutes
