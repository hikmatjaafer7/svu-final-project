import {Formik, useFormikContext} from 'formik'
import {
  KTCard,
  KTCardBody,
  QUERIES,
  ResponeApiCheck,
  initialResponseError,
} from '../../../../../_metronic/helpers'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {useNavigate} from 'react-router-dom'
import {useEffect, useState} from 'react'
import {useIntl} from 'react-intl'
import * as Yup from 'yup'
import FormikSelect from '../../../../components/formik/FormikSelect'
import FormikInput from '../../../../components/formik/FormikInput'
import ResetButton from '../../../../components/ResetButton'
import SubmitButton from '../../../../components/SubmitButton'
import {getFunds} from '../../../admin/funds/core/_requests'
import {useQuery} from 'react-query'
import {createLoan} from '../../../admin/loans/core/_requests'
import {useAuth} from '../../../auth'
import {getSubscriptionsByUserId} from '../../tracking_requests/subscriptions/core/_requests'

const RoleSchema = () => {
  const intl = useIntl()
  const validations = Yup.object().shape({
    FundId: Yup.number()
    .min(1)
    .required(intl.formatMessage({id: 'field_is_required'})),
    value: Yup.number().required(intl.formatMessage({id: 'field_is_required'})),
  })
  return validations
}
const Form = () => {
  const {currentUser} = useAuth()

  const {data: listFunds} = useQuery(
    QUERIES.SUBSCRIPTIONS_BY_USER_LIST,
    () => {
      return getSubscriptionsByUserId(currentUser.id, '').then((resData) => {
        const uniqueFunds = {}
        const result = [
          {
            key: 0,
            value: 'اختر من القائمة',
          },
        ]

        resData?.data?.map((item, index) => {
          const fund = item.Fund
          if (fund && !uniqueFunds[fund.id]) {
            uniqueFunds[fund.id] = true
            result.push({key: fund.id, value: fund.name})
          }
        })

        return result
      })
    },
    {
      refetchOnMount: true,
    }
  )
  const intl = useIntl()
  const {handleSubmit, resetForm, isSubmitting, isValid, touched, values, setFieldValue, errors} =
    useFormikContext()
  return (
    <>
      <div className='d-flex flex-column scroll-y me-n7 pe-7 bg-white ps-7 pt-7 pb-7'>
        <div className='row'>
          <div className='col-md-3 col-sm-12'>
            <FormikSelect isRequired={true} name='FundId' title='fund' options={listFunds} />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              min={1}
              type='number'
              title={'value'}
              name={'value'}
              value={values['value']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikSelect
              isRequired={false}
              name='type'
              title='type'
              options={[
                {key: 'ordinary', value: intl.formatMessage({id: 'ordinary'})},
                {key: 'emergency', value: intl.formatMessage({id: 'emergency'})},
              ]}
            />{' '}
          </div>
          {/* begin::Actions */}
          <div className='text-center pt-15'>
            <ResetButton resetForm={resetForm} isSubmitting={isSubmitting} />
            <SubmitButton
              isSubmitting={isSubmitting}
              isValid={isValid}
              touched={touched}
              onclick={handleSubmit}
            />
          </div>
          {/* end::Actions */}
        </div>
      </div>
    </>
  )
}

const Loan = () => {
  const navigate = useNavigate()
  const {showNotification} = useNotification()
  const {currentUser} = useAuth()

  return (
    <KTCard>
      <KTCardBody className='py-4'>
        <Formik
          enableReinitialize={true}
          validationSchema={RoleSchema()}
          initialValues={{}}
          initialStatus={{edit: false}}
          onSubmit={async (values: any, {setSubmitting}) => {
            if (values && values['FundId'] && values['FundId'] < 1) return
            setSubmitting(true)
            try {
              let reqModel = {
                ...values,
                UserId: currentUser.id,
                user_id: currentUser.id,
                fund_id: values['FundId'],
              }
              const res: ResponeApiCheck = await createLoan(reqModel)
              if (res.result == 'success') {
                navigate('/user/tracking-requests/loans')
              }
              showNotification(
                res,
                'تم تقديم طلبك بنجاح , يمكنك متابعة جميع طلباتك من قسم " متابعة الطلبات  "',
                'success'
              )
            } catch (ex) {
              showNotification({error_description: ex, ...initialResponseError})
              console.error(ex)
            } finally {
              setSubmitting(true)
            }
          }}
          onReset={(values) => {}}
        >
          <Form />
        </Formik>
      </KTCardBody>
    </KTCard>
  )
}

export default Loan
