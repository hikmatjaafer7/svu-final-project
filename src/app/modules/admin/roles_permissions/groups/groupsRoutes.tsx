import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {GroupsListWrapper} from './GroupsListWrapper'
// import PermissionForm from './add-edit/AddEditRole'
import {Form} from './add-edit/Form'
import Add from './add-edit/Add'
import Edit from './add-edit/Edit'

const GroupsRoutes = () => {
  const intl = useIntl()
  const groupsBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'group_management'}),
      path: 'admin/roles-management/groups',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/groups'
          element={
            <>
              <PageTitle breadcrumbs={groupsBreadcrumbs}>
                {intl.formatMessage({id: 'groups_list'})}
              </PageTitle>
              <GroupsListWrapper />
            </>
          }
        />
        <Route
          path='/edit-group'
          element={
            <>
              <PageTitle breadcrumbs={groupsBreadcrumbs}>
                {intl.formatMessage({id: 'edit_group'})}
              </PageTitle>
              <Edit />
            </>
          }
        />
        <Route
          path='/add-group'
          element={
            <>
              <PageTitle breadcrumbs={groupsBreadcrumbs}>
                {intl.formatMessage({id: 'add_group'})}
              </PageTitle>
              <Add />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/roles-management/groups' />} />
    </Routes>
  )
}

export default GroupsRoutes
