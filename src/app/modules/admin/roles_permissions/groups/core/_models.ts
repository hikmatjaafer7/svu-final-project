import {ID, Response} from '../../../../../../_metronic/helpers'
export type Group = {
  id?: ID
  code: string
  name: string
}

export type GroupsQueryResponse = Response<Array<Group>>

export const initialGroup: Group = {
  name: '',
  code: '',
}
