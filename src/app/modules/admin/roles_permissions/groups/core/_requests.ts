import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../../_metronic/helpers'
import {Group} from './_models'
import {getList, dele, create} from '../../../../../../services/crud-api'
import {post} from '../../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const ROLES_GROUPS_URL = `/roles_permissions/groups`
const FULL_ROLES_GROUPS_URL = `${API_URL}${ROLES_GROUPS_URL}`

const getGroups = async (query: string) => {
  const data = await getList(`${FULL_ROLES_GROUPS_URL}?` + query)
  return data
  // .then((d: AxiosResponse<UsersQueryResponse>) => d)
}

const getGroupById = (id: ID): Promise<Group | undefined> => {
  return axios
  .get(`${FULL_ROLES_GROUPS_URL}/${id}`)
  .then((response: AxiosResponse<Response<Group>>) => {
    return response.data
  })
  .then((response: Response<Group>) => response.data)
}

const createGroup = (Group: Group): Promise<ResponeApiCheck | undefined> => {
  return create(`${FULL_ROLES_GROUPS_URL}/`, Group)
  // return axios
  //   .put(Group_URL, Group)
  //   .then((response: AxiosResponse<Response<Group>>) => response.data)
  //   .then((response: Response<Group>) => response.data)
}

const updateGroup = (Group: Group): Promise<ResponeApiCheck | undefined> => {
  return axios
  .put(`${FULL_ROLES_GROUPS_URL}/${Group.id}`, Group)
  .then((response: AxiosResponse<Response<ResponeApiCheck>>) => {
    return response.data
  })
  .then((response: ResponeApiCheck) => {
    return response
  })
}

const deleteGroup = async (GroupId: ID) => {
  return await dele(`${FULL_ROLES_GROUPS_URL}`, GroupId)
}

const deleteSelectedGroups = (GroupIds: Array<ID>): Promise<void> => {
  const requests = GroupIds.map((id) => axios.delete(`${FULL_ROLES_GROUPS_URL}/${id}`))
  return axios.all(requests).then(() => {})
}

export {getGroups, deleteGroup, deleteSelectedGroups, getGroupById, createGroup, updateGroup}
