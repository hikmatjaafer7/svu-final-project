import {GroupsTable} from './table'
import {KTCard, QUERIES} from '../../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponseRefetch,
} from '../../../../core/GenericQueryResonseProvider'
import {
  GenericQueryRequestProvider,
  useGenericQueryRequest,
} from '../../../../core/GenericQueryRequestProvider'
import {deleteSelectedGroups, getGroups} from './core/_requests'
import {ListHeader} from '../../../../components/Table/header/ListHeader'
import {useState} from 'react'
import {Loading} from '../../../../components/Loading'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {useNavigate} from 'react-router-dom'

const GroupsList = () => {
  const {itemIdForUpdate} = useGenericListView()
  const refetch = useGenericQueryResponseRefetch()
  // const {selected} = useGenericListView()
  const [isLoading, setIsLoading] = useState(false)
  const {showNotification} = useNotification()
  const navigate = useNavigate()
  return (
    <>
      <KTCard>
        <ListHeader
          handleAdd={() => {
            navigate('/admin/roles-management/add-group')
          }}
          onFilterSubmit={(filterUpdateState) => {}}
          ListFilterChildren={<></>}
          queryString={QUERIES.ROLES_GROUPS_LIST}
          deleteSelected={(selected) => {
            setIsLoading(true)
            deleteSelectedGroups(selected)
            .then((res) => {
              setIsLoading(false)
              showNotification({result: 'success', data: '', error_description: ''})
              refetch()
            })
            .catch((err) => {
              setIsLoading(false)
              showNotification({result: 'error', data: '', error_description: 'error'})
              refetch()
            })
          }}
        />
        <GroupsTable />
        {isLoading && <Loading />}
      </KTCard>
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <GroupsList />
    </GenericListViewProvider>
  )
}

const GroupsListWrapper = () => {
  return (
    <GenericQueryRequestProvider>
      <GenericQueryResponseProvider getApi={getGroups} queryString={QUERIES.ROLES_GROUPS_LIST}>
        <ListView />
      </GenericQueryResponseProvider>
    </GenericQueryRequestProvider>
  )
}

export {GroupsListWrapper}
