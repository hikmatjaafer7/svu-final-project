import {FC, useState} from 'react'
import {useFormikContext} from 'formik'
import {useIntl} from 'react-intl'
import {Loading} from '../../../../../components/Loading'
import FormikInput from '../../../../../components/formik/FormikInput'
import SubmitButton from '../../../../../components/SubmitButton'
import ResetButton from '../../../../../components/ResetButton'

const Form: FC = (props: any) => {
  const intel = useIntl()
  const [isLoading, setIsLoading] = useState(false)
  const {handleSubmit, resetForm, isSubmitting, isValid, touched, values, setFieldValue, errors} =
    useFormikContext()

  return (
    <div>
      <div className='d-flex flex-column scroll-y me-n7 pe-7 bg-white ps-7 pt-7 pb-7'>
        <div className='row'>
          <div className='col-md-6 col-sm-12'>
            <FormikInput isRequired={true} title={'code'} name={'code'} value={values['code']} />
          </div>
          <div className='col-md-6 col-sm-12'>
            <FormikInput isRequired={true} title={'name'} name={'name'} value={values['name']} />
          </div>
          {/* begin::Actions */}
          <div className='text-center pt-15'>
            <ResetButton resetForm={resetForm} isSubmitting={isSubmitting} />
            <SubmitButton
              isSubmitting={isSubmitting}
              isValid={isValid}
              touched={touched}
              onclick={handleSubmit}
            />
          </div>
          {/* end::Actions */}
        </div>
      </div>
      {isSubmitting && <Loading />}
    </div>
  )
}

export {Form}
