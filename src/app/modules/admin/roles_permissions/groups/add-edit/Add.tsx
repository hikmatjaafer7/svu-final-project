import {Formik} from 'formik'
import {
  KTCard,
  KTCardBody,
  QUERIES,
  ResponeApiCheck,
  initialResponseError,
} from '../../../../../../_metronic/helpers'
import {Form} from './Form'
import {RoleSchema} from './validationForm'
import {createGroup} from '../core/_requests'
import {useNotification} from '../../../../../../_metronic/hooks/useNotification'
import {useNavigate} from 'react-router-dom'
import {initialGroup} from '../core/_models'

const Add = () => {
  const navigate = useNavigate()
  const {showNotification} = useNotification()
  return (
    <KTCard>
      <KTCardBody className='py-4'>
        <Formik
          enableReinitialize={true}
          validationSchema={RoleSchema()}
          initialValues={initialGroup}
          initialStatus={{edit: false}}
          onSubmit={async (values: any, {setSubmitting}) => {
            setSubmitting(true)
            try {
              const res: ResponeApiCheck = await createGroup(values)
              console.log('res REsponse API CHECK :>> ', res)
              if (res.result == 'success') {
                navigate('/admin/roles-management/groups')
              }
              showNotification(res)
            } catch (ex) {
              showNotification({error_description: ex, ...initialResponseError})
              console.error(ex)
            } finally {
              setSubmitting(true)
            }
          }}
          onReset={(values) => {}}
        >
          <Form />
        </Formik>
      </KTCardBody>
    </KTCard>
  )
}

export default Add
