import {useMemo} from 'react'
import {groupsColumns} from './columns/_columns'
import Table from '../../../../../components/Table'
import {useGenericQueryRequest} from '../../../../../core/GenericQueryRequestProvider'
import {
  useGenericQueryResponseData,
  useGenericQueryResponseLoading,
  useGenericQueryResponsePagination,
} from '../../../../../core/GenericQueryResonseProvider'
import {KTCardBody} from '../../../../../../_metronic/helpers'

const GroupsTable = () => {
  const groups = useGenericQueryResponseData()
  const isLoading = useGenericQueryResponseLoading()
  const pagination = useGenericQueryResponsePagination()
  const {updateState} = useGenericQueryRequest()
  const data = useMemo(() => groups, [groups])
  const columns = useMemo(() => groupsColumns, [])
  return (
    <>
      <KTCardBody className='py-4'>
        <Table
          columns={columns}
          data={data}
          isLoading={isLoading}
          pagination={pagination}
          updateState={updateState}
        />
      </KTCardBody>
    </>
  )
}

export {GroupsTable}
