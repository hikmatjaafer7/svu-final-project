// @ts-nocheck
import {Column} from 'react-table'
import {Group} from '../../core/_models'
import {ActionsCell} from './ActionCell'
import {Localize} from '../../../../../../../_metronic/i18n/Localize'
import * as Columns from '../../../../../../components/Table/columns'

const groupsColumns: ReadonlyArray<Column<Group>> = [
  {
    Header: (props) => <Columns.SelectionHeader tableProps={props} />,
    id: 'selection',
    Cell: ({...props}) => <Columns.SelectionCell id={props.data[props.row.index].id} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='name' />}
        className='min-w-125px'
      />
    ),
    id: 'name',
    accessor: 'name',
    // Cell: ({...props}) => <GroupInfoCell group={props.data[props.row.index]} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='code' />}
        className='min-w-125px'
      />
    ),
    accessor: 'code',
    id: 'code',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='actions' />}
        className='text-end min-w-100px'
      />
    ),
    id: 'actions',
    Cell: ({...props}) => <ActionsCell data={props.data[props.row.index]} />,
  },
]

export {groupsColumns}
