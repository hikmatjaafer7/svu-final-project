import {LoansTable} from './table'
import {KTCard, QUERIES} from '../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponseRefetch,
} from '../../../core/GenericQueryResonseProvider'
import {
  GenericQueryRequestProvider,
  useGenericQueryRequest,
} from '../../../core/GenericQueryRequestProvider'
import {
  getFundLoanAlarmsByFundId,
} from './core/_requests'
import {ListHeader} from '../../../components/Table/header/ListHeader'
import {useState} from 'react'
import {Loading} from '../../../components/Loading'
import {useNotification} from '../../../../_metronic/hooks/useNotification'
import {useLocation, useNavigate} from 'react-router-dom'
import Select from '../../../components/Select'
import {useQuery} from 'react-query'
import {getFunds} from '../funds/core/_requests'
import Button from '../../../components/Button'

const LoansList = () => {
  const refetch = useGenericQueryResponseRefetch()
  const [isLoading, setIsLoading] = useState(false)
  const {showNotification} = useNotification()
  const navigate = useNavigate()
  const location: any = useLocation()
  const state: any = location.state
  const submitFundId = state?.id || 0
  return (
    <>
      <KTCard>
        <ListHeader
          onFilterSubmit={(filterUpdateState) => {}}
          ListFilterChildren={<></>}
          queryString={QUERIES.LIST_FUND_SUBSCRIPTION_ALARMS}
          // deleteSelected={(selected) => {
          //   setIsLoading(true)
          //   deleteSelectedFundLoanAlarmss(selected)
          //   .then((res) => {
          //     setIsLoading(false)
          //     showNotification({result: 'success', data: '', error_description: ''})
          //     refetch()
          //   })
          //   .catch((err) => {
          //     setIsLoading(false)
          //     showNotification({result: 'error', data: '', error_description: 'error'})
          //     refetch()
          //   })
          // }}
        />
        <LoansTable />
        {isLoading && <Loading />}
      </KTCard>
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <LoansList />
    </GenericListViewProvider>
  )
}

const LoansAlarmsListWrapper = () => {
  const location: any = useLocation()
  const state: any = location.state
  const submitFundId = state?.id || 0

  return (
    <>
      {submitFundId > 0 && (
        <GenericQueryRequestProvider>
          <GenericQueryResponseProvider
            getApi={getFundLoanAlarmsByFundId}
            urlParameter={submitFundId.toString()}
            queryString={QUERIES.LIST_FUND_SUBSCRIPTION_ALARMS}
          >
            <ListView />
          </GenericQueryResponseProvider>
        </GenericQueryRequestProvider>
      )}
    </>
  )
}

export {LoansAlarmsListWrapper}
