import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {FundLoanAlarms} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const LOAN_URL = `${API_URL}/funds/`

const LOAN_ALARMS_URL = `${API_URL}/subscription-alarms`
const GET_LOANS_URL = `${API_URL}/subscriptions/query`

const getFundLoanAlarmsByFundId = async (fundId: ID, query: string) => {
  const data = await getList(`${LOAN_URL}${fundId}/loan/alarms?` + query)

  console.log('data :>> ', data)
  return {data: data}
  // .then((d: AxiosResponse<FundLoanAlarmssQueryResponse>) => d)
}

export {getFundLoanAlarmsByFundId}
