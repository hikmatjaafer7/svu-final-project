import {ID, Response} from '../../../../../_metronic/helpers'

interface LoanAlarm {
  id: number
  description: string
  createdAt: Date | null
  updatedAt: Date | null
  loan_id: number
}

interface User {
  id: number
  name: string
  email: string
}

export interface FundLoanAlarms {
  id: number
  fund_id: number
  fund_name: string
  value: number
  status: string
  reason: string
  createdAt: string
  Alarms: LoanAlarm[]
  User: User | null
}

export const initFundLoanAlarms: FundLoanAlarms = {
  id: 0,
  fund_id: 0,
  fund_name: '',
  value: 0,
  status: '',
  reason: '',
  createdAt: '',
  Alarms: [],
  User: null,
}

export type FundLoanAlarmsQueryResponse = Response<Array<FundLoanAlarms>>
