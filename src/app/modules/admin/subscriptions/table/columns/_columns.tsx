// @ts-nocheck
import {Column} from 'react-table'
import {Subscription} from '../../core/_models'
import {ActionsCell} from './ActionCell'
import {Localize} from '../../../../../../_metronic/i18n/Localize'
import * as Columns from '../../../../../components/Table/columns'
import {StatusCell} from '../../../../../components/Table/columns/StatusCell'
import {KTIcon} from '../../../../../../_metronic/helpers'

const subscriptionsColumns: ReadonlyArray<Column<Subscription>> = [
  {
    Header: (props) => <Columns.SelectionHeader tableProps={props} />,
    id: 'selection',
    Cell: ({...props}) => <Columns.SelectionCell id={props.data[props.row.index].id} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='subscription_id' />}
        className='min-w-125px'
      />
    ),
    id: 'id',
    accessor: 'id',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='user_name' />}
        className='min-w-125px'
      />
    ),
    id: 'user_name',
    Cell: ({...props}) => (
      <div>
        {props.data[props.row.index]?.User?.first_name +
          ' ' +
          props.data[props.row.index]?.User?.last_name}
      </div>
    ),
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='fund_name' />}
        className='min-w-125px'
      />
    ),
    id: 'fund_name',
    Cell: ({...props}) => <div>{props.data[props.row.index].Fund.name}</div>,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='value' />}
        className='min-w-125px'
      />
    ),
    id: 'value',
    accessor: 'value',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='initial_balance' />}
        className='min-w-125px'
      />
    ),
    id: 'initial_balance',
    accessor: 'initial_balance',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='value_in_shares' />}
        className='min-w-125px'
      />
    ),
    id: 'shares',
    Cell: ({...props}) => (
      <div className='row'>
        <div>
          {Number(props.data[props.row.index].value) /
            Number(props.data[props.row.index].Fund.shares_value) || ''}
          <KTIcon iconName='arrow-up' className=' ms-2 text-primary' />
        </div>
      </div>
    ),
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='status' />}
        className='min-w-125px ps-15'
      />
    ),
    id: 'status',
    Cell: ({...props}) => <StatusCell status={props.data[props.row.index].status || ''} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='created_date' />}
        className='min-w-125px'
      />
    ),
    accessor: 'createdAt',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='actions' />}
        className='text-end min-w-100px'
      />
    ),
    id: 'actions',
    Cell: ({...props}) => <ActionsCell data={props.data[props.row.index]} />,
  },
]

export {subscriptionsColumns}
