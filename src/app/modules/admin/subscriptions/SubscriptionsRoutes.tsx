import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {SubscriptionsListWrapper} from './SubscriptionsListWrapper'
// import PermissionForm from './add-edit/AddEditRole'
import {Form} from './add-edit/Form'
import Add from './add-edit/Add'
import Edit from './add-edit/Edit'
import {SubscriptionsAlarmsListWrapper} from '../subscription-alarms/SubscriptionsAlarmsListWrapper'
import {UserSubscriptionsListWrapper} from '../userSubscriptions/UserSubscriptionsListWrapper'
import UserEdit from '../userSubscriptions/add-edit/Edit'
import UserAdd from '../userSubscriptions/add-edit/Add'

const SubScriptionsRoutes = () => {
  const intl = useIntl()
  const fundsBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'fund_management'}),
      path: 'admin/funds-management/funds',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]

  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/subscriptions'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'subscriptions_list'})}
              </PageTitle>
              <SubscriptionsListWrapper />
            </>
          }
        />
        <Route
          path='/edit-subscription'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'edit_subscription'})}
              </PageTitle>
              <Edit />
            </>
          }
        />
        <Route
          path='/add-subscription'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'add_subscription'})}
              </PageTitle>
              <Add />
            </>
          }
        />
        <Route
          path='/subscriptions-alarms'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'subscriptions_alarms_list'})}
              </PageTitle>
              <SubscriptionsAlarmsListWrapper />
            </>
          }
        />
        <Route
          path='/subscription-requests'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'subscription_requests_list'})}
              </PageTitle>
              <SubscriptionsListWrapper />
            </>
          }
        />

        <Route
          path='/user-subscriptions'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'subscriptions_list'})}
              </PageTitle>
              <UserSubscriptionsListWrapper />
            </>
          }
        />
        <Route
          path='/edit-user-subscription'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'edit_subscription'})}
              </PageTitle>
              <UserEdit />
            </>
          }
        />
        <Route
          path='/add-user-subscription'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'add_subscription'})}
              </PageTitle>
              <UserAdd />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/subscriptions-management/subscriptions' />} />
    </Routes>
  )
}

export default SubScriptionsRoutes
