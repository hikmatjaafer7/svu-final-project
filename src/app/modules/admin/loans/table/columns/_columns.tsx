// @ts-nocheck
import {Column} from 'react-table'
import {Loan} from '../../core/_models'
import {ActionsCell} from './ActionCell'
import {Localize} from '../../../../../../_metronic/i18n/Localize'
import * as Columns from '../../../../../components/Table/columns'
import {StatusCell} from '../../../../../components/Table/columns/StatusCell'

const loansColumns: ReadonlyArray<Column<Loan>> = [
  {
    Header: (props) => <Columns.SelectionHeader tableProps={props} />,
    id: 'selection',
    Cell: ({...props}) => <Columns.SelectionCell id={props.data[props.row.index].id} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='loaned_name' />}
        className='min-w-125px'
      />
    ),
    id: 'user_name',
    Cell: ({...props}) => <div>{props.data[props.row.index]?.User?.first_name + ' ' + props.data[props.row.index]?.User?.last_name}</div>,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='fund_name' />}
        className='min-w-125px'
      />
    ),
    id: 'fund_name',
    Cell: ({...props}) => <div>{props.data[props.row.index].Fund.name}</div>,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='value' />}
        className='min-w-125px'
      />
    ),
    id: 'value',
    accessor: 'value',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='status' />}
        className='min-w-125px ps-15'
      />
    ),
    id: 'status',
    Cell: ({...props}) => <StatusCell status={props.data[props.row.index].status || ''} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='type' />}
        className='min-w-125px'
      />
    ),
    id: 'type',
    Cell: ({...props}) => (
      <>
        {' '}
        {props.data[props.row.index].type === 'emergency'
          ? 'طارئ'
          : props.data[props.row.index].type === 'ordinary'
          ? 'عادي'
          : ''}
      </>
    ),
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='transferDate' />}
        className='min-w-125px'
      />
    ),
    accessor: 'transferDate',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='created_date' />}
        className='min-w-125px'
      />
    ),
    accessor: 'createdAt',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='actions' />}
        className='text-end min-w-100px'
      />
    ),
    id: 'actions',
    Cell: ({...props}) => <ActionsCell data={props.data[props.row.index]} />,
  },
]

export {loansColumns}
