import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {Loan, LoansQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
import {post} from '../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const LOAN_URL = `${API_URL}/loans`
const GET_LOANS_URL = `${API_URL}/loans/query`

const getLoans = async (query: string) => {
  const data = await getList(`${LOAN_URL}?` + query)
  return data
  // .then((d: AxiosResponse<LoansQueryResponse>) => d)
}

const getLoanById = (id: ID): Promise<Loan | undefined> => {
  return axios
  .get(`${LOAN_URL}/${id}`)
  .then((response: AxiosResponse<Response<Loan>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: Response<Loan>) => response.data)
}

const getLoansByFundId = async ({fundId, type}, query: string) => {
  console.log('query :>> ', query)
  const data = await getList(`${LOAN_URL}/fund/${fundId}/${type}?` + query)

  console.log('data :>> ', data)
  return {data: data}
  // .then((d: AxiosResponse<LoansQueryResponse>) => d)
}
const createLoan = (loan: Loan): Promise<ResponeApiCheck | undefined> => {
  return create(`${LOAN_URL}/`, loan)
  // return axios
  //   .put(LOAN_URL, loan)
  //   .then((response: AxiosResponse<Response<Loan>>) => response.data)
  //   .then((response: Response<Loan>) => response.data)
}

const updateLoan = (loan: Loan): Promise<ResponeApiCheck | undefined> => {
  return axios
  .put(`${LOAN_URL}/${loan.id}`, loan)
  .then((response: AxiosResponse<Response<ResponeApiCheck>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: ResponeApiCheck) => {
    console.log('response :>> ', response)
    return response
  })
}

const deleteLoan = async (loanId: ID) => {
  return await dele(`${LOAN_URL}/`, loanId)
}

const deleteSelectedLoans = (loanIds: Array<ID>): Promise<void> => {
  const requests = loanIds.map((id) => axios.delete(`${LOAN_URL}/${id}`))
  return axios.all(requests).then(() => {})
}

export {
  getLoans,
  deleteLoan,
  deleteSelectedLoans,
  getLoanById,
  createLoan,
  updateLoan,
  getLoansByFundId,
}
