import {ID, Response} from '../../../../../_metronic/helpers'
import RequestStatus from '../../../../core/enums/RequestStatus'
export type Loan = {
  id?: ID
  value: string
  UserId: ID
  FundId: ID
  status?: RequestStatus
  reason?: string
  createdAt?: Date
  updatedAt?: Date | null
  type: string
}

export type LoansQueryResponse = Response<Array<Loan>>

export const initialLoan: Loan = {
  value: '',
  status: RequestStatus.Pending,
  UserId: 0,
  FundId: 0,
  type: 'ordinary',
}
