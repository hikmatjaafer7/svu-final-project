import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {LoansListWrapper} from './LoansListWrapper'
// import PermissionForm from './add-edit/AddEditRole'
import {Form} from './add-edit/Form'
import Add from './add-edit/Add'
import Edit from './add-edit/Edit'
import {LoansAlarmsListWrapper} from '../loan-alarms/LoansAlarmsListWrapper'
import {UserLoansListWrapper} from '../userLoans/UserLoansListWrapper'
import UserAdd from '../userLoans/add-edit/Add'
import UserEdit from '../userLoans/add-edit/Edit'

const LoansRoutes = () => {
  const intl = useIntl()
  // const loansBreadcrumbs: Array<PageLink> = [
  //   {
  //     title: intl.formatMessage({id: 'loans_management'}),
  //     path: 'admin/loans-management/loans',
  //     isSeparator: false,
  //     isActive: false,
  //   },
  //   {
  //     title: '',
  //     path: '',
  //     isSeparator: true,
  //     isActive: false,
  //   },
  // ]
  const fundsBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'fund_management'}),
      path: 'admin/funds-management/funds',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/loans'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'loans_list'})}
              </PageTitle>
              <LoansListWrapper />
            </>
          }
        />
        <Route
          path='/edit-loan'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'edit_loan'})}
              </PageTitle>
              <Edit />
            </>
          }
        />
        <Route
          path='/add-loan'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'add_loan'})}
              </PageTitle>
              <Add />
            </>
          }
        />
        <Route
          path='/loans-alarms'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'loans_alarms_list'})}
              </PageTitle>
              <LoansAlarmsListWrapper />
            </>
          }
        />

        <Route
          path='/loan-requests'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'loan_requests_list'})}
              </PageTitle>
              <LoansListWrapper />
            </>
          }
        />

        <Route
          path='/user-loans'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'loans_list'})}
              </PageTitle>
              <UserLoansListWrapper />
            </>
          }
        />
        <Route
          path='/edit-user-loan'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'edit_loan'})}
              </PageTitle>
              <UserEdit />
            </>
          }
        />
        <Route
          path='/add-user-loan'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'add_loan'})}
              </PageTitle>
              <UserAdd />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/loans-management/loans' />} />
    </Routes>
  )
}

export default LoansRoutes
