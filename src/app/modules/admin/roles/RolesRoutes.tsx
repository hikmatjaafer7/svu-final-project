import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {RolesListWrapper} from './RolesListWrapper'
import Edit from './add-edit/Edit'
import Add from './add-edit/Add'
import Permission from './add-edit/Permissions/index'

const RolesRoutes = () => {
  const intl = useIntl()
  const usersBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'roles_management'}),
      path: 'admin/roles-management/roles',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/roles'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'roles_list'})}
              </PageTitle>
              <RolesListWrapper />
            </>
          }
        />

        <Route
          path='/edit-role'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'edit_role'})}
              </PageTitle>
              <Edit />
            </>
          }
        />

        <Route
          path='/add-role'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'add_role'})}
              </PageTitle>
              <Add />
            </>
          }
        />

        <Route
          path='/manage-role-permissions'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'manage_role_permissions'})}
              </PageTitle>
              <Permission />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/roles-management/roles' />} />
    </Routes>
  )
}

export default RolesRoutes
