import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {Role, UsersQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
import {post, put} from '../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const Role_URL = `${API_URL}/roles`
const GET_USERS_URL = `${API_URL}/users/query`

const getRoles = async (query: string) => {
  const data = await getList(`${Role_URL}/`)
  console.log('data1 :>> ', data)
  return data
  // .then((d: AxiosResponse<UsersQueryResponse>) => d)
}

const getRoleById = (id: ID): Promise<Role | undefined> => {
  return axios
  .get(`${Role_URL}/${id}`)
  .then((response: AxiosResponse<Response<Role>>) => response.data)
  .then((response: Response<Role>) => response.data)
}

const createRole = (role: Role): Promise<ResponeApiCheck | undefined> => {
  return create(`${Role_URL}/`, role)
}

const updateRole = (role: Role): Promise<ResponeApiCheck | undefined> => {
  return put(`${Role_URL}/${role.id}`, role).then((response: ResponeApiCheck) => {
    console.log('response :>> ', response)
    return response
  })
}

const deleteRoles = async (userId: ID) => {
  return await dele(`${Role_URL}/`, userId)
}

const deleteSelectedRoles = (userIds: Array<ID>): Promise<void> => {
  const requests = userIds.map((id) => axios.delete(`${Role_URL}/${id}`))
  return axios.all(requests).then(() => {})
}

const addPermissions = async (
  role_id: number | string,
  perms: Array<number>
): Promise<ResponeApiCheck | undefined> => {
  return await post(`${Role_URL}/add-permissions`, {role_id, perms}).then(
    (response: ResponeApiCheck) => {
      console.log('response :>> ', response)
      return response
    }
  )
}
export {
  getRoles,
  deleteRoles,
  deleteSelectedRoles,
  getRoleById,
  createRole,
  updateRole,
  addPermissions,
}
