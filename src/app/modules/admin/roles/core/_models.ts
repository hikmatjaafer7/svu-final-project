import {ID, Response} from '../../../../../_metronic/helpers'
export type Role = {
  id?: ID
  name?: string
  code?: string
  createdAt?: string
  updatedAt?: string
  permissions?: []
}

export type UsersQueryResponse = Response<Array<Role>>

export const initialRole: Role = {
  name: '',
  code: '',
  permissions: [],
}
