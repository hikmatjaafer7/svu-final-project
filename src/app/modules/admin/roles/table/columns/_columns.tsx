// @ts-nocheck
import {Column} from 'react-table'
import {UserInfoCell} from './UserInfoCell'
import {User} from '../../core/_models'
import { ActionsCell } from './ActionCell'
import * as Columns from '../../../../../components/Table/columns'

//  useMutation(() => deleteUser(id), {
//   // 💡 response of the mutation is passed to onSuccess
//   onSuccess: () => {
//     // ✅ update detail view directly
//     queryClient.invalidateQueries([`${QUERIES.USERS_LIST}-${query}`])
//   },
// })
const usersColumns: ReadonlyArray<Column<User>> = [
  {
    Header: (props) => <Columns.SelectionHeader tableProps={props} />,
    id: 'selection',
    Cell: ({...props}) => <Columns.SelectionCell id={props.data[props.row.index].id} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title='الاسم'
        className='min-w-125px'
      />
    ),
    accessor: 'name',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title='الكود'
        className='min-w-125px'
      />
    ),
    accessor: 'code',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader tableProps={props} title='الإجراءات' className='text-end min-w-100px' />
    ),
    id: 'actions',
    Cell: ({...props}) => (
      <ActionsCell data={props.data[props.row.index]}/>
    ),
  },
]

export {usersColumns}
