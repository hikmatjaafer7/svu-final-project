import {useIntl} from 'react-intl'
import * as Yup from 'yup'

export const RoleSchema = () => {
  const intl = useIntl()
  const validations = Yup.object().shape({
    name: Yup.string().required(intl.formatMessage({id: 'field_is_required'})),
    code: Yup.string().required(intl.formatMessage({id: 'field_is_required'})),
  })
  return validations
}
