import {FC, useEffect} from 'react'
import {useState} from 'react'

import {Formik, useFormikContext} from 'formik'
import {useIntl} from 'react-intl'
import {QUERIES} from '../../../../../../_metronic/helpers'
import {useQuery} from 'react-query'
import {getPermissions} from '../../../permissions/core/_requests'
import ResetButton from '../../../../../components/ResetButton'
import {Loading} from '../../../../../components/Loading'
import SubmitButton from '../../../../../components/SubmitButton'
import {Tabs, Tab, Form, Row, Col} from 'react-bootstrap'
import {KTCard, KTCardBody, ResponeApiCheck} from '../../../../../../_metronic/helpers'
import {useLocation, useNavigate} from 'react-router-dom'
import {useNotification} from '../../../../../../_metronic/hooks/useNotification'
import {addPermissions} from '../../core/_requests'
interface Props {
  role_name: string
  role_id: number
  user?: boolean
}
const PermissionForm: FC<Props> = ({role_name, role_id, user = false}) => {
  const [activeKey, setActiveKey] = useState('ads')
  const intel = useIntl()
  const [permsWithGroups, setPermsWithGroups] = useState([])
  const {data: permissionList} = useQuery(
    `${QUERIES.PERMISSIONS_LIST}`,
    () => {
      return getPermissions('')
    },
    {cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false}
  )

  function formatTitle(title) {
    if (!title || title === null) return title
    return title
  }
  useEffect(() => {
    if (permissionList && permissionList?.data) {
      let groupsPerms = []
      permissionList.data.map((perm) => {
        if (perm.group_id === null || perm.group_id === 0) {
          let gpIndex = groupsPerms.findIndex((gp) => gp.id === 0)
          if (gpIndex === -1)
            groupsPerms.push({
              group_id: 0,
              group_name: 'others',
              permissions: [{id: perm.id, name: perm.name}],
            })
          else {
            let pIndex = groupsPerms[gpIndex].permissions.findIndex((p) => p.id === perm.id)
            if (pIndex === -1) {
              groupsPerms[gpIndex].permissions.push({id: perm.id, name: perm.name})
            } else {
              groupsPerms[gpIndex].permissions[pIndex] = {id: perm.id, name: perm.name}
            }
          }
        } else {
          let gpIndex = groupsPerms.findIndex((gp) => gp.group_id === perm.Group.id)
          if (gpIndex === -1)
            groupsPerms.push({
              group_id: perm.Group.id,
              group_name: perm.Group.name,
              permissions: [{id: perm.id, name: perm.name}],
            })
          else {
            let pIndex = groupsPerms[gpIndex].permissions.findIndex((p) => p.id === perm.id)
            if (pIndex === -1) {
              groupsPerms[gpIndex].permissions.push({id: perm.id, name: perm.name})
            } else {
              groupsPerms[gpIndex].permissions[pIndex] = {id: perm.id, name: perm.name}
            }
          }
        }
        // let permGroup =
      })

      if (groupsPerms.length > 0) setActiveKey(groupsPerms[0].group_id)
      setPermsWithGroups(groupsPerms)
    }
  }, [permissionList])
  const {handleSubmit, resetForm, isSubmitting, isValid, touched, values, setValues} =
    useFormikContext()
  console.log('permsWithGroups :>> ', permsWithGroups)
  return (
    <>
      {permsWithGroups && permsWithGroups.length > 0 && (
        <form className='form' onSubmit={handleSubmit} noValidate encType='multipart/form-data'>
          {/* begin::Scroll */}
          <div className='d-flex flex-column scroll-y me-n7 pe-7'>
            <div className='row'>
              <Tabs
                className='mb-3 custom-tabs'
                id='uncontrolled-tab-example'
                activeKey={activeKey}
                onSelect={(k) => setActiveKey(k)}
              >
                {permsWithGroups &&
                  permsWithGroups?.map((group, index) => (
                    <Tab
                      className='mb-3 tab-content'
                      eventKey={group.group_id}
                      title={formatTitle(group.group_name)}
                    >
                      <Form>
                        {index > 0 && <div className='separator separator-dashed my-5'></div>}
                        <Row className='mb-3 mt-5'>
                          <Col>
                            <h5>{formatTitle(group.group_name)}</h5>
                            <Row>
                              {group.permissions.map((item) => (
                                <Col key={item.code} sm={6} md={4} lg={3} className='mb-3'>
                                  <Form.Check
                                    type='checkbox'
                                    label={item.name}
                                    value={item.id}
                                    onChange={(event) => {
                                      console.log('item :>> ', item)
                                      if (values && Array.isArray(values)) {
                                        let val = values.find((x) => x === item.id)
                                        if (val) setValues(values.filter((x) => x !== item.id))
                                        else setValues([...values, item.id])
                                      }
                                    }}
                                    checked={Array.isArray(values) && values.includes(item.id)}
                                  />
                                </Col>
                              ))}
                            </Row>
                          </Col>
                        </Row>
                      </Form>
                    </Tab>
                  ))}
              </Tabs>
            </div>
          </div>
          {/* begin::Actions */}
          <div className='text-center pt-15'>
            <ResetButton resetForm={resetForm} isSubmitting={isSubmitting} />
            <SubmitButton isSubmitting={isSubmitting} isValid={isValid} touched={touched} />
          </div>
        </form>
      )}
      {(isSubmitting || (!permissionList && permissionList?.data)) && <Loading />}
    </>
  )
}

export default function Permission() {
  const navigate = useNavigate()
  const {showNotification} = useNotification()
  const location = useLocation()
  const {permissions, id, name}: any = location.state

  return (
    <KTCard>
      <KTCardBody className='py-4'>
        <Formik
          enableReinitialize={true}
          // validationSchema={roleSchema}
          initialValues={[...permissions?.map((x) => x.id)]}
          initialStatus={{edit: false}}
          onSubmit={async (values, {setSubmitting}) => {
            console.log('values ...s', values)
            setSubmitting(true)

            try {
              const res = await addPermissions(id, values)
              if (res.result == 'success') {
                navigate('/admin/roles-management/roles')
              }

              showNotification(res)
            } catch (ex) {
              showNotification({error_description: ex, data: 'error', result: 'error'})
              console.error(ex)
            } finally {
              setSubmitting(true)
            }
          }}
          onReset={(values) => {
            console.log('Formik onReset')
          }}
        >
          <PermissionForm role_name={name} role_id={id} user />
        </Formik>
      </KTCardBody>
    </KTCard>
  )
}

export {Permission}
