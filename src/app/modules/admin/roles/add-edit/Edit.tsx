import {Formik} from 'formik'
import {
  KTCard,
  KTCardBody,
  ResponeApiCheck,
  initialResponseError,
} from '../../../../../_metronic/helpers'
import {Form} from './Form'
import {RoleSchema} from './validationForm'
import {updateRole} from '../core/_requests'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {useLocation, useNavigate} from 'react-router-dom'

const Edit = () => {
  const navigate = useNavigate()
  const {showNotification} = useNotification()
  const location = useLocation()
  const data: any = location.state
  return (
    <KTCard>
      <KTCardBody className='py-4'>
        {data && (
          <Formik
            enableReinitialize={true}
            validationSchema={RoleSchema()}
            initialValues={data}
            initialStatus={{edit: false}}
            onSubmit={async (values: any, {setSubmitting}) => {
              setSubmitting(true)
              try {
                const res: ResponeApiCheck = await updateRole(values)
                if (res.result == 'success') {
                  navigate('/admin/roles-management/roles')
                }
                showNotification(res)
              } catch (ex) {
                showNotification({error_description: ex, ...initialResponseError})
                console.error(ex)
              } finally {
                setSubmitting(true)
              }
            }}
            onReset={(values) => {}}
          >
            <Form />
          </Formik>
        )}
      </KTCardBody>
    </KTCard>
  )
}

export default Edit
