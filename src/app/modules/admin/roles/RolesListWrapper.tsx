import {UsersTable} from './table'
import {KTCard, QUERIES} from '../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
} from '../../../core/GenericQueryResonseProvider'
import {GenericQueryRequestProvider} from '../../../core/GenericQueryRequestProvider'
import {getRoles} from './core/_requests'
import {ListHeader} from '../../../components/Table/header/ListHeader'
import { useNavigate } from 'react-router-dom'

const RolesList = () => {
  const {itemIdForUpdate} = useGenericListView()
  // const {selected} = useGenericListView()
  const navigate=useNavigate()
  return (
    <>
      <KTCard>
        <ListHeader
          handleAdd={() => {
            navigate('/admin/roles-management/add-role')
          }}
          onFilterSubmit={(filterUpdateState) => {}}
          ListFilterChildren={<></>}
          queryString={QUERIES.ROLES_LIST}
          deleteSelected={(selected) => {}}
        />
        <UsersTable />
      </KTCard>
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <RolesList />
    </GenericListViewProvider>
  )
}

const RolesListWrapper = () => {
  return (
    <GenericQueryRequestProvider>
      <GenericQueryResponseProvider getApi={getRoles} queryString={QUERIES.ROLES_LIST}>
        <ListView />
      </GenericQueryResponseProvider>
    </GenericQueryRequestProvider>
  )
}

export {RolesListWrapper}
