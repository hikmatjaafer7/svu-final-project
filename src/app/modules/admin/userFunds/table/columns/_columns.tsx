// @ts-nocheck
import {Column} from 'react-table'
import {UserInfoCell} from './UserInfoCell'
import {User} from '../../core/_models'
import {ActionsCell} from './ActionCell'
import {Localize} from '../../../../../../_metronic/i18n/Localize'
import * as Columns from '../../../../../components/Table/columns'

const usersColumns: ReadonlyArray<Column<any>> = [
  // {
  //   Header: (props) => <Columns.SelectionHeader tableProps={props} />,
  //   id: 'selection',
  //   Cell: ({...props}) => <Columns.SelectionCell id={props.data[props.row.index].id} />,
  // },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='name' />}
        className='min-w-125px'
      />
    ),
    id: 'name',
    accessor: 'name',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='duration_in_month' />}
        className='min-w-125px'
      />
    ),
    accessor: 'duration_in_month',
  },

  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='start_date' />}
        className='min-w-125px'
      />
    ),
    id: 'start_date',
    accessor: 'start_date',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='end_date' />}
        className='min-w-125px'
      />
    ),
    id: 'end_date',
    accessor: 'end_date',
  },
  // {
  //   Header: (props) => (
  //     <Columns.CustomHeader
  //       tableProps={props}
  //       title={<Localize value='subscriber_number' />}
  //       className='min-w-125px'
  //     />
  //   ),
  //   id: 'subscriber_number',
  //   Cell: ({...props}) => (
  //     <div>
  //       {new Set(
  //         props?.data[props.row.index]?.Subscriptions?.map((subscription) => subscription?.user_id)
  //       )?.size || ''}
  //     </div>
  //   ),
  // },
  // {
  //   Header: (props) => (
  //     <Columns.CustomHeader
  //       tableProps={props}
  //       title={<Localize value='subscribers_allowed_number' />}
  //       className='min-w-125px'
  //     />
  //   ),
  //   id: 'subscribers_allowed_number',
  //   accessor: 'subscribers_allowed_number',
  // },
  // {
  //   Header: (props) => (
  //     <Columns.CustomHeader
  //       tableProps={props}
  //       title={<Localize value='participant_shares_allowed_number' />}
  //       className='min-w-125px'
  //     />
  //   ),
  //   id: 'participant_shares_allowed_number',
  //   accessor: 'participant_shares_allowed_number',
  // },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='shares_value' />}
        className='min-w-125px'
      />
    ),
    id: 'shares_value',
    accessor: 'shares_value',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='fees' />}
        className='min-w-125px'
      />
    ),
    accessor: 'fees',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='actions' />}
        className='text-end min-w-100px'
      />
    ),
    id: 'actions',
    Cell: ({...props}) => <ActionsCell data={props.data[props.row.index]} />,
  },
]

export {usersColumns}
