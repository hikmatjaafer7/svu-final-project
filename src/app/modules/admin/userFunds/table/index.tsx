import {useMemo} from 'react'
import {usersColumns} from './columns/_columns'
import Table from '../../../../components/Table'
import {useGenericQueryRequest} from '../../../../core/GenericQueryRequestProvider'
import {
  useGenericQueryResponseData,
  useGenericQueryResponseLoading,
  useGenericQueryResponsePagination,
} from '../../../../core/GenericQueryResonseProvider'
import {KTCardBody} from '../../../../../_metronic/helpers'

const UsersTable = () => {
  const users = useGenericQueryResponseData()
  const isLoading = useGenericQueryResponseLoading()
  const pagination = useGenericQueryResponsePagination()
  const {updateState} = useGenericQueryRequest()
  const data = useMemo(() => users, [users])
  const columns = useMemo(() => usersColumns, [])

  return (
    <>
      <KTCardBody className='py-4'>
        <Table
          columns={columns}
          data={data}
          isLoading={isLoading}
          pagination={pagination}
          updateState={updateState}
        />
      </KTCardBody>
    </>
  )
}

export {UsersTable}
