import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {UserFundsListWrapper} from './UserFundsListWrapper'
import UserFundDetails from './details'
// import PermissionForm from './add-edit/AddEditRole'

const UserFundsRoutes = () => {
  const intl = useIntl()
  const usersBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'user_management'}),
      path: 'admin/user-funds',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/user-funds'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'funds_list'})}
              </PageTitle>
              <UserFundsListWrapper />
            </>
          }
        />
         <Route
          path='/user-funds/details'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'fund_details'})}
              </PageTitle>
              <UserFundDetails />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/users-management/user-funds' />} />
    </Routes>
  )
}

export default UserFundsRoutes
