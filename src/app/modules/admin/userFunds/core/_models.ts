import {ID, Response} from '../../../../../_metronic/helpers'
import {Role, initialRole} from '../../roles/core/_models'
export type User = {
  id?: ID
  first_name?: string
  last_name?: string
  email?: string
  avatar?: string
  password?: string
  phone_number?: string
  civil_registry?: string
  Bank_account_number?: string
  enabled?: boolean
  last_login?: Date | null
  createdAt?: Date
  updatedAt?: Date | null
  role_id: number
  Role: Role
  image?:string
}

export type UsersQueryResponse = Response<Array<User>>

export const initialUser: User = {
  avatar: '',
  first_name: '',
  last_name: '',
  email: '',
  password: '',
  phone_number: '',
  civil_registry: '',
  Bank_account_number: '',
  enabled: true,
  last_login: null,
  createdAt: new Date(),
  updatedAt: null,
  role_id: 1,
  Role: initialRole,
}
