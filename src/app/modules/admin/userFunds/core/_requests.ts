import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {User, UsersQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
import {get, post} from '../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const FUND_URL = `${API_URL}/funds/users`

const getUserFunds = async (user_id: ID, query: string) => {
  let data = await getList(`${FUND_URL}/${user_id}?` + query)
  let tempData = data?.data?.map((d) => {
    return {...d, user_id}
  })
  return {data: tempData}
}

const getAccountStatementDetails = async (fund_id: ID, user_id: ID, query: string) => {
  const data = await getList(`${API_URL}/funds/${fund_id}/users/${user_id}?` + query)
  console.log('AccountStatement data API :>> ', data)
  return data
}

export {getUserFunds, getAccountStatementDetails}
