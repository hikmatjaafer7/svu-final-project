import {useIntl} from 'react-intl'
import {KTCard, KTCardBody, QUERIES} from '../../../../../_metronic/helpers'
import {useQuery} from 'react-query'
import {useLocation} from 'react-router-dom'
import {getAccountStatementDetails} from '../core/_requests'
import Table from '../../../../components/Table'
import React, {useMemo} from 'react'
import {usersColumns} from './columns/_columns'

const UserFundDetails = () => {
  const intl = useIntl()
  const location: any = useLocation()
  const data: any = location.state
  const {fund_id, user_id} = data

  const {
    data: accountData,
    isLoading,
    refetch,
  } = useQuery(
    `${QUERIES.ACCOUNT_STATEMENT_USER_FUND_DETAILS}-${fund_id}-${user_id}`,
    () => {
      return getAccountStatementDetails(fund_id, user_id, '')
    },
    {
      refetchOnMount: true,
    }
  )
  // const columns = useMemo(() => usersColumns, [])
  const columns = [
    {Header: 'Year', accessor: 'year'},
    {Header: 'Total Sub', accessor: 'sub_payments'},
    {Header: 'Total Ordinary', accessor: 'ordinary_payments'},
    {Header: 'Total Emergency', accessor: 'emergency_payments'},
  ]

  const monthsColumns = [
    {Header: 'Month', accessor: 'name'},
    {Header: 'Sub Total Payments', accessor: 'sub_total_payments'},
    {Header: 'Ordinary Loan Payments', accessor: 'ordinary_loan_total_payments'},
    {Header: 'Emergency Loan Payments', accessor: 'emergency_loan_total_payments'},
  ]
  const months = [
    {
      number: 1,
      name: 'يناير',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 2,
      name: 'فبراير',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 3,
      name: 'مارس',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 4,
      name: 'أبريل',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 5,
      name: 'مايو',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 6,
      name: 'يونيو',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 7,
      name: 'يوليو',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 8,
      name: 'أغسطس',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 9,
      name: 'سبتمبر',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 10,
      name: 'أكتوبر',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 11,
      name: 'نوفمبر',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
    {
      number: 12,
      name: 'ديسمبر',
      sub_total_payments: 0,
      ordinary_loan_total_payments: 0,
      emergency_loan_total_payments: 0,
    },
  ]

  return (
    <div>
      {accountData?.data ? (
        <>
          {' '}
          <div className=' card border border-opacity-25 border-primary mb-6 '>
            <div className='card-header align-items-center'>
              <h1>{intl.formatMessage({id: 'account_statement'})}</h1>
            </div>

            <div className='card-body row'>
              <div className='col-xl-3 col-6 mb-6'>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'user_name'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.user_name}
                    </span>
                  </div>
                </div>{' '}
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'fund_name'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>{accountData?.data?.name}</span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'shares_value'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.shares_value}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'initial_balance'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.initial_balance}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'start_date'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.start_date}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'end_date'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.end_date}
                    </span>
                  </div>
                </div>
              </div>

              <div className='col-xl-3 col-6 mb-6'>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'loan_total_value'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-danger'>
                      {accountData?.data?.loan_total_value}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'loan_total_payments'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-success'>
                      {accountData?.data?.loan_total_payments}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'loan_remaining_repayments'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.loan_remaining_repayments}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'fees'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-danger'>
                      {accountData?.data?.total_fees}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'net_balance'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-primary'>
                      {accountData?.data?.net_balance}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'zakat_amount'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.zakat_amount}
                    </span>
                  </div>
                </div>{' '}
              </div>

              <hr className='d-xl-none ' />

              <div className='col-xl-3 col-6 mb-6'>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'subscription_total_value'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-danger'>
                      {accountData?.data?.sub_total_value}
                    </span>
                  </div>
                </div>{' '}
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'subscription_total_payments'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-success'>
                      {accountData?.data?.sub_total_payments}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'ordinary_loan_total_value'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-danger'>
                      {accountData?.data?.ordinary_loan_total_value}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'ordinary_loan_total_payments'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-success'>
                      {accountData?.data?.ordinary_loan_total_payments}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'emergency_loan_total_value'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-danger'>
                      {accountData?.data?.emergency_loan_total_value}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'emergency_loan_total_payments'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-success'>
                      {accountData?.data?.emergency_loan_total_payments}
                    </span>
                  </div>
                </div>
              </div>

              <div className='col-xl-3 col-6 mb-6'>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'subscription_count'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.sub_count}
                    </span>
                  </div>
                </div>{' '}
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'ordinary_loan_count'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.ordinary_loan_count}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'emergency_loan_count'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.emergency_loan_count}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'subscription_alarm_count'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.sub_alarm_count}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'ordinary_loan_alarm_count'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.ordinary_loan_alarm_count}
                    </span>
                  </div>
                </div>
                <div className='row mb-4'>
                  <label className='col-lg-6 fw-semibold text-gray-700'>
                    {intl.formatMessage({id: 'emergency_loan_alarm_count'})}
                  </label>
                  <div className='col-lg-6'>
                    <span className='fw-bold fs-6 text-gray-800'>
                      {accountData?.data?.emergency_loan_alarm_count}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <KTCard>
            <div className='card-header align-items-center'>
              <h1>تفاصيل الأسداد</h1>
            </div>
            <KTCardBody>
              <div className='table-responsive'>
                <table id='kt_table_users' className='table align-middle table-row-dashed fs-6 '>
                  <thead>
                    <tr className='text-start fw-bolder fs-6 text-uppercase gs-0'>
                      <th className='min-w-100px'>السنة</th>
                      {accountData?.data?.yearsReport?.map((yearReport: any) => {
                        return <th colSpan={3}>{yearReport?.year}</th>
                      })}
                    </tr>
                    <tr className='text-start fw-bolder fs-6 text-uppercase gs-0'>
                      <th>الشهر</th>
                      {accountData?.data?.yearsReport?.map((yearReport: any) => {
                        return (
                          <>
                            <th>الأسهم المدفوعة</th>
                            <th>سداد القرض العادي </th>
                            <th>سداد القرض الطارئ </th>
                          </>
                        )
                      })}
                    </tr>
                  </thead>
                  <hr />
                  <tbody className='text-gray-600 fw-bold'>
                    {months?.map((month: any) => {
                      return (
                        <tr>
                          <td className='text-primary '>
                            {month?.number} - {month?.name}
                          </td>
                          {accountData?.data?.yearsReport?.map((yearReport: any) => {
                            // return yearReport?.months?.fi((mon: any) => {

                            let mon = yearReport?.months?.find((m) => m.number === month.number)
                            return (
                              <>
                                <td>{mon?.sub_total_payments}</td>
                                <td>{mon?.ordinary_loan_total_payments}</td>
                                <td>{mon?.emergency_loan_total_payments}</td>
                              </>
                            )
                            // })
                          })}
                        </tr>
                      )
                    })}
                  </tbody>
                  <hr />
                  <tfoot>
                    <tr className='text-start fw-bolder fs-6 text-uppercase gs-0'>
                      <td>المجموع</td>
                      {accountData?.data?.yearsReport?.map((yearReport: any) => {
                        return (
                          <>
                            <td className='text-success fw-bold'>{yearReport?.sub_payments}</td>
                            <td className='text-success fw-bold'>
                              {yearReport?.ordinary_payments}
                            </td>
                            <td className='text-success fw-bold'>
                              {yearReport?.emergency_payments}
                            </td>
                          </>
                        )
                      })}
                    </tr>
                  </tfoot>
                </table>
              </div>
            </KTCardBody>
          </KTCard>
        </>
      ) : (
        <div className='d-flex justify-content-center align-items-center h-400px card'>
          <h1>{accountData?.message}</h1>
        </div>
      )}
    </div>
  )
}

export default UserFundDetails
