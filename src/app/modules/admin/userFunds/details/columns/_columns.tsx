// @ts-nocheck
import React from 'react'
import {Column} from 'react-table'
import {Localize} from '../../../../../../_metronic/i18n/Localize'
import * as Columns from '../../../../../components/Table/columns'

const usersColumns: ReadonlyArray<Column<any>> = [
  
  {
    Header: 'Basic Information', // Top-level header
    columns: [
      {
        Header: (props) => (
          <Columns.CustomHeader
            tableProps={props}
            title={<Localize value='name' />}
            className='min-w-125px'
          />
        ),
        id: 'name',
        accessor: 'name',
      },
      {
        Header: 'Duration', // Second-level header
        columns: [
          {
            Header: (props) => (
              <Columns.CustomHeader
                tableProps={props}
                title={<Localize value='duration_in_month' />}
                className='min-w-125px'
              />
            ),
            accessor: 'sub_count',
          },
          {
            Header: (props) => (
              <Columns.CustomHeader
                tableProps={props}
                title={<Localize value='start_date' />}
                className='min-w-125px'
              />
            ),
            id: 'start_date',
            accessor: 'start_date',
          },
          {
            Header: (props) => (
              <Columns.CustomHeader
                tableProps={props}
                title={<Localize value='end_date' />}
                className='min-w-125px'
              />
            ),
            id: 'end_date',
            accessor: 'end_date',
          },
        ],
      },
    ],
  },
  {
    Header: 'Shares', // Top-level header
    columns: [
      {
        Header: (props) => (
          <Columns.CustomHeader
            tableProps={props}
            title={<Localize value='shares_value' />}
            className='min-w-125px'
          />
        ),
        id: 'shares_value',
        accessor: 'shares_value',
      },
      {
        Header: (props) => (
          <Columns.CustomHeader
            tableProps={props}
            title={<Localize value='fees' />}
            className='min-w-125px'
          />
        ),
        accessor: 'total_fees',
      },
    ],
  },
]

export {usersColumns}
