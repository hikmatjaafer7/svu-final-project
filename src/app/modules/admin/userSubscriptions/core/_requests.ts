import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {Subscription, SubscriptionsQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
import {post} from '../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const SUBSCRIPTION_URL = `${API_URL}/subscriptions`
const GET_SUBSCRIPTIONS_URL = `${API_URL}/subscriptions/query`

const getSubscriptions = async (query: string) => {
  const data = await getList(`${SUBSCRIPTION_URL}?` + query)
  return data
  // .then((d: AxiosResponse<SubscriptionsQueryResponse>) => d)
}

const getSubscriptionById = (id: ID): Promise<Subscription | undefined> => {
  return axios
  .get(`${SUBSCRIPTION_URL}/${id}`)
  .then((response: AxiosResponse<Response<Subscription>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: Response<Subscription>) => response.data)
}

const getSubscriptionsByFundId = async (fundId: ID, query: string) => {
  const data = await getList(`${SUBSCRIPTION_URL}/fund/${fundId}?` + query)

  console.log('data :>> ', data)
  return {data: data}
  // .then((d: AxiosResponse<SubscriptionsQueryResponse>) => d)
}

const getSubscriptionsByUserId = async (userId: ID, query: string) => {
  const data = await getList(`${SUBSCRIPTION_URL}/user/${userId}?` + query)
  return {data: data}
  // .then((d: AxiosResponse<SubscriptionsQueryResponse>) => d)
}
const createSubscription = (fund: Subscription): Promise<ResponeApiCheck | undefined> => {
  return create(`${SUBSCRIPTION_URL}/`, fund)
  // return axios
  //   .put(SUBSCRIPTION_URL, fund)
  //   .then((response: AxiosResponse<Response<Subscription>>) => response.data)
  //   .then((response: Response<Subscription>) => response.data)
}

const updateSubscription = (fund: Subscription): Promise<ResponeApiCheck | undefined> => {
  return axios
  .put(`${SUBSCRIPTION_URL}/${fund.id}`, fund)
  .then((response: AxiosResponse<Response<ResponeApiCheck>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: ResponeApiCheck) => {
    console.log('response :>> ', response)
    return response
  })
}

const deleteSubscription = async (fundId: ID) => {
  return await dele(`${SUBSCRIPTION_URL}/`, fundId)
}

const deleteSelectedSubscriptions = (fundIds: Array<ID>): Promise<void> => {
  const requests = fundIds.map((id) => axios.delete(`${SUBSCRIPTION_URL}/${id}`))
  return axios.all(requests).then(() => {})
}

export {
  getSubscriptions,
  deleteSubscription,
  getSubscriptionsByUserId,
  deleteSelectedSubscriptions,
  getSubscriptionById,
  createSubscription,
  updateSubscription,
  getSubscriptionsByFundId,
}
