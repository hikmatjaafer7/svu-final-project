import {SubscriptionsTable} from './table'
import {KTCard, QUERIES} from '../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponseRefetch,
} from '../../../core/GenericQueryResonseProvider'
import {
  GenericQueryRequestProvider,
} from '../../../core/GenericQueryRequestProvider'
import {deleteSelectedSubscriptions, getSubscriptionsByFundId, getSubscriptionsByUserId} from './core/_requests'
import {ListHeader} from '../../../components/Table/header/ListHeader'
import {useState} from 'react'
import {Loading} from '../../../components/Loading'
import {useNotification} from '../../../../_metronic/hooks/useNotification'
import {useLocation, useNavigate} from 'react-router-dom'


const UserSubscriptionsList = () => {
  const {itemIdForUpdate} = useGenericListView()
  const refetch = useGenericQueryResponseRefetch()
  // const {selected} = useGenericListView()
  const [isLoading, setIsLoading] = useState(false)
  const {showNotification} = useNotification()
  const navigate = useNavigate()
  const location: any = useLocation()
  const state: any = location.state
  const submitUserId = state?.id || 0
  return (
    <>
      <KTCard>
        <ListHeader
          handleAdd={() => {
            navigate('/admin/subscriptions-management/add-user-subscription', {
              state: {id: submitUserId},
            })
          }}
          onFilterSubmit={(filterUpdateState) => {}}
          ListFilterChildren={<></>}
          queryString={QUERIES.SUBSCRIPTIONS_BY_USER_LIST}
          deleteSelected={(selected) => {
            setIsLoading(true)
            deleteSelectedSubscriptions(selected)
            .then((res) => {
              setIsLoading(false)
              showNotification({result: 'success', data: '', error_description: ''})
              refetch()
            })
            .catch((err) => {
              setIsLoading(false)
              showNotification({result: 'error', data: '', error_description: 'error'})
              refetch()
            })
          }}
        />
        <SubscriptionsTable />
        {isLoading && <Loading />}
      </KTCard>
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <UserSubscriptionsList />
    </GenericListViewProvider>
  )
}

const UserSubscriptionsListWrapper = () => {
  // const [fundId, setFundId] = useState(0)
  // const [, setSubmitUserId] = useState(0)
  const location: any = useLocation()
  const state: any = location.state
  const submitUserId = state?.id || 0

  // const {data: listFunds} = useQuery(
  //   QUERIES.FUNDS_LIST,
  //   () => {
  //     return getFunds('').then((resData) => {
  //       return [
  //         {
  //           key: 0,
  //           value: 'اختر من القائمة',
  //         },
  //         ...resData?.data?.map((item, index) => {
  //           return {
  //             key: item.id,
  //             value: item.name,
  //           }
  //         }),
  //       ]
  //     })
  //   },
  //   {
  //     refetchOnMount: true,
  //   }
  // )

  return (
    <>
      {/* <div>
        <KTCard>
          <div className='row m-4'>
            <div className='col-3'>
              <Select
                selectedValue={fundId}
                onChange={(fundId: any) => setFundId(fundId)}
                title='fund'
                options={listFunds || []}
              />
            </div>
            <div className='col-2 mt-7'>
              <Button
                Disabled={fundId <= 0}
                label='search'
                onClick={() => {
                  setSubmitUserId(fundId)
                }}
              />
            </div>
          </div>
        </KTCard>
      </div>
      <hr /> */}
      {submitUserId > 0 && (
        <GenericQueryRequestProvider>
          <GenericQueryResponseProvider
            getApi={getSubscriptionsByUserId}
            urlParameter={submitUserId.toString()}
            queryString={QUERIES.SUBSCRIPTIONS_BY_USER_LIST}
          >
            <ListView />
          </GenericQueryResponseProvider>
        </GenericQueryRequestProvider>
      )}
    </>
  )
}

export {UserSubscriptionsListWrapper}
