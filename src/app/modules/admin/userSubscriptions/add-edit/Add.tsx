import {Formik} from 'formik'
import {
  KTCard,
  KTCardBody,
  QUERIES,
  ResponeApiCheck,
  initialResponseError,
} from '../../../../../_metronic/helpers'
import {Form} from './Form'
import {RoleSchema} from './validationForm'
import {createSubscription} from '../core/_requests'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {useLocation, useNavigate} from 'react-router-dom'
import {initialSubscription} from '../core/_models'
import {useEffect, useState} from 'react'

const Add = () => {
  const navigate = useNavigate()
  const {showNotification} = useNotification()
  const location = useLocation()
  const data: any = location.state
  return (
    <KTCard>
      <KTCardBody className='py-4'>
        <Formik
          enableReinitialize={true}
          validationSchema={RoleSchema()}
          initialValues={{FundId: data?.id, ...initialSubscription}}
          initialStatus={{edit: false}}
          onSubmit={async (values: any, {setSubmitting}) => {
            setSubmitting(true)
            try {
              const res: ResponeApiCheck = await createSubscription({
                ...values,
                UserId: data?.id,
                user_id: data?.id,
                fund_id: values?.FundId,
              })
              console.log('res REsponse API CHECK :>> ', res)
              if (res.result == 'success') {
                navigate('/admin/subscriptions-management/user-subscriptions', {state: {id: data?.id}})
              }
              showNotification(res)
            } catch (ex) {
              showNotification({error_description: ex, ...initialResponseError})
              console.error(ex)
            } finally {
              setSubmitting(true)
            }
          }}
          onReset={(values) => {}}
        >
          <Form />
        </Formik>
      </KTCardBody>
    </KTCard>
  )
}

export default Add
