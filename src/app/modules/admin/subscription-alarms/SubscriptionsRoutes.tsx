import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {SubscriptionsAlarmsListWrapper} from './SubscriptionsAlarmsListWrapper'
// import PermissionForm from './add-edit/AddEditRole'

const SubScriptionsRoutes = () => {
  const intl = useIntl()
  const fundsBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'fund_management'}),
      path: 'admin/funds-management/subscriptions/alarms',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]

  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/subscriptions/alarms'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'subscriptions_alarms_list'})}
              </PageTitle>
              <SubscriptionsAlarmsListWrapper />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/funds-management/subscriptions/alarms' />} />
    </Routes>
  )
}

export default SubScriptionsRoutes
