import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {FundSubscriptionAlarms} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const SUBSCRIPTION_URL = `${API_URL}/funds/`

const SUBSCRIPTION_ALARMS_URL = `${API_URL}/subscription-alarms`
const GET_SUBSCRIPTIONS_URL = `${API_URL}/subscriptions/query`


const getFundSubscriptionAlarmsByFundId = async (fundId: ID, query: string) => {
  const data = await getList(`${SUBSCRIPTION_URL}${fundId}/subscription/alarms?` + query)

  console.log('data :>> ', data)
  return {data: data}
  // .then((d: AxiosResponse<FundSubscriptionAlarmssQueryResponse>) => d)
}


export {
  getFundSubscriptionAlarmsByFundId,
}
