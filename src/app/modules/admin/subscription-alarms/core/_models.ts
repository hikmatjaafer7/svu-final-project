import {ID, Response} from '../../../../../_metronic/helpers'
import RequestStatus from '../../../../core/enums/RequestStatus'

interface SubscriptionAlarm {
  id: number
  description: string
  createdAt: Date | null
  updatedAt: Date | null
  subscription_id: number
}

interface User {
  id: number
  name: string
  email: string
}

export interface FundSubscriptionAlarms {
  id: number
  fund_id: number
  fund_name: string
  value: number
  status: string
  reason: string
  createdAt: string
  Alarms: SubscriptionAlarm[]
  User: User | null
}

export const initFundSubscriptionAlarms: FundSubscriptionAlarms = {
  id: 0,
  fund_id: 0,
  fund_name: '',
  value: 0,
  status: '',
  reason: '',
  createdAt: '',
  Alarms: [],
  User: null,
}

export type FundSubscriptionAlarmsQueryResponse = Response<Array<FundSubscriptionAlarms>>
