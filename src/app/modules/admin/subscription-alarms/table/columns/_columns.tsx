// @ts-nocheck
import {Column} from 'react-table'
import {Subscription} from '../../core/_models'
import {ActionsCell} from './ActionCell'
import {Localize} from '../../../../../../_metronic/i18n/Localize'
import * as Columns from '../../../../../components/Table/columns'
import {StatusCell} from '../../../../../components/Table/columns/StatusCell'
import {KTIcon} from '../../../../../../_metronic/helpers'

const subscriptionsColumns: ReadonlyArray<Column<Subscription>> = [
  {
    Header: (props) => <Columns.SelectionHeader tableProps={props} />,
    id: 'selection',
    Cell: ({...props}) => <Columns.SelectionCell id={props.data[props.row.index].id} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='subscription_id' />}
        className='min-w-125px'
      />
    ),
    accessor: 'id',
    // Cell: ({...props}) => <div>{props.data[props.row.index].id}</div>,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='fund_name' />}
        className='min-w-125px'
      />
    ),
    accessor: 'fund_name',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='user_name' />}
        className='min-w-125px'
      />
    ),
    accessor: 'User.name',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='alarms_count' />}
        className='min-w-125px'
      />
    ),
    id: 'alarms_count',
    Cell: ({...props}) => <div>{props?.data[props.row.index]?.Alarms?.length || 0}</div>,
  },
  // {
  //   Header: (props) => (
  //     <Columns.CustomHeader
  //       tableProps={props}
  //       title={<Localize value='alarm_created_date' />}
  //       className='min-w-125px'
  //     />
  //   ),
  //   id: 'createdAt',
  //   Cell: ({...props}) => <div>{props?.data[props.row.index]?.Alarms?.length || 0}</div>,
  // },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='actions' />}
        className='text-end min-w-100px'
      />
    ),
    id: 'actions',
    Cell: ({...props}) => <ActionsCell data={props.data[props.row.index]} />,
  },
]

export {subscriptionsColumns}
