/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC, ReactNode, useEffect, useState} from 'react'
import {MenuComponent} from '../../../../../../_metronic/assets/ts/components'
import {ID, KTIcon, QUERIES} from '../../../../../../_metronic/helpers'
import {useNavigate} from 'react-router-dom'
import {useMutation, useQueryClient} from 'react-query'
import {useGenericQueryResponse} from '../../../../../core/GenericQueryResonseProvider'
import {useIntl} from 'react-intl'
import {ModalList} from '../../../../../components/modals/InviteUsers'

type Props = {
  data: any
  menuItems: {label: string; onClick: any; menuElement?: ReactNode}[]
}

const ActionsCell: FC<Props> = ({data}) => {
  useEffect(() => {
    MenuComponent.reinitialization()
  }, [])
  const intl = useIntl()
  const {query} = useGenericQueryResponse()

  const queryClient = useQueryClient()


  return (
    <>
      <a
        href='#'
        className='btn btn-light btn-active-light-primary btn-sm'
        data-kt-menu-trigger='click'
        data-kt-menu-placement='bottom-end'
      >
        {intl.formatMessage({id: 'actions'})}
        <KTIcon iconName='down' className='fs-5 m-0' />
      </a>
      {/* begin::Menu */}
      <div
        className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4'
        data-kt-menu='true'
      >
        {/* begin::Menu items */}
        <div className='menu-item px-3'>
          {
            <a
              data-bs-toggle='modal'
              data-bs-target={'#' + 'kt_modal_list_user_alarms_payments'}
              className='menu-link px-3'
              onClick={() => {}}
            >
              عرض الإنذارات
            </a>
          }
        </div>
        {/* end::Menu item */}
      </div>
      <ModalList
        user_name={data?.fund_name}
        list={
          data?.Alarms?.map((alarm) => {
            return {
              amount: alarm.description,
              updatedAt: alarm.createdAt,
            }
          }) || []
        }
        title={`إنذارات المشترك : ${data?.User.name}`}
        description={`الصندوق : ${data?.fund_name}  |  الإشتراك رقم : ${data?.id}`}
        id='kt_modal_list_user_alarms_payments'
      />
      {/* end::Menu */}
    </>
  )
}

export {ActionsCell}
