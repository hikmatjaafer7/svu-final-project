import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {PermissionsListWrapper} from './PermissionsListWrapper'
// import PermissionForm from './add-edit/AddEditRole'
import {Form} from './add-edit/Form'
import Add from './add-edit/Add'
import Edit from './add-edit/Edit'

const PermissionsRoutes = () => {
  const intl = useIntl()
  const permissionsBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'permission_management'}),
      path: 'admin/roles-management/permissions',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/permissions'
          element={
            <>
              <PageTitle breadcrumbs={permissionsBreadcrumbs}>
                {intl.formatMessage({id: 'permissions_list'})}
              </PageTitle>
              <PermissionsListWrapper />
            </>
          }
        />
        {/* 
        <Route
          path='assign-permission-role'
          element={
            <>
              <PageTitle breadcrumbs={permissionsBreadcrumbs}>
                {intl.formatMessage({id: 'permissions_role'})}
              </PageTitle>
              <PermissionForm />
            </>
          }
        /> */}
        <Route
          path='/edit-permission'
          element={
            <>
              <PageTitle breadcrumbs={permissionsBreadcrumbs}>
                {intl.formatMessage({id: 'edit_permission'})}
              </PageTitle>
              <Edit />
            </>
          }
        />
        <Route
          path='/add-permission'
          element={
            <>
              <PageTitle breadcrumbs={permissionsBreadcrumbs}>
                {intl.formatMessage({id: 'add_permission'})}
              </PageTitle>
              <Add />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/roles-management/permissions' />} />
    </Routes>
  )
}

export default PermissionsRoutes
