import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {Permission, PermissionsQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
import {post} from '../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const PERMISSION_URL = `${API_URL}/permissions`
const GET_USERS_URL = `${API_URL}/permissions/query`

const getPermissions = async (query: string) => {
  const data = await getList(`${PERMISSION_URL}?` + query)
  return data
  // .then((d: AxiosResponse<PermissionsQueryResponse>) => d)
}

const getPermissionById = (id: ID): Promise<Permission | undefined> => {
  return axios
  .get(`${PERMISSION_URL}/${id}`)
  .then((response: AxiosResponse<Response<Permission>>) => {
    return response.data
  })
  .then((response: Response<Permission>) => response.data)
}

const createPermission = (permission: Permission): Promise<ResponeApiCheck | undefined> => {
  return create(`${PERMISSION_URL}/`, permission)
  // return axios
  //   .put(USER_URL, permission)
  //   .then((response: AxiosResponse<Response<Permission>>) => response.data)
  //   .then((response: Response<Permission>) => response.data)
}

const updatePermission = (permission: Permission): Promise<ResponeApiCheck | undefined> => {
  return axios
  .put(`${PERMISSION_URL}/${permission.id}`, permission)
  .then((response: AxiosResponse<Response<ResponeApiCheck>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: ResponeApiCheck) => {
    console.log('response :>> ', response)
    return response
  })
}

const deletePermission = async (permissionId: ID) => {
  return await dele(`${PERMISSION_URL}/`, permissionId)
}

const deleteSelectedPermissions = (permissionIds: Array<ID>): Promise<void> => {
  const requests = permissionIds.map((id) => axios.delete(`${PERMISSION_URL}/${id}`))
  return axios.all(requests).then(() => {})
}

const getGroups = async () => {
  const res = await getList(`${API_URL}/roles_permissions/groups/`)
  return res.data?.map((item) => {
    return {key: item.id, value: item.name}
  })
  // .then((d: AxiosResponse<PermissionsQueryResponse>) => d)
}

export {
  getPermissions,
  deletePermission,
  deleteSelectedPermissions,
  getPermissionById,
  createPermission,
  updatePermission,
  getGroups,
}
