import {ID, Response} from '../../../../../_metronic/helpers'
export type Permission = {
  id?: ID
  code: string
  name: string
  group_id?: ID
}

export type PermissionsQueryResponse = Response<Array<Permission>>

export const initialPermission: Permission = {
  name: '',
  code: '',
}
