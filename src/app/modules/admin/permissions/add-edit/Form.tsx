import {FC} from 'react'
import {useFormikContext} from 'formik'
import {useQuery} from 'react-query'
import {QUERIES} from '../../../../../_metronic/helpers'
import {getGroups} from '../core/_requests'
import {Loading} from '../../../../components/Loading'
import FormikInput from '../../../../components/formik/FormikInput'
import SubmitButton from '../../../../components/SubmitButton'
import ResetButton from '../../../../components/ResetButton'
import FormikSelect from '../../../../components/formik/FormikSelect'

const Form: FC = (props: any) => {
  const {handleSubmit, resetForm, isSubmitting, isValid, touched, values, setFieldValue, errors} =
    useFormikContext()

  const {data: listGroups} = useQuery(
    QUERIES.ROLES_GROUPS_LIST,
    () => {
      return getGroups()
    },
    {
      refetchOnMount: true,
    }
  )
  return (
    <div>
      <div className='d-flex flex-column scroll-y me-n7 pe-7 bg-white ps-7 pt-7 pb-7'>
        <div className='row'>
          <div className='col-md-6 col-sm-12'>
            <FormikInput isRequired={true} title={'code'} name={'code'} value={values['code']} />
          </div>
          <div className='col-md-6 col-sm-12'>
            <FormikInput isRequired={true} title={'name'} name={'name'} value={values['name']} />
          </div>
          <div className='col-md-6 col-sm-12'>
            <FormikSelect isRequired={true} name='group_id' title='group' options={listGroups} />
          </div>
          {/* begin::Actions */}
          <div className='text-center pt-15'>
            <ResetButton resetForm={resetForm} isSubmitting={isSubmitting} />
            <SubmitButton
              isSubmitting={isSubmitting}
              isValid={isValid}
              touched={touched}
              onclick={handleSubmit}
            />
          </div>
          {/* end::Actions */}
        </div>
      </div>
      {isSubmitting && <Loading />}
    </div>
  )
}

export {Form}
