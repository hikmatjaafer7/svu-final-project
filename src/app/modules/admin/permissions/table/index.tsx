import {useMemo} from 'react'
import {permissionsColumns} from './columns/_columns'
import Table from '../../../../components/Table'
import {useGenericQueryRequest} from '../../../../core/GenericQueryRequestProvider'
import {
  useGenericQueryResponseData,
  useGenericQueryResponseLoading,
  useGenericQueryResponsePagination,
} from '../../../../core/GenericQueryResonseProvider'
import {KTCardBody} from '../../../../../_metronic/helpers'

const PermissionsTable = () => {
  const permissions = useGenericQueryResponseData()
  const isLoading = useGenericQueryResponseLoading()
  const pagination = useGenericQueryResponsePagination()
  const {updateState} = useGenericQueryRequest()
  const data = useMemo(() => permissions, [permissions])
  const columns = useMemo(() => permissionsColumns, [])
  return (
    <>
      <KTCardBody className='py-4'>
        <Table
          columns={columns}
          data={data}
          isLoading={isLoading}
          pagination={pagination}
          updateState={updateState}
        />
      </KTCardBody>
    </>
  )
}

export {PermissionsTable}
