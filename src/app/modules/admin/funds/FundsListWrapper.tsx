import {FundsTable} from './table'
import {KTCard, QUERIES} from '../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponseRefetch,
} from '../../../core/GenericQueryResonseProvider'
import {
  GenericQueryRequestProvider,
  useGenericQueryRequest,
} from '../../../core/GenericQueryRequestProvider'
import {deleteSelectedFunds, getFunds} from './core/_requests'
import {ListHeader} from '../../../components/Table/header/ListHeader'
import {useState} from 'react'
import {Loading} from '../../../components/Loading'
import {useNotification} from '../../../../_metronic/hooks/useNotification'
import {useNavigate} from 'react-router-dom'

const FundsList = () => {
  const {itemIdForUpdate} = useGenericListView()
  const refetch = useGenericQueryResponseRefetch()
  // const {selected} = useGenericListView()
  const [isLoading, setIsLoading] = useState(false)
  const {showNotification} = useNotification()
  const navigate = useNavigate()
  return (
    <>
      <KTCard>
        <ListHeader
          handleAdd={() => {
            navigate('/admin/funds-management/add-fund')
          }}
          onFilterSubmit={(filterUpdateState) => {}}
          ListFilterChildren={<></>}
          queryString={QUERIES.FUNDS_LIST}
          deleteSelected={(selected) => {
            setIsLoading(true)
            deleteSelectedFunds(selected)
            .then((res) => {
              setIsLoading(false)
              showNotification({result: 'success', data: '', error_description: ''})
              refetch()
            })
            .catch((err) => {
              setIsLoading(false)
              showNotification({result: 'error', data: '', error_description: 'error'})
              refetch()
            })
          }}
        />
        <FundsTable />
        {isLoading && <Loading />}
      </KTCard>
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <FundsList />
    </GenericListViewProvider>
  )
}

const FundsListWrapper = () => {
  return (
    <GenericQueryRequestProvider>
      <GenericQueryResponseProvider getApi={getFunds} queryString={QUERIES.FUNDS_LIST}>
        <ListView />
      </GenericQueryResponseProvider>
    </GenericQueryRequestProvider>
  )
}

export {FundsListWrapper}
