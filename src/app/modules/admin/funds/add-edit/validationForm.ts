import {useIntl} from 'react-intl'
import * as Yup from 'yup'

export const RoleSchema = () => {
  const intl = useIntl()
  const validations = Yup.object().shape({
    name: Yup.string().required(intl.formatMessage({id: 'field_is_required'})),
    duration_in_month: Yup.number()
    .min(1)
    .required(intl.formatMessage({id: 'field_is_required'})),
    subscribers_allowed_number: Yup.number()
    .min(1)
    .required(intl.formatMessage({id: 'field_is_required'})),
    participant_shares_allowed_number: Yup.number()
    .min(1)
    .required(intl.formatMessage({id: 'field_is_required'})),
    start_date: Yup.date().required(intl.formatMessage({id: 'field_is_required'})),
  })
  return validations
}

export const PaymentRoleSchema = () => {
  const intl = useIntl()
  const validations = Yup.object().shape({
    subscription_id: Yup.number().required(intl.formatMessage({id: 'field_is_required'})),
    amount: Yup.number()
    .min(1)
    .required(intl.formatMessage({id: 'field_is_required'})),
  })
  return validations
}

export const LoanPaymentRoleSchema = () => {
  const intl = useIntl()
  const validations = Yup.object().shape({
    loan_id: Yup.number().required(intl.formatMessage({id: 'field_is_required'})),
    amount: Yup.number()
    .min(1)
    .required(intl.formatMessage({id: 'field_is_required'})),
  })
  return validations
}
