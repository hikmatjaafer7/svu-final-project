import React, {useEffect, useState} from 'react'
import FormModal from '../../../../components/modals/Form'
import {useQuery} from 'react-query'
import {QUERIES, ResponeApiCheck, initialResponseError} from '../../../../../_metronic/helpers'
import {getUsersByFundId} from '../../users/core/_requests'
import {Formik, useFormikContext} from 'formik'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {initialPayment} from '../core/_models'
import {PaymentRoleSchema} from './validationForm'
import {AddMultiSubscriptionPayments, AddSubscriptionPayment} from '../core/_requests'
import FormikSelect from '../../../../components/formik/FormikSelect'
import Select from '../../../../components/Select'
import {useIntl} from 'react-intl'
import RequestStatus from '../../../../core/enums/RequestStatus'
import FormikInput from '../../../../components/formik/FormikInput'
import ResetButton from '../../../../components/ResetButton'
import SubmitButton from '../../../../components/SubmitButton'

const AddMultiPayments: any = ({fund_id, modal_id, refetchApi}) => {
  const {showNotification} = useNotification()

  return (
    <FormModal id={modal_id} title='إضافة مجموعة أسداد' classes='modal-xl'>
      <>
        <Formik
          enableReinitialize={true}
          validationSchema={PaymentRoleSchema()}
          initialValues={[initialPayment]}
          initialStatus={{edit: false}}
          onSubmit={async (values: any, {setSubmitting}) => {
            setSubmitting(true)
            try {
              const res: ResponeApiCheck = await AddMultiSubscriptionPayments(values)
              if (res.result == 'success') {
                if (refetchApi && typeof refetchApi === 'function') refetchApi()
                var element = document.getElementById(`${modal_id}_close`)
                if (element) element.click()
              }
              showNotification(res)
            } catch (ex) {
              showNotification({error_description: ex, ...initialResponseError})
              console.error(ex)
            } finally {
              setSubmitting(true)
            }
          }}
          onReset={(values) => {}}
        >
          <Form fund_id={fund_id} modal_id={modal_id} />
        </Formik>
      </>
    </FormModal>
  )
}

export default AddMultiPayments

const Form = ({fund_id, modal_id}) => {
  const [selectedUserId, setSelectedUserId] = useState([])
  const intl = useIntl()
  const initialPayment = {
    subscription_id: 0,
    amount: 0,
    description: '',
  }

  const {
    handleSubmit,
    resetForm,
    isSubmitting,
    isValid,
    touched,
    values,
    setFieldValue,
    setValues,
    errors,
  } = useFormikContext()

  const [userSubscriptions, setUserSubscriptions] = useState([[]])
  const {data: listUsers} = useQuery(
    `${QUERIES.LIST_USERS_BY_FUND_ID_WITH_SUBSCRIPTIONS}-${fund_id}`,
    () => {
      return getUsersByFundId(Number(fund_id)).then((res) => {
        return [{id: 0, subscriptions: [], name: intl.formatMessage({id: 'select_option'})}, ...res]
      })
    },
    {
      refetchOnMount: true,
    }
  )

  // useEffect(() => {
  //   let userSub = listUsers?.find((user) => Number(user.id) === Number(selectedUserId))
  //   if (userSub) {
  //     {
  //       setUserSubscriptions([
  //         {id: intl.formatMessage({id: 'select_option'})},
  //         ...userSub?.subscriptions
  //         ?.map((sub) => {
  //           if (sub.status === RequestStatus.Accepted) return sub
  //         })
  //         ?.filter((s) => s !== undefined),
  //       ])
  //     }
  //   } else {
  //     setUserSubscriptions([])
  //   }
  // }, [selectedUserId])

  return (
    <>
      {values &&
        Array.isArray(values) &&
        values.map((payment, index) => {
          return (
            <div className='row row-cols-5 p-6 py-0 g-4'>
              <div className='col'>
                <Select
                  Key='id'
                  ValueKey='name'
                  options={listUsers}
                  selectedValue={selectedUserId[index]}
                  onChange={(userId: any) => {
                    let tempSelectedUserIds = [...selectedUserId]
                    tempSelectedUserIds[index] = userId
                    setSelectedUserId(tempSelectedUserIds)
                    let userSub = listUsers?.find((user) => Number(user.id) === Number(userId))
                    let tempUserSubscriptions = [...userSubscriptions]
                    if (userSub) {
                      tempUserSubscriptions[index] = [
                        {id: intl.formatMessage({id: 'select_option'})},
                        ...userSub?.subscriptions
                        ?.map((sub) => {
                          if (sub.status === RequestStatus.Accepted) return sub
                        })
                        ?.filter((s) => s !== undefined),
                      ]
                    } else {
                      tempUserSubscriptions[index] = []
                    }
                    setUserSubscriptions(tempUserSubscriptions)
                  }}
                  title={intl.formatMessage({id: 'user'})}
                />
              </div>
              <div className='col'>
                <FormikSelect
                  Key='id'
                  ValueKey='id'
                  options={userSubscriptions[index] || []}
                  name={`${index}.subscription_id`}
                  isRequired
                  title={intl.formatMessage({id: 'subscription_number'})}
                />
              </div>
              <div className='col'>
                <FormikInput
                  type='number'
                  name={`${index}.amount`}
                  isRequired
                  title={intl.formatMessage({id: 'amount'})}
                />
              </div>
              <div className='col'>
                <FormikInput
                  type='textarea'
                  name={`${index}.description`}
                  isRequired={false}
                  title={intl.formatMessage({id: 'details'})}
                />
              </div>
              {index === values.length - 1 && (
                <div className='col mt-15'>
                  <button
                    className='btn btn-primary mx-1 btn-sm'
                    onClick={() => {
                      setValues([...values, initialPayment])
                    }}
                  >
                    إضافة{' '}
                  </button>

                  <button
                    className='btn btn-danger mx-1 btn-sm'
                    onClick={() => {
                      let tempData = [...values]
                      tempData = tempData.filter((d, dIndex) => dIndex !== index)
                      setValues(tempData)
                    }}
                  >
                    حذف{' '}
                  </button>
                </div>
              )}
            </div>
          )
        })}

      <div className='separator my-4'></div>
      <div className='text-center mb-4'>
        <ResetButton id={`${modal_id}cancel`} resetForm={resetForm} isSubmitting={isSubmitting} />
        <SubmitButton
          id={`${modal_id}_submit`}
          isSubmitting={isSubmitting}
          isValid={isValid}
          touched={touched}
          onclick={handleSubmit}
        />
      </div>
    </>
  )
}
