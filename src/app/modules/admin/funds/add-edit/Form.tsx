import {FC, useState} from 'react'
import {useFormikContext} from 'formik'
import {useIntl} from 'react-intl'
// import {ProfileImage} from '../../../../../_metronic/utlis/formik'
import {useQuery} from 'react-query'
import {QUERIES} from '../../../../../_metronic/helpers'
import Input from '../../../../components/Input'
import Button from '../../../../components/Button'
import {createFund} from '../core/_requests'

import {Loading} from '../../../../components/Loading'
import FormikInput from '../../../../components/formik/FormikInput'
import FormikToggle from '../../../../components/formik/FormikToggle'
import SubmitButton from '../../../../components/SubmitButton'
import ResetButton from '../../../../components/ResetButton'
import FormikSelect from '../../../../components/formik/FormikSelect'
import FormikInputDate from '../../../../components/formik/FormikInputDate'

const Form: FC = (props: any) => {
  const intel = useIntl()

  // const [errorMsg, setErrorMsg] = useState({show: false, message: ''})
  // const navigate = useNavigate()
  const [isLoading, setIsLoading] = useState(false)
  // const handleSubmit = () => {
  //   setIsLoading(true)
  //   createUser(model).then((res) => {
  //     navigate('/admin/users')
  //     setIsLoading(false)
  //   })
  // }
  const {handleSubmit, resetForm, isSubmitting, isValid, touched, values, setFieldValue, errors} =
    useFormikContext()

  return (
    <div>
      <div className='d-flex flex-column scroll-y me-n7 pe-7 bg-white ps-7 pt-7 pb-7'>
        <div className='row'>
          <div className='col-md-3 col-sm-12'>
            <FormikInput isRequired={true} title={'name'} name={'name'} value={values['name']} />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              min={1}
              type='number'
              title={'duration_in_month'}
              name={'duration_in_month'}
              value={values['duration_in_month']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              min={1}
              type='number'
              title={'subscribers_allowed_number'}
              name={'subscribers_allowed_number'}
              value={values['subscribers_allowed_number']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              min={1}
              type='number'
              title={'participant_shares_allowed_number'}
              name={'participant_shares_allowed_number'}
              value={values['participant_shares_allowed_number']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              min={1}
              type='number'
              title={'shares_value'}
              name={'shares_value'}
              value={values['shares_value']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              min={1}
              type='number'
              title={'fees'}
              name={'fees'}
              value={values['fees']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInputDate
              isRequired={true}
              title={'start_date'}
              name={'start_date'}
              value={values['start_date']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInputDate
              isRequired={true}
              title={'end_date'}
              name={'end_date'}
              value={values['end_date']}
            />
          </div>
          {/* begin::Actions */}
          <div className='text-center pt-15'>
            <ResetButton resetForm={resetForm} isSubmitting={isSubmitting} />
            <SubmitButton
              isSubmitting={isSubmitting}
              isValid={isValid}
              touched={touched}
              onclick={handleSubmit}
            />
          </div>
          {/* end::Actions */}
        </div>
      </div>
      {isSubmitting && <Loading />}
    </div>
  )
}

export {Form}
