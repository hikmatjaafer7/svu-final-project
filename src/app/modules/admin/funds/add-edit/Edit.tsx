import {Formik} from 'formik'
import {
  KTCard,
  KTCardBody,
  ResponeApiCheck,
  initialResponseError,
} from '../../../../../_metronic/helpers'
import {Form} from './Form'
import {RoleSchema} from './validationForm'
import {updateFund} from '../core/_requests'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {useLocation, useNavigate} from 'react-router-dom'

const Edit = () => {
  const navigate = useNavigate()
  const {showNotification} = useNotification()
  const location = useLocation()
  const data: any = location.state
  return (
    <KTCard>
      <KTCardBody className='py-4'>
        {data && (
          <Formik
            enableReinitialize={true}
            validationSchema={RoleSchema()}
            initialValues={data}
            initialStatus={{edit: false}}
            onSubmit={async (values: any, {setSubmitting}) => {
              setSubmitting(true)
              try {
                const res: ResponeApiCheck = await updateFund(values)
                console.log('res REsponse API CHECK :>> ', res)
                if (res.result == 'success') {
                  navigate('/admin/funds-management/funds')
                }
                showNotification(res)
              } catch (ex) {
                showNotification({error_description: ex, ...initialResponseError})
                console.error(ex)
              } finally {
                setSubmitting(true)
              }
            }}
            onReset={(values) => {}}
          >
            <Form />
          </Formik>
        )}
      </KTCardBody>
    </KTCard>
  )
}

export default Edit
