import React, {useEffect, useState} from 'react'
import FormModal from '../../../../components/modals/Form'
import {useQuery} from 'react-query'
import {QUERIES, ResponeApiCheck, initialResponseError} from '../../../../../_metronic/helpers'
import {getUsersByFundId} from '../../users/core/_requests'
import {Formik, useFormikContext} from 'formik'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {initialPayment} from '../core/_models'
import {PaymentRoleSchema} from './validationForm'
import {AddMultiLoanPayments} from '../core/_requests'
import FormikSelect from '../../../../components/formik/FormikSelect'
import Select from '../../../../components/Select'
import {useIntl} from 'react-intl'
import RequestStatus from '../../../../core/enums/RequestStatus'
import FormikInput from '../../../../components/formik/FormikInput'
import ResetButton from '../../../../components/ResetButton'
import SubmitButton from '../../../../components/SubmitButton'

const AddMultiLoansPayments: any = ({fund_id, modal_id, refetchApi}) => {
  const {showNotification} = useNotification()

  return (
    <FormModal id={modal_id} title='إضافة مجموعة أسداد' classes='modal-xl'>
      <>
        <Formik
          enableReinitialize={true}
          validationSchema={PaymentRoleSchema()}
          initialValues={[initialPayment]}
          initialStatus={{edit: false}}
          onSubmit={async (values: any, {setSubmitting}) => {
            setSubmitting(true)
            try {
              const res: ResponeApiCheck = await AddMultiLoanPayments(values)
              if (res.result == 'success') {
                if (refetchApi && typeof refetchApi === 'function') refetchApi()
                var element = document.getElementById(`${modal_id}_close`)
                if (element) element.click()
              }
              showNotification(res)
            } catch (ex) {
              showNotification({error_description: ex, ...initialResponseError})
              console.error(ex)
            } finally {
              setSubmitting(true)
            }
          }}
          onReset={(values) => {}}
        >
          <Form fund_id={fund_id} modal_id={modal_id} />
        </Formik>
      </>
    </FormModal>
  )
}

export default AddMultiLoansPayments

const Form = ({fund_id, modal_id}) => {
  const [selectedUserId, setSelectedUserId] = useState([])
  const intl = useIntl()
  const initialPayment = {
    loan_id: 0,
    amount: 0,
    description: '',
  }

  const {
    handleSubmit,
    resetForm,
    isSubmitting,
    isValid,
    touched,
    values,
    setFieldValue,
    setValues,
    errors,
  } = useFormikContext()

  const [userLoan, setUserLoan] = useState([[]])
  const {data: listUsers} = useQuery(
    `${QUERIES.LIST_USERS_BY_FUND_ID_WITH_Loans}-${fund_id}`,
    () => {
      return getUsersByFundId(Number(fund_id)).then((res) => {
        return [{id: 0, loans: [], name: intl.formatMessage({id: 'select_option'})}, ...res]
      })
    },
    {
      refetchOnMount: true,
    }
  )

  // useEffect(() => {
  //   let userSub = listUsers?.find((user) => Number(user.id) === Number(selectedUserId))
  //   if (userSub) {
  //     {
  //       setUserLoan([
  //         {id: intl.formatMessage({id: 'select_option'})},
  //         ...userSub?.loans
  //         ?.map((sub) => {
  //           if (sub.status === RequestStatus.Accepted) return sub
  //         })
  //         ?.filter((s) => s !== undefined),
  //       ])
  //     }
  //   } else {
  //     setUserLoan([])
  //   }
  // }, [selectedUserId])
  const [type, setType] = useState(['ordinary'])
  console.log('selectedUserId :>> ', selectedUserId)
  return (
    <>
      {values &&
        Array.isArray(values) &&
        values.map((payment, index) => {
          return (
            <div className='row row-cols-5 p-6 py-0 g-4'>
              <div className='col'>
                <Select
                  Key='id'
                  ValueKey='name'
                  options={listUsers}
                  selectedValue={selectedUserId[index]}
                  onChange={(userId: any) => {
                    let tempSelectedUserIds = [...selectedUserId]
                    tempSelectedUserIds[index] = userId
                    setSelectedUserId(tempSelectedUserIds)
                    let userSub = listUsers?.find((user) => Number(user.id) === Number(userId))
                    let tempUserLoan = [...userLoan]
                    if (userSub) {
                      tempUserLoan[index] = [
                        {id: intl.formatMessage({id: 'select_option'})},
                        ...userSub?.loans
                        ?.map((sub) => {
                          if (sub.status === RequestStatus.Accepted) return sub
                        })
                        ?.filter((s) => s !== undefined),
                      ]
                    } else {
                      tempUserLoan[index] = []
                    }
                    setUserLoan(tempUserLoan)
                  }}
                  title={intl.formatMessage({id: 'user'})}
                />
              </div>
              <div className='col'>
                <Select
                  options={[
                    {key: 'ordinary', value: intl.formatMessage({id: 'ordinary'})},
                    {key: 'emergency', value: intl.formatMessage({id: 'emergency'})},
                  ]}
                  selectedValue={type[index]}
                  onChange={(typeInd: any) => {
                    let tempType = [...type]
                    tempType[index] = typeInd
                    setType(tempType)
                  }}
                  title={intl.formatMessage({id: 'type'})}
                />
              </div>
              <div className='col'>
                <FormikSelect
                  Key='id'
                  ValueKey='id'
                  options={
                    [
                      {
                        id: intl.formatMessage({id: 'select_option'}),
                        name: intl.formatMessage({id: 'select_option'}),
                      },
                      ...(userLoan[index]?.filter((loan) => loan.type === type[index]) || []),
                    ] || []
                  }
                  name={`${index}.loan_id`}
                  isRequired
                  title={intl.formatMessage({id: 'loan_number'})}
                />
              </div>
              <div className='col'>
                <FormikInput
                  type='number'
                  name={`${index}.amount`}
                  isRequired
                  title={intl.formatMessage({id: 'amount'})}
                />
              </div>
              <div className='col'>
                <FormikInput
                  type='textarea'
                  name={`${index}.description`}
                  isRequired={false}
                  title={intl.formatMessage({id: 'details'})}
                />
              </div>
              {index === values.length - 1 && (
                <div className='col mt-15'>
                  <button
                    className='btn btn-primary mx-1 btn-sm'
                    onClick={() => {
                      setValues([...values, initialPayment])
                    }}
                  >
                    إضافة{' '}
                  </button>

                  <button
                    className='btn btn-danger mx-1 btn-sm'
                    onClick={() => {
                      let tempData = [...values]
                      tempData = tempData?.filter((d, dIndex) => dIndex !== index)
                      setValues(tempData)
                    }}
                  >
                    حذف{' '}
                  </button>
                </div>
              )}
            </div>
          )
        })}

      <div className='separator my-4'></div>
      <div className='text-center mb-4'>
        <ResetButton id={`${modal_id}cancel`} resetForm={resetForm} isSubmitting={isSubmitting} />
        <SubmitButton
          id={`${modal_id}_submit`}
          isSubmitting={isSubmitting}
          isValid={isValid}
          touched={touched}
          onclick={handleSubmit}
        />
      </div>
    </>
  )
}
