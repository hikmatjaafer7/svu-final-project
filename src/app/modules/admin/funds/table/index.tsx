import {useMemo} from 'react'
import {fundsColumns} from './columns/_columns'
import Table from '../../../../components/Table'
import {useGenericQueryRequest} from '../../../../core/GenericQueryRequestProvider'
import {
  useGenericQueryResponseData,
  useGenericQueryResponseLoading,
  useGenericQueryResponsePagination,
} from '../../../../core/GenericQueryResonseProvider'
import {KTCardBody} from '../../../../../_metronic/helpers'

const FundsTable = () => {
  const funds = useGenericQueryResponseData()
  const isLoading = useGenericQueryResponseLoading()
  const pagination = useGenericQueryResponsePagination()
  const {updateState} = useGenericQueryRequest()
  const data = useMemo(() => funds, [funds])
  const columns = useMemo(() => fundsColumns, [])
  return (
    <>
      <KTCardBody className='py-4'>
        <Table
          columns={columns}
          data={data}
          isLoading={isLoading}
          pagination={pagination}
          updateState={updateState}
        />
      </KTCardBody>
    </>
  )
}

export {FundsTable}
