import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {FundsListWrapper} from './FundsListWrapper'
// import PermissionForm from './add-edit/AddEditRole'
import {Form} from './add-edit/Form'
import Add from './add-edit/Add'
import Edit from './add-edit/Edit'
import PaymentsList from './payments/List'
import UsersPaymentsList from './payments/users/List'
import UsersLoansPaymentsList from './loans-payments/users/List'
import LoansPaymentsList from './loans-payments/List'

const FundsRoutes = () => {
  const intl = useIntl()
  const fundsBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'fund_management'}),
      path: 'admin/funds-management/funds',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/funds'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'funds_list'})}
              </PageTitle>
              <FundsListWrapper />
            </>
          }
        />
        <Route
          path='/edit-fund'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'edit_fund'})}
              </PageTitle>
              <Edit />
            </>
          }
        />
        <Route
          path='/add-fund'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'add_fund'})}
              </PageTitle>
              <Add />
            </>
          }
        />
        <Route
          path='/payments'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'payments'})}
              </PageTitle>
              <PaymentsList />
            </>
          }
        />
        <Route
          path='/payments/users'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'users_payments'})}
              </PageTitle>
              <UsersPaymentsList />
            </>
          }
        />
               <Route
          path='/loans-payments'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'payments'})}
              </PageTitle>
              <LoansPaymentsList />
            </>
          }
        />
        <Route
          path='/loans-payments/users'
          element={
            <>
              <PageTitle breadcrumbs={fundsBreadcrumbs}>
                {intl.formatMessage({id: 'users_payments'})}
              </PageTitle>
              <UsersLoansPaymentsList />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/funds-management/funds' />} />
    </Routes>
  )
}

export default FundsRoutes
