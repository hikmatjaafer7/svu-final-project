import React, {useState} from 'react'
import {ListsWidget3} from '../../../../../../_metronic/partials/widgets'
import {useLocation} from 'react-router-dom'
import {useQuery} from 'react-query'
import {QUERIES} from '../../../../../../_metronic/helpers'
import {
  getFundUserLoansPaymentsByYearMonth,
  getFundUserPaymentsByYearMonth,
} from '../../core/_requests'
import {Card3} from '../../../../../../_metronic/partials/content/cards/Card3'
import {ModalList} from '../../../../../components/modals/InviteUsers'

const UsersLoansPaymentsList = () => {
  const location: any = useLocation()
  const {id, year, month, name} = location.state
  const [usersTotalPayments, setUsersTotalPayments] = useState([])
  const [userPaymentsModalData, setUserPaymentsModalData] = useState([])
  const [userModalInfo, setUserModalInfo]: any = useState({})
  const {data: listUsersPayments} = useQuery(
    `${QUERIES.LIST_FUND_LOANS_USER_PAYMENTS}-${id}-${year}-${month}`,
    () => {
      return getFundUserLoansPaymentsByYearMonth(Number(id), year, month).then((users) => {
        console.log('users :>> ', users)
        const result = users.map((user) => {
          const totalAmount = user.Loans.reduce((sum, loan) => {
            const loanTotal = loan.LoanPayments.reduce(
              (amount, payment) => amount + payment.amount,
              0
            )
            return sum + loanTotal
          }, 0)

          return {
            id: user.id,
            first_name: user.first_name,
            last_name: user.last_name,
            total_payment_amount: totalAmount,
          }
        })
        setUsersTotalPayments(result)
        return users
      })
    },
    {
      refetchOnMount: true,
    }
  )
  const userPaymentInfoHandle: any = (userId) => {
    console.log('userId :>> ', userId)
    const user = listUsersPayments.find((user) => user.id === userId)
    console.log('user :>> ', user)
    if (user && user.Loans) {
      const loanPayments = user.Loans.flatMap((loan) => loan.LoanPayments.map((payment) => payment))
      loanPayments.sort((a, b) => {
        const dateA: any = new Date(a.createdAt)
        const dateB: any = new Date(b.createdAt)
        return dateB - dateA
      })
      setUserPaymentsModalData(loanPayments)
      return
    }

    setUserPaymentsModalData([])
  }
  return (
    <>
      <div className=' card card-body px-10 py-8 border border-opacity-25 border-primary mb-6 '>
        <h2 className='fw-bold'>أسداد قروض المشتركين في صندوق : {name}</h2>
        <h3 className='text-gray-600'>
          بتاريخ : {year} / {month}
        </h3>
      </div>
      {/* <button
        type='button'
        className='btn btn-primary'
        data-bs-toggle='modal'
        data-bs-target='#kt_modal_list_user_payments'
        onClick={() => {
          userPaymentInfoHandle(1)
        }}
      >
        Launch demo modal
      </button> */}
      <div className='row g-5 g-xl-8'>
        {usersTotalPayments &&
          usersTotalPayments.map((user) => {
            return (
              <>
                {' '}
                <div className='col-sm-6 col-xl-4 col-xxl-3 ' key={user.id}>
                  <Card3
                    btnTargetId='kt_modal_list_user_loan_payments'
                    onClickInfo={() => {
                      setUserModalInfo(user)
                      userPaymentInfoHandle(user.id)
                    }}
                    online
                    color='primary'
                    name={`${user.first_name} ${user.last_name}`}
                    job={user.email}
                    totalEarnings={user.total_payment_amount}
                  />
                </div>
              </>
            )
          })}
      </div>
      <ModalList
        user_name={userModalInfo?.first_name}
        list={userPaymentsModalData}
        title={`أسداد المشترك : ${userModalInfo.first_name} ${userModalInfo.last_name}`}
        description={`إجمالي أسداد المشترك : ${userModalInfo.total_payment_amount}`}
        id='kt_modal_list_user_loan_payments'
      />
    </>
  )
}

export default UsersLoansPaymentsList
