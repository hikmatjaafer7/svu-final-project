import React, {useState} from 'react'
import {ListsWidget3} from '../../../../../_metronic/partials/widgets'
import {useLocation, useNavigate} from 'react-router-dom'
import {useQuery} from 'react-query'
import {QUERIES} from '../../../../../_metronic/helpers'
import {getFundLoansPayments, getFundSubscriptionsPayments} from '../core/_requests'
import {InviteUsers} from '../../../../../_metronic/partials'
import {ModalList} from '../../../../components/modals/InviteUsers'
import Button from '../../../../components/Button'
import AddPayment from '../add-edit/AddPayment'
import AddLoanPayment from '../add-edit/AddLoanPayment'
import AddMultiLoansPayments from '../add-edit/AddMultiLoanPayments'

const LoansPaymentsList = () => {
  const location: any = useLocation()
  const data: any = location.state
  const id: string | number = data?.id
  const navigate = useNavigate()
  const {data: listPaymentsYears, refetch} = useQuery(
    `${QUERIES.LIST_FUND_LOANS_PAYMENTS}-${id}`,
    () => {
      return getFundLoansPayments(Number(id))
    },
    {
      refetchOnMount: true,
    }
  )

  const renderWidgetItems = (year) => {
    const months = [
      {monthNumber: 1, name: 'يناير', color: 'primary'},
      {monthNumber: 2, name: 'فبراير', color: 'green'},
      {monthNumber: 3, name: 'مارس', color: 'success'},
      {monthNumber: 4, name: 'أبريل', color: 'danger'},
      {monthNumber: 5, name: 'مايو', color: 'warning'},
      {monthNumber: 6, name: 'يونيو', color: 'info'},
      {monthNumber: 7, name: 'يوليو', color: 'dark'},
      {monthNumber: 8, name: 'أغسطس', color: 'primary'},
      {monthNumber: 9, name: 'سبتمبر', color: 'green'},
      {monthNumber: 10, name: 'أكتوبر', color: 'success'},
      {monthNumber: 11, name: 'نوفمبر', color: 'danger'},
      {monthNumber: 12, name: 'ديسمبر', color: 'info'},
    ]

    const yearData = listPaymentsYears.find((paymentYear) => paymentYear.year === year)
    const payments = yearData ? yearData.months : []

    const items = months.map((month) => {
      const payment = payments.find((p) => p.month === month.monthNumber)
      const totalAmount = payment ? payment.totalAmount : '-- --'
      return {
        number: month.monthNumber,
        color: month.color,
        title: month.name,
        description: '',
        total: totalAmount.toString(),
      }
    })

    return items
  }

  return (
    <>
      <div className=' card card-body px-10 py-8 border border-opacity-25 border-primary mb-6 '>
        <div className='row'>
          <div className='col'>
            <h2 className='fw-bold'>أسداد قروض الصندوق : {data?.name}</h2>
            <h3 className='text-gray-600'>حسب العام والأشهر </h3>
          </div>
          <div className='col-12 col-sm-5 d-flex justify-content-end py-3'>
            <button
              onClick={() => {}}
              className='btn btn-sm btn-info mx-1'
              data-bs-toggle='modal'
              data-bs-target={'#add_multi_loan_payments'}
            >
              {/* <KTIcon iconName='upper' className='fs-3' /> */}
              إضافة مجموعة أسداد
            </button>{' '}
            <button
              onClick={() => {}}
              className='btn btn-sm btn-primary mx-1'
              data-bs-toggle='modal'
              data-bs-target={'#add_loan_payment'}
            >
              {/* <KTIcon iconName='upper' className='fs-3' /> */}
              إضافة سداد
            </button>
          </div>
        </div>
      </div>
      <div className='row g-5 g-xl-8'>
        {listPaymentsYears &&
          listPaymentsYears?.map((paymentYear) => {
            return (
              <>
                {' '}
                <div className='col-sm-6 col-xl-4 col-xxl-3 ' key={paymentYear?.year}>
                  <ListsWidget3
                    to='/admin/funds-management/loans-payments/users'
                    className='card-xl-stretch h-450px overflow-scroll mb-xl-8'
                    title={`عام ${paymentYear?.year}`}
                    items={renderWidgetItems(paymentYear?.year)}
                    year={paymentYear?.year}
                    fund_id={id}
                    name={data?.name}
                  />
                </div>
              </>
            )
          })}
      </div>
      <AddLoanPayment modal_id={'add_loan_payment'} fund_id={id} refetchApi={refetch} />
      <AddMultiLoansPayments
        modal_id={'add_multi_loan_payments'}
        fund_id={id}
        refetchApi={refetch}
      />
    </>
  )
}

export default LoansPaymentsList
