import {ID, Response} from '../../../../../_metronic/helpers'
export type Fund = {
  id?: ID
  name?: string
  duration_in_month?: string
  start_date?: Date
  end_date?: Date
  subscribers_allowed_number?: string
  participant_shares_allowed_number?: string
  shares_value?: string
  createdAt?: Date
  updatedAt?: Date | null
  fees?: number | string
}

export type FundsQueryResponse = Response<Array<Fund>>

export const initialFund: Fund = {
  name: '',
  duration_in_month: '',
  start_date: new Date(),
  subscribers_allowed_number: '',
  participant_shares_allowed_number: '',
  shares_value: '1000',
  fees: '',
}

export type Payment = {
  id?: ID
  subscription_id: ID
  amount: number
  description: string
  createdAt: string
  updatedAt: string
}

export const initialPayment: Payment = {
  amount: null,
  description: null,
  createdAt: null,
  updatedAt: null,
  subscription_id: null,
}


export type Loan = {
  id?: ID
  loan_id: ID
  amount: number
  description: string
  createdAt: string
  updatedAt: string
}

export const initialLoan: Loan = {
  amount: null,
  description: null,
  createdAt: null,
  updatedAt: null,
  loan_id: null,
}
