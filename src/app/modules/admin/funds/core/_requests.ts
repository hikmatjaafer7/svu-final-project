import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {Fund, FundsQueryResponse, Payment} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
import {get, post} from '../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const FUND_URL = `${API_URL}/funds`
const GET_FUNDS_URL = `${API_URL}/funds/query`

const getFunds = async (query: string) => {
  const data = await getList(`${FUND_URL}?` + query)
  return data
  // .then((d: AxiosResponse<FundsQueryResponse>) => d)
}

const getFundById = (id: ID): Promise<Fund | undefined> => {
  return axios
  .get(`${FUND_URL}/${id}`)
  .then((response: AxiosResponse<Response<Fund>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: Response<Fund>) => response.data)
}

const createFund = (fund: Fund): Promise<ResponeApiCheck | undefined> => {
  return create(`${FUND_URL}/`, fund)
  // return axios
  //   .put(FUND_URL, fund)
  //   .then((response: AxiosResponse<Response<Fund>>) => response.data)
  //   .then((response: Response<Fund>) => response.data)
}

const updateFund = (fund: Fund): Promise<ResponeApiCheck | undefined> => {
  return axios
  .put(`${FUND_URL}/${fund.id}`, fund)
  .then((response: AxiosResponse<Response<ResponeApiCheck>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: ResponeApiCheck) => {
    console.log('response :>> ', response)
    return response
  })
}

const deleteFund = async (fundId: ID) => {
  return await dele(`${FUND_URL}/`, fundId)
}

const deleteSelectedFunds = (fundIds: Array<ID>): Promise<void> => {
  const requests = fundIds.map((id) => axios.delete(`${FUND_URL}/${id}`))
  return axios.all(requests).then(() => {})
}

const getFundSubscriptionsPayments = async (fundId: ID): Promise<any> => {
  return await get(`${FUND_URL}/${fundId}/subscription-payments`)
}
const getFundLoansPayments = async (fundId: ID): Promise<any> => {
  return await get(`${FUND_URL}/${fundId}/loans-payments`)
}
const getFundUserPaymentsByYearMonth = async (
  fundId: ID,
  year: string,
  month: string
): Promise<any> => {
  return await get(`${FUND_URL}/${fundId}/users?year=${year}&month=${month}`)
}

const getFundUserLoansPaymentsByYearMonth = async (
  fundId: ID,
  year: string,
  month: string
): Promise<any> => {
  return await get(`${FUND_URL}/${fundId}/loans/users?year=${year}&month=${month}`)
}

const AddSubscriptionPayment = async (model: Payment): Promise<any> => {
  return await post(`/subscription-payments`, model)
}

const AddMultiSubscriptionPayments = async (model: Array<Payment>): Promise<any> => {
  return await post(`/subscription-payments/bulkCreate`, model)
}

const AddLoanPayment = async (model: Payment): Promise<any> => {
  return await post(`/loan-payments`, model)
}

const AddMultiLoanPayments = async (model: Array<Payment>): Promise<any> => {
  return await post(`/loan-payments/bulkCreate`, model)
}

export {
  getFunds,
  deleteFund,
  getFundSubscriptionsPayments,
  getFundUserPaymentsByYearMonth,
  deleteSelectedFunds,
  getFundById,
  createFund,
  updateFund,
  AddSubscriptionPayment,
  AddLoanPayment,
  getFundLoansPayments,
  getFundUserLoansPaymentsByYearMonth,
  AddMultiSubscriptionPayments,
  AddMultiLoanPayments,
}
