import {FC, useState} from 'react'
import {useFormikContext} from 'formik'
import {useIntl} from 'react-intl'
// import {ProfileImage} from '../../../../../_metronic/utlis/formik'
import {useQuery} from 'react-query'
import {QUERIES} from '../../../../../_metronic/helpers'
import Input from '../../../../components/Input'
import Button from '../../../../components/Button'
import {createUser, getRoles} from '../core/_requests'
import Switcher from '../../../../components/Switcher'
import {useNavigate} from 'react-router-dom'
import {Loading} from '../../../../components/Loading'
import FormikInput from '../../../../components/formik/FormikInput'
import FormikToggle from '../../../../components/formik/FormikToggle'
import SubmitButton from '../../../../components/SubmitButton'
import ResetButton from '../../../../components/ResetButton'
import FormikSelect from '../../../../components/formik/FormikSelect'

const Form: FC = (props: any) => {
  const intel = useIntl()

  // const [errorMsg, setErrorMsg] = useState({show: false, message: ''})
  // const navigate = useNavigate()
  const [isLoading, setIsLoading] = useState(false)
  // const handleSubmit = () => {
  //   setIsLoading(true)
  //   createUser(model).then((res) => {
  //     navigate('/admin/users')
  //     setIsLoading(false)
  //   })
  // }
  const {handleSubmit, resetForm, isSubmitting, isValid, touched, values, setFieldValue, errors} =
    useFormikContext()

  const {data: listRoles} = useQuery(
    `ROLE_LIST_VALUES`,
    () => {
      return getRoles()
    },
    {
      refetchOnMount: true,
    }
  )
  return (
    <div>
      <div className='d-flex flex-column scroll-y me-n7 pe-7 bg-white ps-7 pt-7 pb-7'>
        <div className='row'>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              title={'first_name'}
              name={'first_name'}
              value={values['first_name']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              title={'last_name'}
              name={'last_name'}
              value={values['last_name']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput isRequired={true} title={'email'} name={'email'} value={values['email']} />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              title={'password'}
              name={'password'}
              value={values['password']}
            />
          </div>

          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={false}
              title={'phone_number'}
              name={'phone_number'}
              value={values['phone_number']}
            />
          </div>

          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={false}
              title={'civil_registry'}
              name={'civil_registry'}
              value={values['civil_registry']}
            />
          </div>

          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              title={'bank_account_number'}
              name={'bank_account_number'}
              value={values['bank_account_number']}
            />
          </div>
          <div className='col-md-3 col-sm-12 '>
            <FormikToggle title={'enable'} isRequired={false} name={'enabled'} />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikSelect isRequired={true} name='role_id' title='role' options={listRoles} />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={false}
              type='file'
              title={'image'}
              name={'image-empty'}
              onChangeEvent={(event) => {
                setFieldValue('image', event?.target?.files[0])
              }}
              value={''}
            />
          </div>
          {/* begin::Actions */}
          <div className='text-center pt-15'>
            <ResetButton resetForm={resetForm} isSubmitting={isSubmitting} />
            <SubmitButton
              isSubmitting={isSubmitting}
              isValid={isValid}
              touched={touched}
              onclick={handleSubmit}
            />
          </div>
          {/* end::Actions */}
        </div>
      </div>
      {isSubmitting && <Loading />}
    </div>
  )
}

export {Form}
