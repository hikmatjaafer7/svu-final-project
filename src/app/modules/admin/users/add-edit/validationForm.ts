import {useIntl} from 'react-intl'
import * as Yup from 'yup'

export const RoleSchema = () => {
  const intl = useIntl()
  const validations = Yup.object().shape({
    first_name: Yup.string().required(intl.formatMessage({id: 'field_is_required'})),
    last_name: Yup.string().required(intl.formatMessage({id: 'field_is_required'})),
    email: Yup.string()
    .email()
    .required(intl.formatMessage({id: 'field_is_required'})),
    password: Yup.string()
    .min(8)
    .required(intl.formatMessage({id: 'field_is_required'})),
    role_id: Yup.string().required(),
    bank_account_number: Yup.string()
    .matches(/^[A-Z]{2}\d{2}[A-Z0-9]{1,30}$/, 'IBAN غير صالح')
    .required('IBAN مطلوب'),
  })
  return validations
}
