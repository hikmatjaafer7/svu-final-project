import {UsersTable} from './table'
import {KTCard, QUERIES} from '../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponseRefetch,
} from '../../../core/GenericQueryResonseProvider'
import {
  GenericQueryRequestProvider,
  useGenericQueryRequest,
} from '../../../core/GenericQueryRequestProvider'
import {deleteSelectedUsers, getUsers} from './core/_requests'
import {ListHeader} from '../../../components/Table/header/ListHeader'
import {useState} from 'react'
import {Loading} from '../../../components/Loading'
import {useNotification} from '../../../../_metronic/hooks/useNotification'
import {useNavigate} from 'react-router-dom'

const UsersList = () => {
  const {itemIdForUpdate} = useGenericListView()
  const refetch = useGenericQueryResponseRefetch()
  // const {selected} = useGenericListView()
  const [isLoading, setIsLoading] = useState(false)
  const {showNotification} = useNotification()
  const navigate = useNavigate()
  return (
    <>
      <KTCard>
        <ListHeader
          handleAdd={() => {
            navigate('/admin/users-management/add-user')
          }}
          onFilterSubmit={(filterUpdateState) => {}}
          ListFilterChildren={<></>}
          queryString={QUERIES.USERS_LIST}
          deleteSelected={(selected) => {
            setIsLoading(true)
            deleteSelectedUsers(selected)
            .then((res) => {
              setIsLoading(false)
              showNotification({result: 'success', data: '', error_description: ''})
              refetch()
            })
            .catch((err) => {
              setIsLoading(false)
              showNotification({result: 'error', data: '', error_description: 'error'})
              refetch()
            })
          }}
        />
        <UsersTable />
        {isLoading && <Loading />}
      </KTCard>
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <UsersList />
    </GenericListViewProvider>
  )
}

const UsersListWrapper = () => {
  return (
    <GenericQueryRequestProvider>
      <GenericQueryResponseProvider getApi={getUsers} queryString={QUERIES.USERS_LIST}>
        <ListView />
      </GenericQueryResponseProvider>
    </GenericQueryRequestProvider>
  )
}

export {UsersListWrapper}
