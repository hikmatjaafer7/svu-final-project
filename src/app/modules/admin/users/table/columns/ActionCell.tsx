/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC, ReactNode, useEffect} from 'react'
import {MenuComponent} from '../../../../../../_metronic/assets/ts/components'
import {ID, KTIcon, QUERIES} from '../../../../../../_metronic/helpers'
import {useNavigate} from 'react-router-dom'
import {useMutation, useQueryClient} from 'react-query'
import {deleteUser} from '../../core/_requests'
import {useGenericQueryResponse} from '../../../../../core/GenericQueryResonseProvider'
import {useIntl} from 'react-intl'

type Props = {
  data: any
  menuItems: {label: string; onClick: any; menuElement?: ReactNode}[]
}

const ActionsCell: FC<Props> = ({data}) => {
  useEffect(() => {
    MenuComponent.reinitialization()
  }, [])
  const intl = useIntl()
  const {query} = useGenericQueryResponse()

  const queryClient = useQueryClient()
  const deleteItem = useMutation(() => deleteUser(data.id), {
    // 💡 response of the mutation is passed to onSuccess
    onSuccess: () => {
      // ✅ update detail view directly
      queryClient.invalidateQueries([`${QUERIES.USERS_LIST}-${query}`])
    },
  })
  const navigate = useNavigate()

  const roleItem = () => {
    navigate('/admin/assign-user-role')
  }

  const editItem = () => {
    navigate('/admin/users-management/edit-user', {state: data, replace: true})
  }
  const subscriptionsItem = () => {
    navigate('/admin/subscriptions-management/user-subscriptions', {state: data, replace: true})
  }
  const loansItem = () => {
    navigate('/admin/loans-management/user-loans', {state: data, replace: true})
  }
  const fundsItem = () => {
    navigate('/admin/users-management/user-funds', {state: data, replace: true})
  }
  return (
    <>
      <a
        href='#'
        className='btn btn-light btn-active-light-primary btn-sm text-nowrap'
        data-kt-menu-trigger='click'
        data-kt-menu-placement='bottom-end'
      >
        {intl.formatMessage({id: 'actions'})}
        <KTIcon iconName='down' className='fs-5 m-0' />
      </a>
      {/* begin::Menu */}
      <div
        className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4'
        data-kt-menu='true'
      >
        {/* begin::Menu items */}
        {[
          {
            label: intl.formatMessage({id: 'delete'}),
            onClick: async () => await deleteItem.mutateAsync(),
          },
          {label: intl.formatMessage({id: 'edit'}), onClick: editItem},
          {label: intl.formatMessage({id: 'subscriptions'}), onClick: subscriptionsItem},
          {label: intl.formatMessage({id: 'loans'}), onClick: loansItem},
          {label: intl.formatMessage({id: 'funds'}), onClick: fundsItem},
        ].map((item) => {
          return (
            <div className='menu-item px-3'>
              {
                <a className='menu-link px-3' onClick={item.onClick}>
                  {item.label}
                </a>
              }
            </div>
          )
        })}
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  )
}

export {ActionsCell}
