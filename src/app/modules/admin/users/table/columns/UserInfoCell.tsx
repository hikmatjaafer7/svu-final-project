/* eslint-disable jsx-a11y/anchor-is-valid */
import clsx from 'clsx'
import {FC} from 'react'
import {toAbsoluteUrl} from '../../../../../../_metronic/helpers'
import {User} from '../../core/_models'

type Props = {
  user: User
}

const UserInfoCell: FC<Props> = ({user}) => (
  <div className='d-flex align-items-center'>
    {/* begin:: Avatar */}
    <div className='symbol symbol-circle symbol-50px overflow-hidden me-3'>
      <a href='#'>
        {user?.image ? (
          <div className='symbol-label'>
            <img
              src={process.env.REACT_APP_PROJECT_IMAGE_URL + '/' + user?.image}
              // src={toAbsoluteUrl(
              //   `/media/${user.avatar === 'avatars/300-6.jpg' ? 'avatars/300-1.jpg' : user.avatar}`
              // )}
              alt={user.first_name + ' ' + user.last_name}
              className='w-100'
            />
          </div>
        ) : (
          <div className={clsx('symbol-label fs-3', `bg-light-primary`, `text-primary`)}>
            {user.first_name?.slice(0, 1).toUpperCase()}
          </div>
        )}
      </a>
    </div>
    <div className='d-flex flex-column'>
      <a href='#' className='text-gray-800 text-hover-primary mb-1'>
        {user.first_name + ' ' + user.last_name}
      </a>
      <span>{user.email}</span>
    </div>
  </div>
)

export {UserInfoCell}
