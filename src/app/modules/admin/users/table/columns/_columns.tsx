// @ts-nocheck
import {Column} from 'react-table'
import {UserInfoCell} from './UserInfoCell'
import {User} from '../../core/_models'
import {ActionsCell} from './ActionCell'
import {Localize} from '../../../../../../_metronic/i18n/Localize'
import * as Columns from '../../../../../components/Table/columns'

const usersColumns: ReadonlyArray<Column<User>> = [
  {
    Header: (props) => <Columns.SelectionHeader tableProps={props} />,
    id: 'selection',
    Cell: ({...props}) => <Columns.SelectionCell id={props.data[props.row.index].id} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='name' />}
        className='min-w-125px'
      />
    ),
    id: 'name',
    Cell: ({...props}) => <UserInfoCell user={props.data[props.row.index]} />,
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='bank_account_number' />}
        className='min-w-125px'
      />
    ),
    accessor: 'bank_account_number',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='role' />}
        className='min-w-125px'
      />
    ),
    id: 'role',
    accessor: 'Role.name',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='subscriptions_value_total' />}
        className='min-w-125px'
      />
    ),
    accessor: 'subscriptionsSum',
  },
  {
    Header: (props) => (  
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='loans_value_total' />}
        className='min-w-125px'
      />
    ),
    accessor: 'loansSum',
  },
  {
    Header: (props) => (  
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='subscriptions_payments_total' />}
        className='min-w-125px'
      />
    ),
    accessor: 'subscriptionPaymentSum',
  },
  {
    Header: (props) => (  
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='loans_payments_total' />}
        className='min-w-125px'
      />
    ),
    accessor: 'loanPaymentSum',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='net_balance' />}
        className='min-w-125px'
      />
    ),
    accessor: 'netBalance',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='subscriptions_count' />}
        className='min-w-125px'
      />
    ),
    accessor: 'subscriptionsCount',
  },

  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='loans_count' />}
        className='min-w-125px'
      />
    ),
    accessor: 'loansCount',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='loan_alarms_count' />}
        className='min-w-125px'
      />
    ),
    accessor: 'loanAlarmsCount',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='subscription_alarm_count' />}
        className='min-w-125px'
      />
    ),
    accessor: 'subscriptionAlarmsCount',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='joined_date' />}
        className='min-w-125px'
      />
    ),
    accessor: 'createdAt',
  },
  {
    Header: (props) => (
      <Columns.CustomHeader
        tableProps={props}
        title={<Localize value='actions' />}
        className='text-end min-w-100px'
      />
    ),
    id: 'actions',
    Cell: ({...props}) => <ActionsCell data={props.data[props.row.index]} />,
  },
]

export {usersColumns}
