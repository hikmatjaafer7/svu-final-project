import axios, {AxiosResponse} from 'axios'
import {ID, ResponeApiCheck, Response} from '../../../../../_metronic/helpers'
import {User, UsersQueryResponse} from './_models'
import {getList, dele, create} from '../../../../../services/crud-api'
import {get, post} from '../../../../../services/common-http'
const API_URL = process.env.REACT_APP_PROJECT_API_URL
const USER_URL = `${API_URL}/users`
const GET_USERS_URL = `${API_URL}/users/query`

const getUsers = async (query: string) => {
  const data = await getList(`${USER_URL}?` + query)
  return data
  // .then((d: AxiosResponse<UsersQueryResponse>) => d)
}

const getUserById = (id: ID): Promise<User | undefined> => {
  return axios
  .get(`${USER_URL}/${id}`)
  .then((response: AxiosResponse<Response<User>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: Response<User>) => response.data)
}

const createUser = (user: User): Promise<ResponeApiCheck | undefined> => {
 
  const formData = new FormData()
  Object.keys(user).forEach((key) => {
    formData.append(key, user[key])
  })

  return axios
  .post(`${USER_URL}`, formData)
  .then((response: AxiosResponse<Response<ResponeApiCheck>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: ResponeApiCheck) => {
    console.log('response :>> ', response)
    return response
  })
  // return create(`${USER_URL}/`, user)
  // return axios
  //   .put(USER_URL, user)
  //   .then((response: AxiosResponse<Response<User>>) => response.data)
  //   .then((response: Response<User>) => response.data)
}

const updateUser = (user: User): Promise<ResponeApiCheck | undefined> => {
  const formData = new FormData()
  Object.keys(user).forEach((key) => {
    formData.append(key, user[key])
  })

  return axios
  .put(`${USER_URL}/${user.id}`, formData)
  .then((response: AxiosResponse<Response<ResponeApiCheck>>) => {
    console.log('response :>> ', response)
    return response.data
  })
  .then((response: ResponeApiCheck) => {
    console.log('response :>> ', response)
    return response
  })
}

const deleteUser = async (userId: ID) => {
  return await dele(`${USER_URL}/`, userId)
}

const deleteSelectedUsers = (userIds: Array<ID>): Promise<void> => {
  const requests = userIds.map((id) => axios.delete(`${USER_URL}/${id}`))
  return axios.all(requests).then(() => {})
}

const getRoles = async () => {
  const res = await getList(`${API_URL}/roles/`)
  return res.data?.map((item) => {
    return {key: item.id, value: item.name}
  })
  // .then((d: AxiosResponse<UsersQueryResponse>) => d)
}

const getUsersByFundId = async (fund_id: ID): Promise<any> => {
  return await get(`${USER_URL}/fund/${fund_id}`)
}

const getLoanUsersByFundId = async (fund_id: ID): Promise<any> => {
  return await get(`${USER_URL}/fund/loan/${fund_id}`)
}

export {
  getUsers,
  deleteUser,
  deleteSelectedUsers,
  getUserById,
  createUser,
  updateUser,
  getRoles,
  getUsersByFundId,
  getLoanUsersByFundId,
}
