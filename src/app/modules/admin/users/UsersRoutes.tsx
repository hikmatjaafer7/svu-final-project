import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../../_metronic/layout/core'
import {useIntl} from 'react-intl'
import {UsersListWrapper} from './UsersListWrapper'
// import PermissionForm from './add-edit/AddEditRole'
import {Form} from './add-edit/Form'
import Add from './add-edit/Add'
import Edit from './add-edit/Edit'

const UsersRoutes = () => {
  const intl = useIntl()
  const usersBreadcrumbs: Array<PageLink> = [
    {
      title: intl.formatMessage({id: 'user_management'}),
      path: 'admin/users',
      isSeparator: false,
      isActive: false,
    },
    {
      title: '',
      path: '',
      isSeparator: true,
      isActive: false,
    },
  ]
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/users'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'users_list'})}
              </PageTitle>
              <UsersListWrapper />
            </>
          }
        />
        <Route
          path='/edit-user'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'edit_user'})}
              </PageTitle>
              <Edit />
            </>
          }
        />
        <Route
          path='/add-user'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>
                {intl.formatMessage({id: 'add_user'})}
              </PageTitle>
              <Add />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='/admin/users-management/users' />} />
    </Routes>
  )
}

export default UsersRoutes
