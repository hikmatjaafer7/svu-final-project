import {Formik} from 'formik'
import {
  KTCard,
  KTCardBody,
  ResponeApiCheck,
  initialResponseError,
} from '../../../../../_metronic/helpers'
import {Form} from './Form'
import {RoleSchema} from './validationForm'
import {updateLoan} from '../core/_requests'
import {useNotification} from '../../../../../_metronic/hooks/useNotification'
import {useLocation, useNavigate} from 'react-router-dom'

const Edit = () => {
  const navigate = useNavigate()
  const {showNotification} = useNotification()
  const location = useLocation()
  const data: any = location.state
  return (
    <KTCard>
      <KTCardBody className='py-4'>
        {data && (
          <Formik
            enableReinitialize={true}
            validationSchema={RoleSchema()}
            initialValues={{...data, FundId: data?.fund_id, UserId: data?.user_id}}
            initialStatus={{edit: false}}
            onSubmit={async (values: any, {setSubmitting}) => {
              setSubmitting(true)
              try {
                const res: ResponeApiCheck = await updateLoan({
                  ...values,
                  UserId: data?.user_id,
                })
                if (res.result == 'success') {
                  navigate('/admin/loans-management/user-loans', {
                    state: {id: data?.user_id},
                  })
                }
                showNotification(res)
              } catch (ex) {
                showNotification({error_description: ex, ...initialResponseError})
                console.error(ex)
              } finally {
                setSubmitting(true)
              }
            }}
            onReset={(values) => {}}
          >
            <Form />
          </Formik>
        )}
      </KTCardBody>
    </KTCard>
  )
}

export default Edit
