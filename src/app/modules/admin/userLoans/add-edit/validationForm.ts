import {useIntl} from 'react-intl'
import * as Yup from 'yup'

export const RoleSchema = () => {
  const intl = useIntl()
  const validations = Yup.object().shape({
    FundId: Yup.number().required(intl.formatMessage({id: 'field_is_required'})),
    UserId: Yup.number().required(intl.formatMessage({id: 'field_is_required'})),
    value: Yup.number().required(intl.formatMessage({id: 'field_is_required'})),
  })
  return validations
}
