import {FC, useState} from 'react'
import {useFormikContext} from 'formik'
import {useIntl} from 'react-intl'
// import {ProfileImage} from '../../../../../_metronic/utlis/formik'
import {useQuery} from 'react-query'
import {QUERIES} from '../../../../../_metronic/helpers'

import {Loading} from '../../../../components/Loading'
import FormikInput from '../../../../components/formik/FormikInput'
import FormikToggle from '../../../../components/formik/FormikToggle'
import SubmitButton from '../../../../components/SubmitButton'
import ResetButton from '../../../../components/ResetButton'
import FormikSelect from '../../../../components/formik/FormikSelect'
import FormikInputDate from '../../../../components/formik/FormikInputDate'
import {getFunds} from '../../funds/core/_requests'
import {getUsers} from '../../users/core/_requests'
import RequestStatus from '../../../../core/enums/RequestStatus'
import {getSubscriptionsByUserId} from '../../../user/tracking_requests/subscriptions/core/_requests'
import {useAuth} from '../../../auth'

const Form: FC = (props: any) => {
  const intl = useIntl()

  // const [errorMsg, setErrorMsg] = useState({show: false, message: ''})
  // const navigate = useNavigate()
  const [isLoading, setIsLoading] = useState(false)
  // const handleSubmit = () => {
  //   setIsLoading(true)
  //   createUser(model).then((res) => {
  //     navigate('/admin/users')
  //     setIsLoading(false)
  //   })
  // }
  const {data: listFunds} = useQuery(
    QUERIES.FUNDS_LIST,
    () => {
      return getFunds('').then((resData) => {
        return [
          {
            key: 0,
            value: 'اختر من القائمة',
          },
          ...resData?.data?.map((item, index) => {
            return {
              key: item.id,
              value: item.name,
            }
          }),
        ]
      })
    },
    {
      refetchOnMount: true,
    }
  )
  const {currentUser} = useAuth()
  // const {data: listFunds} = useQuery(
  //   QUERIES.SUBSCRIPTIONS_BY_USER_LIST,
  //   () => {
  //     return getSubscriptionsByUserId(currentUser.id, '').then((resData) => {
  //       const uniqueFunds = Object.values(
  //         resData?.data?.reduce((acc, item) => {
  //           const fundId = item.Fund.id
  //           if (!acc[fundId]) {
  //             acc[fundId] = {
  //               key: item.Fund.id,
  //               value: item.Fund.name,
  //             }
  //           }
  //           return acc
  //         }, {})
  //       )

  //       return [
  //         {
  //           key: 0,
  //           value: 'اختر من القائمة',
  //         },
  //         ...uniqueFunds,
  //       ]
  //     })
  //   },
  //   {
  //     refetchOnMount: true,
  //   }
  // )

  // const {data: listUsers} = useQuery(
  //   QUERIES.USERS_LIST,
  //   () => {
  //     return getUsers('').then((resData) => {
  //       return [
  //         {
  //           key: 0,
  //           value: 'اختر من القائمة',
  //         },
  //         ...resData?.data?.map((item, index) => {
  //           return {
  //             key: item.id,
  //             value: `${item.first_name} ${item.last_name}`,
  //           }
  //         }),
  //       ]
  //     })
  //   },
  //   {
  //     refetchOnMount: true,
  //   }
  // )

  const {handleSubmit, resetForm, isSubmitting, isValid, touched, values, setFieldValue, errors} =
    useFormikContext()
  console.log('errors :>> ', errors)
  return (
    <div>
      <div className='d-flex flex-column scroll-y me-n7 pe-7 bg-white ps-7 pt-7 pb-7'>
        <div className='row'>
          <div className='col-md-3 col-sm-12'>
            <FormikSelect isRequired={true} name='FundId' title='fund' options={listFunds} />
          </div>
          {/* <div className='col-md-3 col-sm-12'>
            <FormikSelect isRequired={true} name='UserId' title='user' options={listUsers} />
          </div> */}
          <div className='col-md-3 col-sm-12'>
            <FormikSelect
              isRequired={false}
              name='status'
              title='status'
              options={Object.values(RequestStatus).map((status) => ({
                key: status,
                value: intl.formatMessage({id: status}),
              }))}
            />
          </div>{' '}
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={true}
              min={1}
              type='number'
              title={'value'}
              name={'value'}
              value={values['value']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikInput
              isRequired={false}
              title={'reason'}
              name={'reason'}
              value={values['reason']}
            />
          </div>
          <div className='col-md-3 col-sm-12'>
            <FormikSelect
              isRequired={false}
              name='type'
              title='type'
              options={[
                {key: 'ordinary', value: intl.formatMessage({id: 'ordinary'})},
                {key: 'emergency', value: intl.formatMessage({id: 'emergency'})},
              ]}
            />
          </div>{' '}
          {/* begin::Actions */}
          <div className='text-center pt-15'>
            <ResetButton resetForm={resetForm} isSubmitting={isSubmitting} />
            <SubmitButton
              isSubmitting={isSubmitting}
              isValid={isValid}
              touched={touched}
              onclick={handleSubmit}
            />
          </div>
          {/* end::Actions */}
        </div>
      </div>
      {isSubmitting && <Loading />}
    </div>
  )
}

export {Form}
