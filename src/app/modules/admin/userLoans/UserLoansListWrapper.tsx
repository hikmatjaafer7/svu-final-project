import {LoansTable} from './table'
import {KTCard, QUERIES} from '../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponseRefetch,
} from '../../../core/GenericQueryResonseProvider'
import {
  GenericQueryRequestProvider,
  useGenericQueryRequest,
} from '../../../core/GenericQueryRequestProvider'
import {deleteSelectedLoans, getLoansByFundId, getLoansByUserId} from './core/_requests'
import {ListHeader} from '../../../components/Table/header/ListHeader'
import {useState} from 'react'
import {Loading} from '../../../components/Loading'
import {useNotification} from '../../../../_metronic/hooks/useNotification'
import {useLocation, useNavigate} from 'react-router-dom'
import Select from '../../../components/Select'
import {useQuery} from 'react-query'
import {getFunds} from '../funds/core/_requests'
import Button from '../../../components/Button'
import {getSubscriptionsByUserId} from '../../user/tracking_requests/subscriptions/core/_requests'
import {useAuth} from '../../auth'
import {useIntl} from 'react-intl'

const UserLoansList = ({type, setType}) => {
  const {itemIdForUpdate} = useGenericListView()
  const refetch = useGenericQueryResponseRefetch()
  // const {selected} = useGenericListView()
  const [isLoading, setIsLoading] = useState(false)
  const {showNotification} = useNotification()
  const navigate = useNavigate()
  const location: any = useLocation()
  const intl = useIntl()
  const state: any = location.state
  const submitUserId = state?.id || 0
  return (
    <>
      <KTCard>
        <ListHeader
          handleAdd={() => {
            navigate('/admin/loans-management/add-user-loan', {
              state: {id: submitUserId},
            })
          }}
          withResetBtn={false}
          withSubmitBtn={false}
          onFilterSubmit={(filterUpdateState) => {
            // filterUpdateState({filter: {loan_type: type}})
          }}
          ListFilterChildren={
            <div className='mb-5'>
              <Select
                options={[
                  {key: 'null', value: intl.formatMessage({id: 'all'})},
                  {key: 'ordinary', value: intl.formatMessage({id: 'ordinary'})},
                  {key: 'emergency', value: intl.formatMessage({id: 'emergency'})},
                ]}
                selectedValue={type}
                onChange={(type: any) => setType(type)}
                title='type'
              />
            </div>
          }
          queryString={QUERIES.LOANS_BY_USER_LIST + type}
          deleteSelected={(selected) => {
            setIsLoading(true)
            deleteSelectedLoans(selected)
            .then((res) => {
              setIsLoading(false)
              showNotification({result: 'success', data: '', error_description: ''})
              refetch()
            })
            .catch((err) => {
              setIsLoading(false)
              showNotification({result: 'error', data: '', error_description: 'error'})
              refetch()
            })
          }}
        />
        <LoansTable />
        {isLoading && <Loading />}
      </KTCard>
    </>
  )
}

const ListView = ({type, setType}) => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <UserLoansList type={type} setType={setType} />
    </GenericListViewProvider>
  )
}

const UserLoansListWrapper = () => {
  // const [fundId, setFundId] = useState(0)
  // const [, setSubmitUserId] = useState(0)
  const location: any = useLocation()
  const state: any = location.state
  const submitUserId = state?.id || 0
  const data = useGenericQueryResponseData()
  const {currentUser} = useAuth()

  const [type, setType] = useState(null)

  return (
    <>
      {/* <div>
        <KTCard>
          <div className='row m-4'>
            <div className='col-3'>
              <Select
                selectedValue={fundId}
                onChange={(fundId: any) => setFundId(fundId)}
                title='fund'
                options={listFunds || []}
              />
            </div>
            <div className='col-2 mt-7'>
              <Button
                Disabled={fundId <= 0}
                label='search'
                onClick={() => {
                  setSubmitUserId(fundId)
                }}
              />
            </div>
          </div>
        </KTCard>
      </div>
      <hr /> */}
      {submitUserId > 0 && (
        <GenericQueryRequestProvider>
          <GenericQueryResponseProvider
            getApi={getLoansByUserId}
            urlParameter={{userId: submitUserId.toString(), type}}
            queryString={QUERIES.LOANS_BY_USER_LIST + type}
          >
            <ListView type={type} setType={setType} />
          </GenericQueryResponseProvider>
        </GenericQueryRequestProvider>
      )}
    </>
  )
}

export {UserLoansListWrapper}
