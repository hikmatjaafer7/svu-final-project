import {ListViewProvider, useListView} from './core/ListViewProvider'
import {QueryRequestProvider} from './core/QueryRequestProvider'
import {QueryResponseProvider} from './core/QueryResponseProvider'
import {UsersListHeader} from './components/header/UsersListHeader'
import {UsersTable} from './table/UsersTable'
import {UserEditModal} from './user-edit-modal/UserEditModal'
import {KTCard, QUERIES} from '../../../../../_metronic/helpers'
import {GenericListViewProvider, useGenericListView} from '../../../../core/GenericListViewProvider'
import {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
} from '../../../../core/GenericQueryResonseProvider'
import {GenericQueryRequestProvider} from '../../../../core/GenericQueryRequestProvider'
import {getUsers} from './core/_requests'

const UsersList = () => {
  const {itemIdForUpdate} = useGenericListView()
  return (
    <>
      <KTCard>
        <UsersListHeader />
        <UsersTable />
      </KTCard>
      {itemIdForUpdate !== undefined && <UserEditModal />}
    </>
  )
}

const ListView = () => {
  const {isLoading} = useGenericQueryResponse()
  const data = useGenericQueryResponseData()
  return (
    <GenericListViewProvider data={data} isLoading={isLoading}>
      <UsersList />
    </GenericListViewProvider>
  )
}

const UsersListWrapper = () => {
  return (
    <GenericQueryRequestProvider>
      <GenericQueryResponseProvider getApi={getUsers} queryString={QUERIES.USERS_LIST}>
        <ListView />
      </GenericQueryResponseProvider>
    </GenericQueryRequestProvider>
  )
}

export {UsersListWrapper}
