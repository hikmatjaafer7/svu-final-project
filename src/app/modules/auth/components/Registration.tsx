/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable jsx-a11y/anchor-is-valid */
import {useState, useEffect} from 'react'
import {useFormik} from 'formik'
import * as Yup from 'yup'
import clsx from 'clsx'
import {getUserByToken, register} from '../core/_requests'
import {Link} from 'react-router-dom'
import {toAbsoluteUrl} from '../../../../_metronic/helpers'
import {PasswordMeterComponent} from '../../../../_metronic/assets/ts/components'
import {useAuth} from '../core/Auth'
import {useNotification} from '../../../../_metronic/hooks/useNotification'
import {useIntl} from 'react-intl'

const initialValues = {
  firstname: '',
  lastname: '',
  email: '',
  password: '',
  changepassword: '',
  acceptTerms: false,
  bank_account_number: '',
}

const registrationSchema = Yup.object().shape({
  firstname: Yup.string()
  .min(3, 'الحد الأدنى 3 رموز')
  .max(50, 'الحد الأقصى 50 رمزًا')
  .required('الاسم الأول مطلوب'),
  email: Yup.string()
  .email('تنسيق البريد الإلكتروني خاطئ')
  .min(3, 'الحد الأدنى 3 رموز')
  .max(50, 'الحد الأقصى 50 رمزًا')
  .required('البريد الإلكتروني مطلوب'),
  lastname: Yup.string()
  .min(3, 'الحد الأدنى 3 رموز')
  .max(50, 'الحد الأقصى 50 رمزًا')
  .required('حقل الكنية مطلوب'),
  password: Yup.string()
  .min(3, 'الحد الأدنى 3 رموز')
  .max(50, 'الحد الأقصى 50 رمزًا')
  .required('حقل كلمة السر مطلوب'),
  changepassword: Yup.string()
  .min(3, 'الحد الأدنى 3 رموز')
  .max(50, 'الحد الأقصى 50 رمزًا')
  .required('مطلوب تأكيد كلمة المرور')
  .oneOf([Yup.ref('password')], 'كلمة المرور وتأكيد كلمة المرور غير متطابقين'),
  acceptTerms: Yup.bool().required('يجب عليك قبول الأحكام والشروط'),
  bank_account_number: Yup.string()
  .matches(/^[A-Z]{2}\d{2}[A-Z0-9]{1,30}$/, 'IBAN غير صالح')
  .required('IBAN مطلوب'),
})

export function Registration() {
  const [loading, setLoading] = useState(false)
  const {saveAuth, setCurrentUser} = useAuth()
  const intl = useIntl()
  const {showNotification} = useNotification()
  const formik = useFormik({
    initialValues,
    validationSchema: registrationSchema,
    onSubmit: async (values, {setStatus, setSubmitting}) => {
      setLoading(true)
      try {
        const {data: auth} = await register(
          values.email,
          values.firstname,
          values.lastname,
          values.password,
          values.changepassword,
          values.bank_account_number
        )
        saveAuth(auth)
        const {data: user} = await getUserByToken(auth.api_token)
        setCurrentUser(user)
      } catch (error) {
        console.log('error :>> ', error)
        saveAuth(undefined)
        showNotification(error, error?.response?.data?.message || error.message)
        setStatus('The registration details is incorrect')
        setSubmitting(false)
        setLoading(false)
      }
    },
  })

  useEffect(() => {
    PasswordMeterComponent.bootstrap()
  }, [])

  return (
    <form
      className='form w-100 fv-plugins-bootstrap5 fv-plugins-framework'
      noValidate
      id='kt_login_signup_form'
      onSubmit={formik.handleSubmit}
    >
      {formik.status && (
        <div className='mb-lg-15 alert alert-danger'>
          <div className='alert-text font-weight-bold'>{formik.status}</div>
        </div>
      )}

      <div className='row row-cols-2 g-5 mb-8'>
        <div className='col'>
          <label className='form-label fw-bolder text-dark fs-6'>
            {intl.formatMessage({id: 'first_name'})}
          </label>
          <input
            placeholder={intl.formatMessage({id: 'first_name'})}
            type='text'
            autoComplete='off'
            {...formik.getFieldProps('firstname')}
            className={clsx(
              'form-control bg-transparent',
              {
                'is-invalid': formik.touched.firstname && formik.errors.firstname,
              },
              {
                'is-valid': formik.touched.firstname && !formik.errors.firstname,
              }
            )}
          />
          {formik.touched.firstname && formik.errors.firstname && (
            <div className='fv-plugins-message-container'>
              <div className='fv-help-block'>
                <span role='alert'>{formik.errors.firstname}</span>
              </div>
            </div>
          )}
        </div>

        <div className='col'>
          {/* begin::Form group Lastname */}
          <label className='form-label fw-bolder text-dark fs-6'>
            {intl.formatMessage({id: 'last_name'})}
          </label>
          <input
            placeholder={intl.formatMessage({id: 'last_name'})}
            type='text'
            autoComplete='off'
            {...formik.getFieldProps('lastname')}
            className={clsx(
              'form-control bg-transparent',
              {
                'is-invalid': formik.touched.lastname && formik.errors.lastname,
              },
              {
                'is-valid': formik.touched.lastname && !formik.errors.lastname,
              }
            )}
          />
          {formik.touched.lastname && formik.errors.lastname && (
            <div className='fv-plugins-message-container'>
              <div className='fv-help-block'>
                <span role='alert'>{formik.errors.lastname}</span>
              </div>
            </div>
          )}
          {/* end::Form group */}
        </div>
      </div>

      <div className='fv-row mb-8'>
        <label className='form-label fw-bolder text-dark fs-6'>
          {intl.formatMessage({id: 'رقم الحساب البنكي'})}
        </label>
        <input
          placeholder={intl.formatMessage({id: 'IBAN Code'})}
          type='text'
          autoComplete='off'
          {...formik.getFieldProps('bank_account_number')}
          className={clsx(
            'form-control bg-transparent',
            {
              'is-invalid': formik.touched.bank_account_number && formik.errors.bank_account_number,
            },
            {
              'is-valid': formik.touched.bank_account_number && !formik.errors.bank_account_number,
            }
          )}
        />
        {formik.touched.bank_account_number && formik.errors.bank_account_number && (
          <div className='fv-plugins-message-container'>
            <div className='fv-help-block'>
              <span role='alert'>{formik.errors.bank_account_number}</span>
            </div>
          </div>
        )}
      </div>
      {/* begin::Form group Email */}
      <div className='fv-row mb-8'>
        <label className='form-label fw-bolder text-dark fs-6'>
          {intl.formatMessage({id: 'email'})}
        </label>
        <input
          // placeholder={intl.formatMessage({id: 'email'})}
          type='email'
          autoComplete='off'
          {...formik.getFieldProps('email')}
          className={clsx(
            'form-control bg-transparent',
            {'is-invalid': formik.touched.email && formik.errors.email},
            {
              'is-valid': formik.touched.email && !formik.errors.email,
            }
          )}
        />
        {formik.touched.email && formik.errors.email && (
          <div className='fv-plugins-message-container'>
            <div className='fv-help-block'>
              <span role='alert'>{formik.errors.email}</span>
            </div>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Form group Password */}
      <div className='fv-row mb-8' data-kt-password-meter='true'>
        <div className='mb-1'>
          <label className='form-label fw-bolder text-dark fs-6'>
            {intl.formatMessage({id: 'password'})}
          </label>
          <div className='position-relative mb-3'>
            <input
              type='password'
              placeholder={intl.formatMessage({id: 'password'})}
              autoComplete='off'
              {...formik.getFieldProps('password')}
              className={clsx(
                'form-control bg-transparent',
                {
                  'is-invalid': formik.touched.password && formik.errors.password,
                },
                {
                  'is-valid': formik.touched.password && !formik.errors.password,
                }
              )}
            />
            {formik.touched.password && formik.errors.password && (
              <div className='fv-plugins-message-container'>
                <div className='fv-help-block'>
                  <span role='alert'>{formik.errors.password}</span>
                </div>
              </div>
            )}
          </div>
          {/* begin::Meter */}
          <div
            className='d-flex align-items-center mb-3'
            data-kt-password-meter-control='highlight'
          >
            <div className='flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2'></div>
            <div className='flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2'></div>
            <div className='flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2'></div>
            <div className='flex-grow-1 bg-secondary bg-active-success rounded h-5px'></div>
          </div>
          {/* end::Meter */}
        </div>
        <div className='text-muted'>{intl.formatMessage({id: 'passowrd_confition_message'})}</div>
      </div>
      {/* end::Form group */}

      {/* begin::Form group Confirm password */}
      <div className='fv-row mb-5'>
        <label className='form-label fw-bolder text-dark fs-6'>
          {intl.formatMessage({id: 'confirm_password'})}
        </label>
        <input
          type='password'
          placeholder={intl.formatMessage({id: 'confirm_password'})}
          autoComplete='off'
          {...formik.getFieldProps('changepassword')}
          className={clsx(
            'form-control bg-transparent',
            {
              'is-invalid': formik.touched.changepassword && formik.errors.changepassword,
            },
            {
              'is-valid': formik.touched.changepassword && !formik.errors.changepassword,
            }
          )}
        />
        {formik.touched.changepassword && formik.errors.changepassword && (
          <div className='fv-plugins-message-container'>
            <div className='fv-help-block'>
              <span role='alert'>{formik.errors.changepassword}</span>
            </div>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className='fv-row mb-8'>
        <label className='form-check form-check-inline' htmlFor='kt_login_toc_agree'>
          <input
            className='form-check-input'
            type='checkbox'
            id='kt_login_toc_agree'
            {...formik.getFieldProps('acceptTerms')}
          />
          <span>
            {intl.formatMessage({id: 'i_accept_the'})}{' '}
            <a
              href='https://keenthemes.com/metronic/?page=faq'
              target='_blank'
              className='ms-1 link-primary'
            >
              {intl.formatMessage({id: 'terms'})}
            </a>
            .
          </span>
        </label>
        {formik.touched.acceptTerms && formik.errors.acceptTerms && (
          <div className='fv-plugins-message-container'>
            <div className='fv-help-block'>
              <span role='alert'>{formik.errors.acceptTerms}</span>
            </div>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className='text-center'>
        <button
          type='submit'
          id='kt_sign_up_submit'
          className='btn btn-lg btn-primary w-100 mb-5'
          disabled={formik.isSubmitting || !formik.isValid || !formik.values.acceptTerms}
        >
          {!loading && (
            <span className='indicator-label'> {intl.formatMessage({id: 'submit'})}</span>
          )}
          {loading && (
            <span className='indicator-progress' style={{display: 'block'}}>
              {intl.formatMessage({id: 'please_wait...'})}{' '}
              <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
            </span>
          )}
        </button>
        <Link to='/auth/login'>
          <button
            type='button'
            id='kt_login_signup_form_cancel_button'
            className='btn btn-lg btn-light-primary w-100 mb-5'
          >
            {intl.formatMessage({id: 'cancel'})}{' '}
          </button>
        </Link>
      </div>
      {/* end::Form group */}
    </form>
  )
}
