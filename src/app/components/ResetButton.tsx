import React from 'react'
import {useIntl} from 'react-intl'

interface props {
  isSubmitting: boolean
  resetForm: () => void
  id?:any
}
const ResetButton = (props: props) => {
  const {resetForm, isSubmitting} = props
  const intel = useIntl()

  return (
    <button
    id={props.id}
      type='reset'
      onClick={() => resetForm()}
      className='btn btn-light me-3'
      data-kt-users-modal-action='cancel'
      disabled={isSubmitting}
    >
      {intel.formatMessage({id: 'discard'})}
    </button>
  )
}

export default ResetButton
