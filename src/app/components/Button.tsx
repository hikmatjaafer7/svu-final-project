import React, {ReactNode} from 'react'
import {useIntl} from 'react-intl'

interface ButtonProps {
  onClick?: any
  label: string
  icon?: any
  btnClass?: string
  Disabled?: boolean
}

const Button: React.FC<ButtonProps> = ({
  onClick,
  label,
  icon,
  Disabled = false,
  btnClass = 'btn btn-primary',
  ...buttonProps
}) => {
  const intl = useIntl()
  const handleClick = (event: any) => {
    if (onClick && typeof onClick === 'function') onClick(event?.target?.value)
  }
  return (
    <button
      type='button'
      disabled={Disabled}
      className={btnClass}
      onClick={handleClick}
      {...buttonProps}
    >
      {icon && icon}
      {intl.formatMessage({id: label})}
    </button>
  )
}

export default Button
