import React, {ChangeEvent} from 'react'
import {useIntl} from 'react-intl'

interface InputProps {
  title?: string
  inputValue: string | number
  onChange?: (value: string) => void
  icon?: any
  placeholder?: string
  inputType?: string
  inputClass?: string
  ref?: any
  name?: string
  // isRequired?: boolean
  validationMessage?: string
}

const Input: React.FC<InputProps> = ({
  inputValue,
  onChange,
  icon,
  inputType = 'text',
  inputClass = 'form-control form-control-solid w-250px ',
  placeholder,
  title,
  ref,
  name,
  // isRequired,
  validationMessage = 'field_is_required',
  ...inputProps
}) => {
  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (onChange) onChange(e?.target?.value)
  }
  const intl = useIntl()
  return (
    <div className='d-block align-items-center position-relative my-1 '>
      {icon && icon}
      {title && (
        <>
          <label>{intl.formatMessage({id: title})}</label>
          {/* {isRequired && <span className='text-danger'> *</span>} */}
        </>
      )}
      <input
        type={inputType}
        className={inputClass}
        placeholder={placeholder}
        value={inputValue}
        onChange={handleInputChange}
        ref={ref}
        // required={isRequired}
        {...inputProps}
      />
      {/* {isRequired && (
        <span className='text-danger'>{intl.formatMessage({id: validationMessage})}</span>
      )} */}
    </div>
  )
}

export default Input
