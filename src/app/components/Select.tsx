import React, {ChangeEvent} from 'react'
import {useIntl} from 'react-intl'

export interface OptionItem {
  key: string | number
  value: string | number
}
interface SelectProps {
  Key?:string
  ValueKey?:string
  title?: string
  selectedValue: string | number
  onChange?: (value: string) => void
  options: Array<OptionItem> | any
}

const Select: React.FC<SelectProps> = ({selectedValue, onChange, title, options,Key='key',ValueKey='value'}) => {
  const handleSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
    if (onChange) onChange(e.target.value)
  }

  const intl = useIntl()

  return (
    <div className='d-block align-items-center position-relative my-1'>
      {title && <label className='mb-2 fs-6 fw-bold'>{intl.formatMessage({id: title})}</label>}
      <select
        className='form-select form-select-solid fw-bolder'
        data-kt-select2='true'
        data-placeholder={intl.formatMessage({id: 'select_option'})}
        data-allow-clear='true'
        data-kt-user-table-filter='two-step'
        data-hide-search='true'
        onChange={handleSelectChange}
        value={selectedValue}
      >
        {options &&
          options.map((opt) => {
            return (
              <option key={opt[Key]} value={opt[Key]}>
                {opt[ValueKey]}
              </option>
            )
          })}
      </select>
    </div>
  )
}

export default Select
