import React from 'react'
import {useIntl} from 'react-intl'

interface props {
  isSubmitting: boolean
  isValid: boolean
  touched: boolean
  onclick?: any
  id?:any
}
const SubmitButton = (props) => {
  const {isSubmitting, isValid, touched, onclick,id} = props
  const intel = useIntl()
  return (
    <button
    id={id}
      type='submit'
      className='btn btn-primary'
      data-kt-users-modal-action='submit'
      disabled={isSubmitting || !isValid || !touched}
      onClick={onclick ? () => onclick() : () => console.log()}
    >
      {!isSubmitting && (
        <span className='indicator-label'>{intel.formatMessage({id: 'submit'})}</span>
      )}
      {isSubmitting && (
        <span className=''>
          {intel.formatMessage({id: 'please_wait...'})}{' '}
          <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
        </span>
      )}
    </button>
  )
}

export default SubmitButton
