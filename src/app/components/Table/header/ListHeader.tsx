import {useGenericListView} from '../../../core/GenericListViewProvider'
import {ListToolbar} from './ListToolbar'
import {ListGrouping} from './ListGrouping'
import {ListSearchComponent} from './ListSearchComponent'

interface ListHeaderProps {
  ListFilterChildren: any
  onFilterSubmit?: any
  deleteSelected?: any
  queryString: string
  handleAdd?: any
  withResetBtn?: boolean
  withSubmitBtn?: boolean
}
const ListHeader: React.FC<ListHeaderProps> = ({
  ListFilterChildren,
  onFilterSubmit,
  deleteSelected,
  queryString,
  handleAdd,
  withResetBtn,
  withSubmitBtn,
}) => {
  const {selected} = useGenericListView()
  return (
    <div className='card-header border-0 pt-6'>
      <ListSearchComponent />
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        {deleteSelected && selected.length > 0 ? (
          <ListGrouping deleteSelected={deleteSelected} queryString={queryString} />
        ) : (
          <ListToolbar
            withResetBtn={withResetBtn}
            withSubmitBtn={withSubmitBtn}
            ListFilterChildren={ListFilterChildren}
            onFilterSubmit={onFilterSubmit}
            handleAdd={handleAdd}
          />
        )}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export {ListHeader}
