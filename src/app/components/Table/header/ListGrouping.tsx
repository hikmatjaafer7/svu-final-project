import {useQueryClient, useMutation} from 'react-query'
import {useGenericQueryResponse} from '../../../core/GenericQueryResonseProvider'
import {useGenericListView} from '../../../core/GenericListViewProvider'

interface ListGroupingProps {
  deleteSelected: any
  queryString: string
}
const ListGrouping: React.FC<ListGroupingProps> = ({deleteSelected, queryString}) => {
  const {selected, clearSelected} = useGenericListView()
  const queryClient = useQueryClient()
  const {query} = useGenericQueryResponse()

  const deleteSelectedItems = useMutation(() => deleteSelected(selected), {
    // 💡 response of the mutation is passed to onSuccess
    onSuccess: () => {
      // ✅ update detail view directly
      queryClient.invalidateQueries([`${queryString}-${query}`])
      clearSelected()
    },
  })

  return (
    <div className='d-flex justify-content-end align-items-center'>
      <div className='fw-bolder me-5'>
        <span className='me-2'>{selected.length}</span> Selected
      </div>

      <button
        type='button'
        className='btn btn-danger'
        onClick={async () => await deleteSelectedItems.mutateAsync()}
      >
        Delete Selected
      </button>
    </div>
  )
}

export {ListGrouping}
