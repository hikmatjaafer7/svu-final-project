import {KTIcon} from '../../../../_metronic/helpers'
import Button from '../../Button'
import {useGenericListView} from '../../../core/GenericListViewProvider'
import {ListFilter} from './ListFilter'
interface ListToolbarProps {
  ListFilterChildren: any
  onFilterSubmit: any
  handleAdd: any
  withResetBtn?: boolean
  withSubmitBtn?: boolean
}
const ListToolbar: React.FC<ListToolbarProps> = ({
  ListFilterChildren,
  onFilterSubmit,
  handleAdd,
  withResetBtn,
  withSubmitBtn,
}) => {
  const {setItemIdForUpdate} = useGenericListView()

  return (
    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
      {onFilterSubmit && (
        <ListFilter
          withResetBtn={withResetBtn}
          withSUbmitBtn={withSubmitBtn}
          onFilterSubmit={onFilterSubmit}
        >
          {ListFilterChildren}
        </ListFilter>
      )}

      {/* begin::Export */}
      {/* <Button
        label='Export2'
        btnClass='btn btn-light-primary me-3'
        icon={<KTIcon iconName='exit-up' className='fs-2' />}
      /> */}
      {/* <button type='button' className='btn btn-light-primary me-3'>
        <KTIcon iconName='exit-up' className='fs-2' />
        Export
      </button> */}
      {/* end::Export */}

      {/* begin::Add  */}
      {handleAdd && (
        <button type='button' className='btn btn-primary' onClick={handleAdd}>
          <KTIcon iconName='plus' className='fs-2' />
          إضافة
        </button>
      )}
      {/* end::Add  */}
    </div>
  )
}

export {ListToolbar}
