/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC, ReactNode, useEffect} from 'react'
import {MenuComponent} from '../../../../_metronic/assets/ts/components'
import {ID, KTIcon} from '../../../../_metronic/helpers'

type Props = {
  id: ID
  menuItems: {label: string; onClick: any; menuElement?: ReactNode,isNavigate:boolean,path:string}[]
}

const ActionsCell: FC<Props> = ({id, menuItems}) => {
  useEffect(() => {
    MenuComponent.reinitialization()
  }, [])

  return (
    <>
      <a
        href='#'
        className='btn btn-light btn-active-light-primary btn-sm'
        data-kt-menu-trigger='click'
        data-kt-menu-placement='bottom-end'
      >
        الإجراءات 
        <KTIcon iconName='down' className='fs-5 m-0' />
      </a>
      {/* begin::Menu */}
      <div
        className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4'
        data-kt-menu='true'
      >
        {/* begin::Menu items */}
        {menuItems &&
          Array.isArray(menuItems) &&
          menuItems.map((item) => {
            return (
              <div className='menu-item px-3'>
                {item.menuElement ? (
                  item.menuElement
                ) : (
                  <a className='menu-link px-3' onClick={item.onClick}>
                    {item.label}
                  </a>
                )}
              </div>
            )
          })}
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  )
}

export {ActionsCell}
