import {FC} from 'react'
import RequestStatus from '../../../core/enums/RequestStatus'
import {Localize} from '../../../../_metronic/i18n/Localize'
import {useIntl} from 'react-intl'
type Props = {
  status?: any
  label?: string
}
const StatusCell: FC<Props> = ({status, label}) => {
  const intl = useIntl()
  return (
    <>
      <div
        className={`badge
        ${
          status === RequestStatus.Pending
            ? ' badge-light-info '
            : status === RequestStatus.Rejected
            ? ' badge-light-danger '
            : 'badge-light-success'
        }
       fw-bolder min-w-100px justify-content-center rounded rounded-5 py-3 px-5 fs-7`}
      >
        {intl.formatMessage({id: status})}
      </div>
    </>
  )
}

export {StatusCell}
