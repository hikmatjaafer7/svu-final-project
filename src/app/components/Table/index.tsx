import {ColumnInstance, Row, useTable} from 'react-table'
import {Loading} from '../Loading'
import {CustomHeaderColumn} from './columns/CustomHeaderColumn'
import {CustomRow} from './columns/CustomRow'
import {Pagination} from './pagination/index.'

interface TableProps {
  isLoading: boolean
  pagination: any
  updateState: any
  columns: any
  data: any
  withPagination?: boolean
}

export default function Table({
  isLoading,
  pagination,
  updateState,
  columns,
  data,
  withPagination = true,
}: TableProps) {
  const {getTableProps, getTableBodyProps, headerGroups, rows, prepareRow} = useTable({
    columns,
    data,
  })

  return (
    <>
      <div className='table-responsive'>
        <table
          id='kt_table_users'
          className='table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer'
          {...getTableProps()}
        >
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr
                {...headerGroup.getHeaderGroupProps()}
                className='text-start text-muted fw-bolder fs-7 text-uppercase gs-0'
              >
                {headerGroup.headers.map((column: ColumnInstance) => (
                  <CustomHeaderColumn key={column.id} column={column} />
                ))}
              </tr>
            ))}
          </thead>
          <tbody className='text-gray-600 fw-bold' {...getTableBodyProps()}>
            {rows.length > 0 ? (
              rows.map((row: Row, i) => {
                prepareRow(row)
                return <CustomRow row={row} key={`row-${i}-${row.id}`} />
              })
            ) : (
              <tr>
                <td colSpan={7}>
                  <div className='d-flex text-center w-100 align-content-center justify-content-center'>
                    لم يتم العثور على سجلات مطابقة
                  </div>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      {withPagination && (
        <Pagination isLoading={isLoading} updateState={updateState} pagination={pagination} />
      )}
      {isLoading && <Loading />}
    </>
  )
}
