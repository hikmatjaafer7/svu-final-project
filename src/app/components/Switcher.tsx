import React from 'react'
import {useIntl} from 'react-intl'

interface SwitcherProps {
  onChange?: any
  label?: string
  id: string
  value: boolean
}

const Switcher: React.FC<SwitcherProps> = ({onChange, label, value, id}) => {
  const intl = useIntl()
  const handleChange = (event: any) => {
    if (onChange && typeof onChange === 'function') onChange(event?.target?.value)
  }
  return (
    <div className='form-check form-switch form-check-custom form-check-solid my-1'>
      {label && (
        <label className='form-check-label mx-2 fw-bold' htmlFor={id}>
          {intl.formatMessage({id: label})}
        </label>
      )}{' '}
      <input
        className='form-check-input'
        type='checkbox'
        checked={value}
        id={id}
        onChange={handleChange}
      />
    </div>
  )
}

export default Switcher
