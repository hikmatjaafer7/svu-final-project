import {useFormikContext} from 'formik'
import React, {ChangeEvent, useEffect, useRef} from 'react'
import {useIntl} from 'react-intl'

interface OptionItem {
  key: string | number
  value: string | number
}
interface SelectProps {
  Key?:string
  ValueKey?:string
  title?: string
  onChange?: (value: string) => void
  options: Array<OptionItem> | any
  name: string
  isRequired: boolean
  disabled?: boolean
  hint?: string
}

const FormikSelect: React.FC<SelectProps> = (props) => {
  const {title, name, isRequired, disabled, hint, onChange, options,Key='key',ValueKey='value'} = props
  const {errors, touched, getFieldProps, isSubmitting, values, setValues, setFieldValue} =
    useFormikContext()
  const handleSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
    setFieldValue(name, e.target.value)
    if (onChange) onChange(e.target.value)
  }
  const selectRef = useRef(null)

  const intl = useIntl()
  useEffect(() => {
    // This code will run when the component is initially rendered

    // You can perform actions on the input element here
    if (selectRef.current && selectRef?.current?.value)
      setFieldValue(name, selectRef?.current?.value) // Set the input value

    // You can add other actions you want to perform on the input element here
  }, [])
  return (
    <div className='d-block align-items-center position-relative my-1'>
      {title && <label className='fw-bold fs-6 mb-2'>{intl.formatMessage({id: title})}</label>}
      <select
        className='form-select form-select-solid fw-bolder'
        data-kt-select2='true'
        data-placeholder={intl.formatMessage({id: 'select_option'})}
        data-allow-clear='true'
        data-kt-user-table-filter='two-step'
        data-hide-search='true'
        onChange={handleSelectChange}
        value={values[name]}
        ref={selectRef}
      >
        {options &&
          options.map((opt) => {
            return (
              <option key={opt[Key]} value={opt[Key]}>
                {opt[ValueKey]}
              </option>
            )
          })}
      </select>
      {touched[name] && errors[name] && (
        <div className='fv-plugins-message-container'>
          <div className='fv-help-block text-danger mt-1 ms-1'>
            <span role='alert'>{errors[name]}</span>
          </div>
        </div>
      )}
      {hint && (
        <div className='fv-plugins-message-container'>
          <div className='text-dark'>
            <span role='alert'>{hint}</span>
          </div>
        </div>
      )}
    </div>
  )
}

export default FormikSelect
