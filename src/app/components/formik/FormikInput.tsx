import {useFormikContext} from 'formik'
import clsx from 'clsx'
import {useEffect, useRef} from 'react'
import {useIntl} from 'react-intl'
interface props {
  title: string
  name: string
  type?: string
  isRequired: boolean
  disabled?: boolean
  hint?: string
  onChange?: any
  value?: string
  hidden?: boolean
  min?: number
  onChangeEvent?: any
}

const FormikInput = (props: props) => {
  const {
    title,
    name,
    type,
    isRequired,
    disabled,
    hint,
    onChange,
    onChangeEvent,
    value,
    hidden,
    min,
  } = props
  const {errors, touched, getFieldProps, isSubmitting, values, setValues, setFieldValue} =
    useFormikContext()

  const inputRef = useRef(null)
  const handleChange = (event) => {
    // console.log(values)
    console.log('event.target.files :>> ', event.target.files);
    setFieldValue(name, event.target.value)
    if (onChange && typeof onChange === 'function') onChange(event.target.value)
    if (onChangeEvent && typeof onChangeEvent === 'function') onChangeEvent(event)
  }
  useEffect(() => {
    // This code will run when the component is initially rendered

    // You can perform actions on the input element here
    if (inputRef.current && inputRef?.current?.value) setFieldValue(name, inputRef?.current?.value) // Set the input value

    // You can add other actions you want to perform on the input element here
  }, [])
  // console.log('values[name] :>> ', values[name])

  /*  useEffect(() => {
    handleChange(values[name])
  }, [values[name]]) */
  const intl = useIntl()
  return (
    <div className={`fv-row mb-7  ${hidden ? 'd-none' : ''}`}>
      {/* begin::Label */}
      <label className={`${isRequired ? 'required' : ''} fw-bold fs-6 mb-3`}>
        {intl.formatMessage({id: title})}
      </label>
      <input
        placeholder={intl.formatMessage({id: title})}
        {...getFieldProps({name})}
        type={type || 'text'}
        name={name}
        min={min}
        value={!values[name] ? value : values[name]}
        className={clsx(
          'form-control form-control-solid mb-3 mb-lg-0',
          {'is-invalid': touched[name] && errors[name]},
          {
            'is-valid': touched[name] && !errors[name],
          }
        )}
        autoComplete='off'
        disabled={isSubmitting || disabled}
        onChange={(event) => handleChange(event)}
        ref={inputRef}
      />
      {touched[name] && errors[name] && (
        <div className='fv-plugins-message-container'>
          <div className='fv-help-block text-danger mt-1 ms-1'>
            <span role='alert'>{errors[name]}</span>
          </div>
        </div>
      )}
      {hint && (
        <div className='fv-plugins-message-container'>
          <div className='text-dark'>
            <span role='alert'>{hint}</span>
          </div>
        </div>
      )}
      {/* end::Input */}
    </div>
  )
}

export default FormikInput
