import {useFormikContext} from 'formik'
import clsx from 'clsx'
import {useIntl} from 'react-intl'
interface props {
  title: string
  name: string
  type?: string
  isRequired: boolean
  value?: any
}
const FormikInputDate = (props: props) => {
  const {title, name, type, isRequired, value} = props
  const {errors, touched, getFieldProps, isSubmitting, values} = useFormikContext()
  const intl = useIntl()
  return (
    <div className='fv-row mb-7'>
      {/* begin::Label */}
      <label className={`${isRequired ? 'required' : ''} fw-bold fs-6 mb-2`}>
        {intl.formatMessage({id: title})}
      </label>
      <input
        placeholder={title}
        {...getFieldProps({name})}
        type={type || 'date'}
        name={name}
        value={!values[name] ? value : values[name]}
        className={clsx(
          'form-control form-control-solid mb-3 mb-lg-0',
          {'is-invalid': touched[name] && errors[name]},
          {
            'is-valid': touched[name] && !errors[name],
          }
        )}
        autoComplete='off'
        disabled={isSubmitting}
      />
      {touched[name] && errors[name] && (
        <div className='fv-plugins-message-container'>
          <div className='fv-help-block'>
            <span role='alert'>{errors[name]}</span>
          </div>
        </div>
      )}
      {/* end::Input */}
    </div>
  )
}

export default FormikInputDate
