import {useFormikContext} from 'formik'
import clsx from 'clsx'
import { useIntl } from 'react-intl'

interface props {
  title: string
  name: string
  isRequired: boolean
  disabled?: boolean
  hint?: string
}

const FormikToggle = (props: props) => {
  const intl = useIntl()
  const {title, name, isRequired, disabled, hint} = props
  const {errors, touched, getFieldProps, values, isSubmitting, setFieldValue} = useFormikContext()
  return (
    <div className='fv-row mb-7 d-flex flex-column'>
      <label className={`${isRequired ? 'required' : ''} fw-bold fs-6 mb-7`}>{intl.formatMessage({id:title})}</label>
      <div className='form-check form-switch ml-5'>
        <input
          className={clsx(
            'form-check-input',
            {'is-invalid': touched[name] && errors[name]},
            {
              'is-valid': touched[name] && !errors[name],
            }
          )}
          type='checkbox'
          id={name}
          checked={values[name]}
          onChange={(e) => setFieldValue(name, e.target.checked)}
          disabled={isSubmitting || disabled}
          {...getFieldProps(name)}
        />
      </div>
      {touched[name] && errors[name] && (
        <div className='fv-plugins-message-container'>
          <div className='fv-help-block'>
            <span role='alert'>{errors[name]}</span>
          </div>
        </div>
      )}
      {hint && (
        <div className='fv-plugins-message-container'>
          <div className='text-dark'>
            <span role='alert'>{hint}</span>
          </div>
        </div>
      )}
    </div>
  )
}

export default FormikToggle
