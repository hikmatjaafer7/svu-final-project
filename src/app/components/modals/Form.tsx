import React, {ReactNode} from 'react'

type FormModalProps = {
  children: ReactNode
  title: string
  id: string
  classes?: string
}

const FormModal: React.FC<FormModalProps> = ({children, title, id, classes = ''}) => {
  return (
    <div className='modal fade' id={`${id}`} tabIndex={-1} aria-hidden='true'>
      <div className={'modal-dialog modal-dialog-centered ' + classes}>
        <div className='modal-content'>
          {/* <form className='form' action='#' id={`${id}_form`}> */}
          <div className='modal-header' id={`${id}_header`}>
            <h2>{title}</h2>
            <div
              className='btn btn-sm btn-icon btn-active-color-primary'
              id={`${id}_close`}
              data-bs-dismiss='modal'
            >
              <i className='ki-duotone ki-cross fs-1'>
                <span className='path1'></span>
                <span className='path2'></span>
              </i>
            </div>
          </div>
          {children}
          {/* <div className='modal-footer flex-center'>
              <button type='reset' id={`${id}_cancel`} className='btn btn-light me-3'>
                Discard
              </button>
              <button type='submit' id={`${id}_submit`} className='btn btn-primary'>
                <span className='indicator-label'>Submit</span>
              </button>
            </div> */}
          {/* </form> */}
        </div>
      </div>
    </div>
  )
}

export default FormModal
