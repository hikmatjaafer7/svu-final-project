/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {FC} from 'react'
import {KTIcon, toAbsoluteUrl} from '../../../_metronic/helpers'

interface listItem {
  img?: string
  amount: string
  description: string
  updatedAt: string
  // access : string;
  // color?: string
}

interface ModalListProps {
  title: string
  description: string
  list: listItem[]
  id: string
  user_name: string
}

const ModalList: FC<ModalListProps> = ({title, description, list, user_name, id}) => {
  return (
    <div className='modal fade' id={id} aria-hidden='true'>
      <div className='modal-dialog mw-400px'>
        <div className='modal-content'>
          <div className='modal-header pb-0 border-0 justify-content-end'>
            <div className='btn btn-sm btn-icon btn-active-color-primary' data-bs-dismiss='modal'>
              <KTIcon iconName='cross' className='fs-1' />
            </div>
          </div>

          <div className='modal-body scroll-y mx-5 mx-xl-18pt-0 pb-15'>
            <div className='text-center mb-13'>
              <h1 className='mb-3'>{title}</h1>

              <div className='text-muted fw-bold fs-5'>{description}</div>
            </div>

            <div className='mb-10'>
              {/* <div className='fs-6 fw-bold mb-2'>Your Invitations</div> */}

              <div className='mh-300px scroll-y me-n7 pe-7'>
                {list.map((item, i) => {
                  return (
                    <div
                      className='d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed'
                      key={i}
                    >
                      <div className='d-flex align-items-center w-100'>
                        <div className='symbol symbol-35px symbol-circle'>
                          {/* {user.img && <img alt='Pic' src={user.img} />} */}
                          {/* {user.color && ( */}
                          <div className='symbol symbol-35px symbol-circle'>
                            <span className={`symbol-label bg-light-primary text-primary fw-bold`}>
                              {user_name?.charAt(0)}
                            </span>
                          </div>
                          {/* )} */}
                        </div>

                        <div className='ms-5 w-100'>
                          <div className='row w-100 justify-content-between'>
                            <div className='col fs-5 fw-bolder text-gray-900 text-hover-primary mb-2'>
                              {item.amount}
                            </div>
                            <div className='col-auto justify-content-end text-end badge badge-light-primary'>
                              {item?.updatedAt}
                            </div>
                          </div>
                          <div className='fw-bold text-muted'>{item.description}</div>
                        </div>
                      </div>

                      {/* <div className='ms-2 w-100px'>
                        <select
                          defaultValue={'2'}
                          className='form-select form-select-solid form-select-sm select2-hidden-accessible'
                        >
                          <option value='1'>Guest</option>
                          <option value='2' data-select2-id='select2-data-12-vz6w'>
                            Owner
                          </option>
                          <option value='3'>Can Edit</option>
                        </select>
                      </div> */}
                    </div>
                  )
                })}
              </div>
            </div>

            {/* <div className='d-flex flex-stack'>
              <div className='me-5 fw-bold'>
                <label className='fs-6'>Adding listItems by Team Members</label>
                <div className='fs-7 text-muted'>
                  If you need more info, please check budget planning
                </div>
              </div>

              <label className='form-check form-switch form-check-custom form-check-solid'>
                <input className='form-check-input' type='checkbox' value='1' />

                <span className='form-check-label fw-bold text-muted'>Allowed</span>
              </label>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  )
}

export {ModalList}
