import {FC, useState, createContext, useContext, useMemo, ReactNode} from 'react'
import {
  ID,
  calculatedGroupingIsDisabled,
  calculateIsAllDataSelected,
  groupingOnSelect,
  initialListView,
  ListViewContextProps,
  groupingOnSelectAll,
  WithChildren,
} from '../../_metronic/helpers'
import {useGenericQueryResponse, useGenericQueryResponseData} from './GenericQueryResonseProvider'

interface ListViewProviderProps {
  children: ReactNode
  data: any
  isLoading: boolean
}

const ListViewContext = createContext<ListViewContextProps>(initialListView)

const GenericListViewProvider: FC<ListViewProviderProps> = ({children, data, isLoading}) => {
  const [selected, setSelected] = useState<Array<ID>>(initialListView.selected)
  const [itemIdForUpdate, setItemIdForUpdate] = useState<ID>(initialListView.itemIdForUpdate)

  const disabled = useMemo(() => calculatedGroupingIsDisabled(isLoading, data), [isLoading, data])
  const isAllSelected = useMemo(() => calculateIsAllDataSelected(data, selected), [data, selected])
  return (
    <ListViewContext.Provider
      value={{
        selected,
        itemIdForUpdate,
        setItemIdForUpdate,
        disabled,
        isAllSelected,
        onSelect: (id: ID) => {
          groupingOnSelect(id, selected, setSelected)
        },
        onSelectAll: () => {
          console.log('isAllSelected :>> ', isAllSelected)
          groupingOnSelectAll(isAllSelected, setSelected, data)
        },
        clearSelected: () => {
          setSelected([])
        },
      }}
    >
      {children}
    </ListViewContext.Provider>
  )
}

const useGenericListView = () => useContext(ListViewContext)

export {GenericListViewProvider, useGenericListView}
