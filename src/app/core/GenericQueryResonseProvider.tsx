/* eslint-disable react-hooks/exhaustive-deps */
import {FC, useContext, useState, useEffect, useMemo, ReactNode} from 'react'
import {useQuery} from 'react-query'
import {
  createResponseContext,
  initialQueryResponse,
  initialQueryState,
  PaginationState,
  stringifyRequestQuery,
} from '../../_metronic/helpers'

import {useGenericQueryRequest} from './GenericQueryRequestProvider'

interface ResponseProviderProps {
  children: ReactNode
  getApi: any
  modelType?: any
  queryString: string
  urlParameter?: any
}

const GenericQueryResponseContext = createResponseContext<any>(initialQueryResponse)
const GenericQueryResponseProvider: FC<ResponseProviderProps> = ({
  children,
  getApi,
  queryString,
  urlParameter,
}) => {
  const {state} = useGenericQueryRequest()
  const [query, setQuery] = useState<string>(stringifyRequestQuery(state))
  const updatedQuery = useMemo(() => stringifyRequestQuery(state), [state])

  useEffect(() => {
    if (query !== updatedQuery) {
      setQuery(updatedQuery)
    }
  }, [updatedQuery])

  const {
    isFetching,
    refetch,
    data: response,
  } = useQuery(
    `${queryString}-${query}-${urlParameter ? urlParameter : ''}`,
    () => {
      if (urlParameter) return getApi(urlParameter, query)
      return getApi(query)
    },
    {cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false}
  )

  return (
    <GenericQueryResponseContext.Provider value={{isLoading: isFetching, refetch, response, query}}>
      {children}
    </GenericQueryResponseContext.Provider>
  )
}

const useGenericQueryResponse = () => useContext(GenericQueryResponseContext)

const useGenericQueryResponseData = () => {
  const {response} = useGenericQueryResponse()
  if (!response) {
    return []
  }

  return response?.data || []
}

const useGenericQueryResponsePagination = () => {
  const defaultPaginationState: PaginationState = {
    links: [],
    ...initialQueryState,
  }

  const {response} = useGenericQueryResponse()
  if (!response || !response.payload || !response.payload.pagination) {
    return defaultPaginationState
  }

  return response.payload.pagination
}

const useGenericQueryResponseLoading = (): boolean => {
  const {isLoading} = useGenericQueryResponse()
  return isLoading
}
const useGenericQueryResponseRefetch = () => {
  const {refetch} = useGenericQueryResponse()
  return refetch
}
export {
  GenericQueryResponseProvider,
  useGenericQueryResponse,
  useGenericQueryResponseData,
  useGenericQueryResponsePagination,
  useGenericQueryResponseLoading,
  useGenericQueryResponseRefetch,
}
