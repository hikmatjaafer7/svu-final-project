import {useIntl} from 'react-intl'
import {PageTitle} from '../../../_metronic/layout/core'
import {StatisticsWidget1} from '../../../_metronic/partials/widgets'
import {useQuery} from 'react-query'
import {QUERIES} from '../../../_metronic/helpers'
import {get} from '../../../services/common-http'
import {useAuth} from '../../modules/auth'

const UserDashboardPage = () => {
  const {currentUser} = useAuth()
  const {data: monthSum}: any = useQuery(
    QUERIES.COUNTS_BY_MONTH_BY_USER,
    () => {
      return get('/funds/sum/month/user/' + currentUser.id)
    },
    {
      refetchOnMount: false,
      cacheTime: 100,
    }
  )
  console.log('monthSum :>> ', monthSum)
  const {data: yearSum}: any = useQuery(
    QUERIES.COUNTS_BY_YEAR_BY_USER,
    () => {
      return get('/funds/sum/year/user/' + currentUser.id)
    },
    {
      refetchOnMount: false,
      cacheTime: 100,
    }
  )
  return (
    <>
      {/* begin::Row */}
      <div className='row g-5 g-xl-8'>
        <h1>إحصائيات سنوية</h1>
        {/* <div className='col-xl-4'>
          <StatisticsWidget1
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-4.svg'
            title='مشتركين جدد'
            count={yearSum?.userCount}
            time={new Date().toDateString()}
            description=''
          />
        </div> */}
        <div className='col-xl-4  '>
          <StatisticsWidget1
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-4.svg'
            title='إجمالي الأسهم'
            count={yearSum?.subscriptionSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4  '>
          <StatisticsWidget1
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-4.svg'
            title='إجمالي القروض'
            count={yearSum?.loanSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            titleClass=' fw-bold text-success'
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-4.svg'
            title='إجمالي أسداد الإشتراكات'
            count={yearSum?.subscriptionPaymentSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            titleClass=' fw-bold text-success'
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-4.svg'
            title='إجمالي أسداد القروض'
            count={yearSum?.loanPaymentSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            className='card-xl-stretch mb-xl-8 text-dark'
            titleClass=' fw-bold text-danger'
            image='abstract-4.svg'
            title='عدد الإنذارات الإشتراكات'
            count={yearSum?.subscriptionAlarmCount}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            titleClass=' fw-bold text-danger'
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-4.svg'
            title='عدد الإنذارات القروض'
            count={yearSum?.loanAlarmCount}
            time={new Date().toDateString()}
            description=''
          />
        </div>
      </div>
      {/* end::Row */}

      <hr className='my-5' />
      {/* begin::Row */}
      <div className='row  g-5 g-xl-8'>
        <h1>إحصائيات شهرية</h1>
        {/* <div className='col-xl-4'>
          <StatisticsWidget1
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-5.svg'
            title='مشتركين جدد'
            count={monthSum?.userCount}
            time={new Date().toDateString()}
            description=''
          />
        </div> */}
        <div className='col-xl-4  '>
          <StatisticsWidget1
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-5.svg'
            title='إجمالي الأسهم'
            count={monthSum?.subscriptionSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4  '>
          <StatisticsWidget1
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-5.svg'
            title='إجمالي القروض'
            count={monthSum?.loanSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            titleClass=' fw-bold text-success'
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-5.svg'
            title='إجمالي أسداد الإشتراكات'
            count={monthSum?.subscriptionPaymentSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            titleClass=' fw-bold text-success'
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-5.svg'
            title='إجمالي أسداد القروض'
            count={monthSum?.loanPaymentSum}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            titleClass=' fw-bold text-danger'
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-5.svg'
            title='عدد الإنذارات الإشتراكات'
            count={monthSum?.subscriptionAlarmCount}
            time={new Date().toDateString()}
            description=''
          />
        </div>
        <div className='col-xl-4'>
          <StatisticsWidget1
            titleClass=' fw-bold text-danger'
            className='card-xl-stretch mb-xl-8 text-dark'
            image='abstract-5.svg'
            title='عدد الإنذارات القروض'
            count={monthSum?.loanAlarmCount}
            time={new Date().toDateString()}
            description=''
          />
        </div>
      </div>
      {/* end::Row */}
    </>
  )
}

const UserDashboardWrapper = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({id: 'dashboard'})}</PageTitle>
      <UserDashboardPage />
    </>
  )
}

export {UserDashboardWrapper}
