import axios, {AxiosResponse} from 'axios'
import {post, get, del} from './common-http'

const getList = async (url: string, query?: string, page?: number): Promise<any> => {
  var response: any = await get(`${url}`)
  return response
}

const getOne = async (url: string, id: number): Promise<any> => {
  var response: any = await post(`${url}/${id}`)
  return response.data?.data
}

const create = async (url: string, object: any = {}) => {
  var response: any = await post(url, object)
  return response
}

const update = async (url: string, object: any, id: any) => {
  var response: any = await post(`${url}/${id}`, object)
  return response
}

const dele = async (url: string, id: any) => {
  var response: any = await del(`${url}/${id}`)
  return response
}

export {getList, getOne, create, update, dele}
