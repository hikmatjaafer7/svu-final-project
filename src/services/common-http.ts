import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'

const token: any = JSON?.parse(localStorage?.getItem('kt-auth-react-v'))
axios.defaults.headers.common['Authorization'] = `Bearer ${token?.api_token}`

const api = axios.create({
  baseURL: process.env.REACT_APP_PROJECT_API_URL, //'http://localhost:3000/api',
  headers: {Authorization: `Bearer ${token?.api_token}`},
})

interface RequestOptions extends AxiosRequestConfig {}

async function request<T>(url: string, options: RequestOptions, errorMessage): Promise<T> {
  try {
    const response: AxiosResponse<T> = await api(url, options)
    return response.data
  } catch (error) {
    throw error
  }
}

export async function get<T>(url: string): Promise<T> {
  const options: RequestOptions = {
    method: 'GET',
  }
  return request<T>(url, options, `Error:\nMethod: Get\nUrl: ${url} \n Message:`)
}

export async function post<T>(url: string, body: any = {}): Promise<T> {
  const options: RequestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
  }
  return request<T>(url, options, `Error:\nMethod: Post\nUrl: ${url} \n Message:`)
}

export async function put<T>(url: string, body: any): Promise<T> {
  const options: RequestOptions = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
  }
  return request<T>(url, options, `Error:\nMethod: Put\nUrl: ${url} \n Message:`)
}

export async function del<T>(url: string): Promise<T> {
  const options: RequestOptions = {
    method: 'DELETE',
  }
  return request<T>(url, options, `Error:\nMethod: Delete\nUrl: ${url} \n Message:`)
}
